﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO.Ports;

namespace DoorsControlApplication
{
    public enum TrxPacketFields
    {
        Start = 0,
        Address = 4,
        Type = 5,
        Data = 6,
        CRC = 30
    }
    public enum AddressType
    {
        pc = 0,
        drv,
        tdrv,
        tlog,
        tpost
    }

    //тип подключения
    public enum ConnectionType  
    {
        Unused = 0,                     //не инициализировано
        COM,                            //USB - UART
        Ethernet,                       //LAN
        WiFi,                           //Web
        Bluetooth                       //беспроводной
    }

    //статус драйвера
    public enum StatusType
    {
        Unused = 0,                     //не инициализировано
        OK,                             //все штатно
        SystemError,                    //системная ошибка
        FatalError                      //критическая ошибка
    }

    //тип ошибок
    public enum ErrorType
    {
        Unused = 0,                     //не инициализировано
        NoMotorData,                    //нет данных на мотор
        InVoltageLow,                   //низкое входное напряжение
        InVoltageNegative,              //отрицатеьное входное напряжение 
        AccVoltageNegative,             //отрицательное напряжение аккумулятора
        Overcurrent,                    //превышение по току
        PositionUnreached,              //невозможно достичь заданную позицию
        CalibrationFailed,              //ошибка алгоритма калибровки
        NoDataInFlash,                  //память МК пуста
        FlashEraseFailed,               //память МК не отвечает
        ADCUnunit,                      //ошибка инициализации АЦП
        ADCConvertionFailed,            //ошибка преобразования АЦП    
        DMAUninit,                      //ошибка инициализации прямого доступа к памяти
        I2CUninit,                      //ошибка инициализации интерфейса резервного хранилища
        TimPWMUninit,                   //ошибка инициализации ШИМ
        TimEncUninit,                   //ошибка инициализации энкодера
        TimCycleUninit,                 //ошибка инициализации таймера
        UartUninit                      //ошибка инициализации консоли
    }

    //тип комманд
    public enum CommandType
    {
        Unused = 0,                     //не инициализировано
        SetElectData = 5,               //5- установка параметров электрической системы
        GetElectData,                   //6- чтение параметров электрической системы
        SetMechData,                    //7- установка параметров механической системы
        GetMechData,                    //8- чтение параметров механической системы
        SetDynData,                     //9- установка параметров механической системы
        GetDynData,                     //10- чтение параметров механической системы
        SetManCtrl,                     //11- установка ручного режима управления дверью
        SetAutoCtrl,                    //12- установка автоматического режима управления дверью
        SetWintCtrl,                    //13- установка автоматического режима управления дверью
        ResetWintCtrl,                  //14- установка автоматического режима управления дверью
        GetCalibrate,                   //15- калибровка автоматики
        SetDoorOpenSlow,                //16- открыть дверь медленно    
        SetDoorOpen,                    //17- открыть дверь
        SetDoorClose,                   //18- закрыть дверь
        GetAccVolt,                     //19- чтение напряжения на аккумуляторе
        SetAccOpen,                     //20- открыть транзистор аккумулятора
        SetAccClose,                    //21- закрыть транзистор аккумулятора
        SetTime,                        //22- установка времени
        GetTime,                        //23- чтение времени
        GetErrorCount,                  //24-- запрос количества ошибок
        GetVoltageData,                 //25- чтение пула напряжений
        GetSystemData,                  //26- чтение параметров состояния системы
        GetErrorData,                   //27- чтение лога ошибок
        ChangeReverse,                  //28- сменить полярность мотора
        SetCurMonitor,                  //29 - включить монитор тока
        ResetCurMonitor,                //30 - выключить монитор тока
        SetSelfCheck,                   //31- проверка крайних положений
        ResetSelfCheck,                 //32- проверка крайних положений
        SetDoorStop,                    //33- остановка двери
        SetServiceParam,                //34- установка сервисных параметров
        GetServiceParam,                //35- чтение сервисных параметров

        SetDiagData = 50,               //50- запись диагностических коэффициентов
        GetDiagData,                    //51- чтение диагностических коэффициентов

    }
    public partial class Form1 : Form
    {
        ConnectionType contype = ConnectionType.COM;
        bool connect = false;
        UInt32 driver_data_packs_ok = 0, driver_data_packs_lost = 0;

        Form2 auth_form;
        string[] allowed_login = { "admin", "a" };
        string[] allowed_pass = { "password", "a" };
        string session_login = "";
        string session_pass = "";

        float c1 = 0, c2 = 0, c3 = 0, c4 = 0;
        UInt64 time = 0;

        const uint magic_32 = 0xf3c3f00c;
        Thread serial_port_read;
        delegate void SetFormtDataDelegate(object sender, object[] data, bool clean);

        public Form1()
        {
            InitializeComponent();
            Renew_ports_list();

            auth_form = new Form2();
            auth_form.Hide();
        }

        //обновление списка текущих COM-портов
        private void Renew_ports_list()
        {
            this.ports_comboBox.Items.Clear();
            switch (contype)
            {
                case ConnectionType.COM:
                    string[] ports = SerialPort.GetPortNames();
                    foreach (string p in ports)
                    {
                        this.ports_comboBox.Items.Add(p);
                    }
                    break;
                case ConnectionType.Ethernet:
                    //do nothing
                    break;
                default:
                    break;
            }
        }

        //обновление выпадающего списка портов по клику 
        private void ports_comboBox_MouseClick(object sender, MouseEventArgs e)
        {
            Renew_ports_list();
        }

        //статус подключения был изменен
        private void Set_Connection_Status(bool state)
        {
           this.connect_radioButton.Checked = state;
           this.disconnect_radioButton.Checked = !state;
           connect = state;
        }

        //изменить тип подключения на COM
        private void COM_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            contype = ConnectionType.COM;
            Renew_ports_list();
        }

        //изменить тип подключения на Ethernet
        private void Ethernet_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            contype = ConnectionType.Ethernet;
            Renew_ports_list();
        }

        //подключение к устройству
        private void Connection_button_Click(object sender, EventArgs e)
        {
            switch (contype)
            {
                case ConnectionType.COM:
                    if (!this.serialPort1.IsOpen)
                    {
                        try
                        {
                            this.serialPort1.BaudRate = 115200;
                            this.serialPort1.PortName = this.ports_comboBox.Text;
                            this.serialPort1.Open();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            Set_Connection_Status(false);
                            return;
                        }
                        this.serialPort1.DiscardInBuffer();
                        this.Connection_button.BackColor = Color.ForestGreen;
                        Set_Connection_Status(true);

                        serial_port_read = new Thread(new ThreadStart(Serial_Port_Read_Thread));
                        serial_port_read.IsBackground = true;
                        serial_port_read.Start();
                    }
                    else
                    {
                        this.serialPort1.Close();
                        this.Connection_button.BackColor = SystemColors.ButtonFace;
                        Set_Connection_Status(false);
                        serial_port_read.Abort();
                    }
                    break;
                default:
                    break;
            }
        }

        //проверка уровня доступа к ресурсам администрирования
        private async void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 2 && !this.timer1.Enabled)
            {
                auth_form.Show();
                int res = await auth_form.wait_user_data();
                if (res > 0)
                {
                    this.session_login = auth_form.log;
                    this.session_pass = auth_form.pass;
                    auth_form.log = "";
                    auth_form.pass = "";

                    foreach (string l in allowed_login)                                     //check login
                    {
                        if (this.session_login.Equals(l))                                   //login OK
                        {
                            foreach (string p in allowed_pass)                              //check password
                            {
                                if (this.session_pass.Equals(p))                            //password OK
                                {
                                    foreach (GroupBox gb in this.tabPage2.Controls)         //show tab content
                                    {
                                        SetFormData(gb, new object[] { true }, false);
                                    }
                                    this.timer1.Enabled = true;                             //start session timer
                                }
                            }
                        }
                    }
                }
                auth_form.state = -1;
                if (!this.timer1.Enabled)        //authorization failed                
                {
                    this.tabControl1.SelectedIndex = 0;
                    MessageBox.Show("Неверный логин/пароль!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //событие таймера -> окончание сессии администрирования
        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (GroupBox gb in this.tabPage2.Controls)    //hide tab content
            {
                SetFormData(gb, new object[] { false }, false);
            }
            this.session_login = "";
            this.session_pass = "";
            this.timer1.Enabled = false;
        }

        //очистка графика
        private void chart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SetFormData(this.chart1, new object[] { 0 }, true);
        }

        //изменить управление на ручное
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            bool chk = this.radioButton1.Checked;
            this.Calib_button.Enabled = !chk;

            if (chk)
            {
                if (!this.serialPort1.IsOpen)
                {
                    MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] b;
                byte[] buffer = new byte[32];
                b = BitConverter.GetBytes(magic_32);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

                buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
                buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetManCtrl;

                this.serialPort1.Write(buffer, 0, buffer.Length);
            }
        }

        //изменить управление на автоматическое
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            bool chk = this.radioButton3.Checked;
            this.Calib_button.Enabled = chk;

            if (chk)
            {
                if (!this.serialPort1.IsOpen)
                {
                    MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] b;
                byte[] buffer = new byte[32];
                b = BitConverter.GetBytes(magic_32);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

                buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
                buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetAutoCtrl;

                this.serialPort1.Write(buffer, 0, buffer.Length);
            }
        }

        //метод безопасного изменения данных на форме 
        private void SetFormData(object sender, object[] data, bool clean)
        {
            Control c = (Control)sender;

            if (c.InvokeRequired)
            {
                SetFormtDataDelegate tsDelegate = new SetFormtDataDelegate(SetFormData);
                c.Invoke(tsDelegate, sender, data, clean);
            }
            else
            {
                string t = sender.GetType().ToString();
                if (t.Equals("System.Windows.Forms.TextBox"))
                {
                    TextBox tb = (TextBox)sender;
                    if (clean) tb.Text = "";
                    else tb.Text = data[0].ToString();
                }
                else if (t.Equals("System.Windows.Forms.ComboBox"))
                {
                    ComboBox cb = (ComboBox)sender;
                    Int32 ind = Convert.ToInt32(data[0]) - 1;
                    if (ind > cb.Items.Count) ind = 0;
                    cb.SelectedIndex = ind;
                }
                else if (t.Equals("System.Windows.Forms.RadioButton"))
                {
                    RadioButton rb = (RadioButton)sender;
                    bool b = (bool)data[0];
                    if (b) rb.Checked = true;
                    else rb.Checked = false;
                }
                else if (t.Equals("System.Windows.Forms.DataVisualization.Charting.Chart"))
                {
                    Chart ch = (Chart)sender;
                    if (clean)
                    {
                        foreach (Series s in ch.Series)
                        {
                            s.Points.Clear();
                        }
                    }
                    else
                    {
                        if (ch.Series[0].Points.Count > 100)
                        {
                            ch.Series[0].Points.RemoveAt(0);
                            ch.Series[1].Points.RemoveAt(0);
                            ch.Series[2].Points.RemoveAt(0);
                            ch.Series[3].Points.RemoveAt(0);
                            ch.Series[4].Points.RemoveAt(0);
                            ch.Series[5].Points.RemoveAt(0);
                        }
                        ch.Series[0].Points.Add(Convert.ToDouble(data[0]));
                        ch.Series[1].Points.Add(Convert.ToDouble(data[1]));
                        ch.Series[2].Points.Add(Convert.ToDouble(data[2]));
                        ch.Series[3].Points.Add(Convert.ToDouble(data[3]));
                        ch.Series[4].Points.Add(Convert.ToDouble(data[4]));
                        ch.Series[5].Points.Add(Convert.ToDouble(data[5]));
                    }
                }
                else if (t.Equals("System.Windows.Forms.GroupBox"))
                {
                    GroupBox gb = (GroupBox)sender;
                    gb.Visible = (bool)data[0];
                }
                else if (t.Equals("System.Windows.Forms.DataGridView"))
                {
                    DataGridView dg = (DataGridView)sender;
                    dg.Rows.Add();
                    dg[0, dg.RowCount - 1].Value = data[0].ToString();
                    dg[1, dg.RowCount - 1].Value = data[1].ToString();
                    dg[2, dg.RowCount - 1].Value = data[2].ToString();
                    dg[3, dg.RowCount - 1].Value = data[3].ToString();
                }
            }
        }

        //обработчик принятого пакета
        private void Serial_Port_Read_Thread()
        {
            uint sword = 0;
            byte[] buffer = new byte[32];       //MAX SIZE of packet - 32 bytes
            int err_cnt = 0, grid_cnt = 0;
            StatusType Status;
            ErrorType Error;

            while (this.serialPort1.IsOpen)    //close the thread when serial port is closed
            {
                uint b = 0;
                try                            //check unexpected external disconnection 
                {
                    b = (uint)this.serialPort1.ReadByte();
                }
                catch (Exception)              //catch unauthorized access exception if connection was closed
                {
                    MessageBox.Show("COM-Порт закрыт или устройство не отвечает", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;                     //close the thread          
                }

                sword >>= 8;                   //right shift sync word - LSB first
                sword |= (b << 24);            //last received byte is MSB 

                if (sword == magic_32)         //check start word
                {
                    Thread.Sleep(20);
                    this.serialPort1.Read(buffer, (int)TrxPacketFields.Address, 28);
                    if ((AddressType)(buffer[(int)TrxPacketFields.Address] & 0xF0) == AddressType.pc)     //rx address -> PC
                    {
                        if ((AddressType)(buffer[(int)TrxPacketFields.Address] & 0x0F) == AddressType.tdrv) //tx address -> driver board
                        {
                            driver_data_packs_ok++;
                            SetFormData(textBox30, new object[] { driver_data_packs_ok }, false);

                            CommandType Command = (CommandType)buffer[(int)TrxPacketFields.Type];         //check type
                            if (Command.Equals(CommandType.Unused)) continue;

                            switch (Command)
                            {
                                case CommandType.GetElectData:
                                    try
                                    {
                                        Int16 Max_current = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 0);
                                        Int16 Max_voltage = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 2);
                                        Int16 Vel_o = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 4);
                                        Int16 Vel_wint_o = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 6);
                                        Int16 Vel_c = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 8);
                                        Int16 Vel_wint_c = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 10);
                                        Int16 Open_percent = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 12);
                                        Int16 Reverse = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 14);
                                        Int16 Is_hold = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 16);
                                        SetFormData(textBox5, new object[] { Max_current }, false);
                                        SetFormData(textBox3, new object[] { Max_voltage }, false);
                                        SetFormData(textBox24, new object[] { Vel_o }, false);
                                        SetFormData(textBox2, new object[] { Vel_wint_o }, false);
                                        SetFormData(textBox9, new object[] { Vel_c }, false);
                                        SetFormData(textBox8, new object[] { Vel_wint_c }, false);
                                        SetFormData(textBox6, new object[] { Open_percent }, false);
                                        if (Reverse > 0) SetFormData(radioButton8, new object[] { true }, false);
                                        else SetFormData(radioButton7, new object[] { true }, false);
                                        if (Is_hold > 0) SetFormData(radioButton6, new object[] { true }, false);
                                        else SetFormData(radioButton5, new object[] { true }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetMechData:
                                    try
                                    {
                                        Int16 Open_delay = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 0);
                                        Int16 Max_delay = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 2);
                                        Int16 Max_delay_wint = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 4);
                                        Int16 Max_delay_fire = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 6);
                                        Int16 Is_lock = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 8);
                                        Int16 Fire_job = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 10);
                                        Int16 Batt_job = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 12);
                                        Int16 Lock_type = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 14);
                                        SetFormData(textBox25, new object[] { Open_delay }, false);
                                        SetFormData(textBox11, new object[] { Max_delay }, false);
                                        SetFormData(textBox13, new object[] { Max_delay_wint }, false);
                                        SetFormData(textBox14, new object[] { Max_delay_fire }, false);
                                        if (Is_lock > 0) SetFormData(radioButton2, new object[] { true }, false);
                                        else SetFormData(radioButton4, new object[] { true }, false);
                                        SetFormData(comboBox1, new object[] { Fire_job }, false);
                                        SetFormData(comboBox2, new object[] { Batt_job }, false);
                                        SetFormData(comboBox3, new object[] { Lock_type }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetErrorCount:
                                    err_cnt = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 0);
                                    break;
                                case CommandType.GetSystemData:
                                    try
                                    {
                                        int curr = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 0);
                                        float athres = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 4);
                                        int pos = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 8);
                                        int vel = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 12);
                                        int acc = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 16);
                                        float e = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 20);
                                        athres = Math.Sign(curr) * athres;
                                        SetFormData(this.chart1, new object[] { curr, athres, pos, vel, acc, e }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetServiceParam:
                                    try
                                    {
                                        byte dyn_c = buffer[(int)TrxPacketFields.Data + 0];
                                        byte thold = buffer[(int)TrxPacketFields.Data + 1];
                                        byte crack_a = buffer[(int)TrxPacketFields.Data + 2];
                                        byte crack_w = buffer[(int)TrxPacketFields.Data + 3];
                                        UInt16 dturn = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 4);
                                        UInt16 dcurr = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 6);
                                        UInt16 ctres_a = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 8);
                                        UInt16 ctres_c = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 10);
                                        UInt16 elength = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 12);
                                        UInt16 epos = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 14);
                                        UInt16 max_err = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 16);
                                        UInt16 calib_delay = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 18);
                                        UInt16 barriers = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 20);
                                        SetFormData(textBox20, new object[] { dyn_c }, false);
                                        SetFormData(textBox18, new object[] { thold }, false);
                                        SetFormData(textBox17, new object[] { crack_a }, false);
                                        SetFormData(textBox16, new object[] { crack_w }, false);
                                        SetFormData(textBox7, new object[] { dturn }, false);
                                        SetFormData(textBox1, new object[] { dcurr }, false);
                                        SetFormData(textBox10, new object[] { ctres_a }, false);
                                        SetFormData(textBox12, new object[] { ctres_c }, false);
                                        SetFormData(textBox15, new object[] { elength }, false);
                                        SetFormData(textBox23, new object[] { epos }, false);
                                        SetFormData(textBox4, new object[] { max_err }, false);
                                        SetFormData(textBox19, new object[] { calib_delay }, false);
                                        if (barriers > 0) SetFormData(radioButton10, new object[] { true }, false);
                                        else SetFormData(radioButton9, new object[] { true }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetErrorData:
                                        grid_cnt++;

                                        string err = "";
                                        Error = (ErrorType)buffer[(int)TrxPacketFields.Data + 0];
                                    switch (Error)
                                        {
                                            case ErrorType.NoMotorData:
                                                err = "нет данных на мотор";
                                                break;
                                            case ErrorType.InVoltageLow:
                                                err = "низкое входное напряжение";
                                            break;
                                            case ErrorType.InVoltageNegative:
                                                err = "отрицательное входное напряжение";
                                                break;
                                            case ErrorType.AccVoltageNegative:             
                                                err = "отрицательное напряжение аккумулятора";
                                                break;
                                            case ErrorType.Overcurrent:
                                                err = "превышение по току";
                                                break;
                                            case ErrorType.PositionUnreached:
                                                err = "невозможно достичь заданную позицию";
                                                break;
                                            case ErrorType.CalibrationFailed:
                                                err = "ошибка алгоритма калибровки";
                                                break;
                                            case ErrorType.NoDataInFlash:
                                                err = "память драйвера пуста";
                                                break;
                                            case ErrorType.FlashEraseFailed:
                                                err = "память драйвера не отвечает";
                                                break;
                                            case ErrorType.ADCUnunit:
                                                err = "ошибка инициализации АЦП";
                                                break;
                                            case ErrorType.ADCConvertionFailed:
                                                err = "ошибка преобразования АЦП";
                                                break;
                                            case ErrorType.DMAUninit:
                                                err = "ошибка инициализации прямого доступа к памяти";
                                                break;
                                            case ErrorType.I2CUninit:
                                                err = "ошибка инициализации интерфейса резервного хранилища";
                                                break;
                                            case ErrorType.TimPWMUninit:
                                                err = "ошибка инициализации ШИМ";
                                                break;
                                            case ErrorType.TimEncUninit:
                                                err = "ошибка инициализации энкодера";
                                                break;
                                            case ErrorType.TimCycleUninit:
                                                err = "ошибка инициализации таймера";
                                                break;
                                            case ErrorType.UartUninit:
                                                err = "ошибка инициализации консоли";
                                                break;
                                            default:
                                                break;
                                        }

                                        string st = "";
                                        Status = (StatusType)buffer[(int)TrxPacketFields.Data + 0]; 
                                    switch (Status)
                                        {
                                            case StatusType.SystemError:
                                                st = "системная ошибка";
                                                break;
                                            case StatusType.FatalError:
                                                st = "критическая ошибка";
                                                break;
                                            default:
                                                break;
                                        }
                                            
                                        SetFormData(dataGridView1, new object[] { grid_cnt, time, err, st }, false);
                                        if (grid_cnt == err_cnt) grid_cnt = 0;
                                    break;
                                default:
                                    this.serialPort1.DiscardInBuffer();
                                    driver_data_packs_lost++;
                                    SetFormData(textBox31, new object[] { driver_data_packs_lost }, false);
                                    break;
                            }    //end of case
                        }        //end of rx address -> driver board
                    }            //end of rx address -> PC
                }
            }
        }
        
        //команда на чтение параметров платы драйвера
        private void Read_mdata_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;                            
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetElectData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение дпараметров платы мастера
        private void Read_mech_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetMechData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на чтение сервисных параметров
        private void Read_time_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetServiceParam;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
                     
        //команда на запись данных о моторе
        private void Write_mdata_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetElectData;

            try
            {
                UInt16 amax = Convert.ToUInt16(textBox5.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                UInt16 umax = Convert.ToUInt16(textBox3.Text);
                b = BitConverter.GetBytes(umax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 2, b.Length);

                UInt16 scfo = Convert.ToUInt16(textBox24.Text);
                b = BitConverter.GetBytes(scfo);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                UInt16 scfwo = Convert.ToUInt16(textBox2.Text);
                b = BitConverter.GetBytes(scfwo);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 6, b.Length);

                UInt16 scfc = Convert.ToUInt16(textBox9.Text);
                b = BitConverter.GetBytes(scfc);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                UInt16 scfwc = Convert.ToUInt16(textBox8.Text);
                b = BitConverter.GetBytes(scfwc);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 10, b.Length);

                UInt16 perc = Convert.ToUInt16(textBox6.Text);
                b = BitConverter.GetBytes(perc);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);

                UInt16 rev = (UInt16)(radioButton8.Checked ? 1 : radioButton7.Checked ? 0 : 0);
                b = BitConverter.GetBytes(rev);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 14, b.Length);

                UInt16 hold = (UInt16)(radioButton6.Checked ? 1 : radioButton5.Checked ? 0 : 0);
                b = BitConverter.GetBytes(hold);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 16, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
           

            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на запись данных о механической системе - только крайние положения
        private void Write_mech_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetMechData;

            try
            {
                UInt16 odel = Convert.ToUInt16(textBox11.Text);
                b = BitConverter.GetBytes(odel);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                UInt16 del = Convert.ToUInt16(textBox11.Text);
                b = BitConverter.GetBytes(del);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 2, b.Length);

                UInt16 delw = Convert.ToUInt16(textBox13.Text);
                b = BitConverter.GetBytes(delw);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                UInt16 delf = Convert.ToUInt16(textBox14.Text);
                b = BitConverter.GetBytes(delf);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 6, b.Length);

                UInt16 lck = (UInt16)(radioButton2.Checked ? 1 : radioButton4.Checked ? 0 : 0);
                b = BitConverter.GetBytes(lck);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                UInt16 fire = Convert.ToUInt16(comboBox1.SelectedItem);
                b = BitConverter.GetBytes(fire);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 10, b.Length);

                UInt16 batt = Convert.ToUInt16(comboBox2.SelectedItem);
                b = BitConverter.GetBytes(batt);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);

                UInt16 ltype = Convert.ToUInt16(comboBox3.SelectedItem);
                b = BitConverter.GetBytes(ltype);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 14, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на запись сервисных параметров
        private void Write_time_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetServiceParam;

            try
            {
                float dyn_coef = Convert.ToSingle(textBox20.Text);
                b = BitConverter.GetBytes(dyn_coef);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);           

                byte hold_period = Convert.ToByte(textBox18.Text);
                b = BitConverter.GetBytes(hold_period);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 1, b.Length);

                byte crack_a = Convert.ToByte(textBox17.Text);
                b = BitConverter.GetBytes(crack_a);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 2, b.Length);

                byte crack_w = Convert.ToByte(textBox16.Text);
                b = BitConverter.GetBytes(crack_w);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 3, b.Length);

                UInt16 turn_delay = Convert.ToUInt16(textBox7.Text);
                b = BitConverter.GetBytes(turn_delay);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                UInt16 hold_current = Convert.ToUInt16(textBox1.Text);
                b = BitConverter.GetBytes(hold_current);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 6, b.Length);

                UInt16 current_thres_a = Convert.ToUInt16(textBox10.Text);
                b = BitConverter.GetBytes(current_thres_a);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                UInt16 current_thres_c = Convert.ToUInt16(textBox12.Text);
                b = BitConverter.GetBytes(current_thres_c);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 10, b.Length);

                UInt16 length_error = Convert.ToUInt16(textBox15.Text);
                b = BitConverter.GetBytes(length_error);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);

                UInt16 pos_error = Convert.ToUInt16(textBox23.Text);
                b = BitConverter.GetBytes(pos_error);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 14, b.Length);

                UInt16 max_error = Convert.ToUInt16(textBox4.Text);
                b = BitConverter.GetBytes(max_error);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 16, b.Length);

                UInt16 calib_time = Convert.ToUInt16(textBox19.Text);
                b = BitConverter.GetBytes(calib_time);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 18, b.Length);

                UInt16 barriers = (byte)(radioButton10.Checked ? 1 : radioButton9.Checked ? 0 : 0);
                b = BitConverter.GetBytes(barriers);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 20, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на чтение текущих системных показателей
        private void Read_datalog_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetSystemData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда калибровки
        private void Calib_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetCalibrate;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда открыть двери
        private void Open_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetDoorOpen;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда закрыть дверь
        private void Close_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetDoorClose;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение лога ошибок
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetErrorData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение числа ошибок
        private void Read_ErrCnt_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetErrorCount;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        private void chart1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button.Equals(MouseButtons.Right))
            {
                this.contextMenuStrip1.SetBounds(e.Location.X, e.Location.Y, this.contextMenuStrip1.Width, this.contextMenuStrip1.Height);
                this.contextMenuStrip1.Show();
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[0].Enabled = s.Checked;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[1].Enabled = s.Checked;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[2].Enabled = s.Checked;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            time++;
        }

        private void element_click(object sender, MouseEventArgs e)
        {
            time = 0;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[3].Enabled = s.Checked;
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[4].Enabled = s.Checked;
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            s.Checked = !s.Checked;
            this.chart1.Series[5].Enabled = s.Checked;
        }
        //масштабирование графика
        private void tabControl1_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.tabControl1.SelectedTab == this.tabControl1.TabPages[1])
            {
                if (e.Control)
                {
                    if (e.KeyCode.Equals(Keys.Oemplus)) this.chart1.Scale(new SizeF(2, 2));    //rewrite it
                    else if (e.KeyCode.Equals(Keys.OemMinus)) this.chart1.Scale(new SizeF(0.5f, 0.5f));
                }
            }
        }
    }
}

