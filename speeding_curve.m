nom_vel = [15:2:35];     %%nominal (calibration) velocity

%%cycle
for i = 1:length(nom_vel) 
    figure(nom_vel(i)); hold on;
    for k = 1:6
        scf_vel = 1 / k;
        PID_S_lim = nom_vel(i) / scf_vel;
        PID_S_out = [-PID_S_lim:1:PID_S_lim]; 
        PID_V_lim = zeros(1, length(PID_S_out));
        for j = 1:length(PID_S_out)
            PID_V_lim(j) = (-scf_vel*PID_S_out(j)*PID_S_out(j)) + (nom_vel(i)*abs(PID_S_out(j))) + 240;
            if (PID_V_lim(j) > 1440) PID_V_lim(j) = 1440;
            end
        end
        
        plot(PID_S_out, PID_V_lim); 
    end
    hold off;
end