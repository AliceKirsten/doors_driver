// ���������� ������, �������� ������, ������ �� ������ ������ ���� ����������
// ����� - ����� ��������� � �������� crc16
#include <stdint.h>
#include "msg_types.h"
#include "get_packet_from_q.h"
#include "crc16.h"

void get_packet_from_q(packet_build_t *p, uint32_t time)
{
	QUEUE_VAR_TYPE n;
	packet_struct_t *b = (packet_struct_t*)&p->buf;
	uint32_t *pp = 0;
	uint32_t i = 0, j = 0;
	uint32_t dl = 0;

	if (p->flag == FP_READY) return;

	while(1){
		switch(p->st){
			case BUF_ZERO:{ // ��������� ���������, ������������� ���. ����, ������� ������� ������.
				p->n = 0;
				p->flag = FP_NO;
				n = get_data_size_queue(p->q);
				if (n == 0) return;
				p->st = COPY_DATA;
				break;
            }//case BUF_ZERO
			
			case WAIT_DATA:{ // ����� �������� �������� �������, ������� ������� ���������� ����� ������
				n = get_data_size_queue(p->q);
				if (n >= PACKET_SIZE - p->n){
					p->st = COPY_DATA;
					break;
				} else {
					if (p->t <= time) p->st = BUF_ZERO;
					return;
				}
			}//case WAIT_DATA
			
			case COPY_DATA:{ // �������� ������ �� ������� � ������������� ����� ������.
				n = get_data_size_queue(p->q);
				//if (n == 0)
				if ( n >= PACKET_SIZE - p->n ){
					dl = PACKET_SIZE - p->n;
				} else {
					dl = n;
				}
				((queue_buf_t*)(p->q))->r_len = dl;
				((queue_buf_t*)(p->q))->r_buf = &p->buf[ p->n ];
				pop_data_queue( p->q );
				p->n = p->n + dl;
				if (p->n == PACKET_SIZE){
					p->st = PREAMBLE_CHECK;
					break;
				} else {
					p->t = time + PACKET_TIME_OUT_BUILD;
					p->st = WAIT_DATA;
					return;
				}
			}//case COPY_DATA
			
			case PREAMBLE_CHECK:{ // ��������� ������� ���������, ��� ������������ ����� ��������� � ������.
				if (b->preamble == PREAMBLE_32_BIT){ 
					p->st = CRC_CHECK; 
					break;
				}else{
					// ���������� ����� ����������
					for (i=1; i<PACKET_SIZE - sizeof(PREAMBLE_32_BIT); i++){
						pp = (uint32_t*)&p->buf[ i ];
						if (*pp == PREAMBLE_32_BIT){
							for (j=0; j<PACKET_SIZE - i; j++){p->buf[ j ] = p->buf[ j + i ];}
							p->n = PACKET_SIZE - i;
							p->st = WAIT_DATA;
							return;
						}
					}
					//��������� ��������� 4 ����� � ����� � buf, ����� ��������� ����� ����� ������� � ��������� � ������.
					for (j=0; j<sizeof(PREAMBLE_32_BIT); j++){p->buf[ j ] = p->buf[ j + PACKET_SIZE - sizeof(PREAMBLE_32_BIT) ];}
					p->n = sizeof(PREAMBLE_32_BIT);
					p->st = WAIT_DATA;
    				return;
				}
			}//case PREAMBLE_CHECK
			
			case CRC_CHECK:{ // �������� ����������� �����.
				if (p->fcrc == FCRC_DISABLE){ 
                                    p->st = BUF_ZERO;
                                    p->flag = FP_READY; 
                                    return;
                                }
				if (crc16(p->buf, PACKET_SIZE) == 0){
					p->st = BUF_ZERO;
					p->flag = FP_READY;
					p->t = time + PACKET_TIME_OUT_PUT;
					return;
				} else {
					p->flag = FP_NO;
					p->st = BUF_ZERO;
					return;
				}
			}//case CRC_CHECK

		}// switch
	} // while
}
