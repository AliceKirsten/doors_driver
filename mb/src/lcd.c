// LCD OLED 128x64
#include "stm32f745xx.h"
#include "main.h"
#include "lcd.h"
#include "printf_hal.h"
#include "rcc_745.h" 
#include "dma_745.h"
#include "gpio_745.h"
#include "spi_745.h"
#include "string.h"
#include "font_rus_5x7.h"
#include "time_hal.h"


uint8_t lcd_buf_p[ LCD_PAGES * LCD_RES_X ];// ��������� ����� (����������)
uint8_t lcd_buf_n[ LCD_PAGES * LCD_RES_X ];// ��������� ����� (����������)

const uint8_t LCD_INIT_CMD_0[] = {
    0xfd,0x12, /*Command Lock */
    0xae,      /*Set Display Off */
    0xd5,0xa0, /*set Display Clock Divide Ratio/Oscillator Frequency */
    0xa8,0x3f, /*Set Multiplex Ratio */
    0x3d,0x00, /*Set Display Offset*/
    0x40,      /*Set Display Start Line*/
    0xa1,      /*Set Segment Re-Map*/
    0xc8,      /*Set COM Output Scan Direction*/
    0xda,0x12, /*Set COM Pins Hardware Configuration*/
    0x81,0xdf, /*Set Current Control */
    0xd9,0x82, /*Set Pre-Charge Period */
    0xdb,0x34, /*Set VCOMH Deselect Level */
    0xa4,      /*Set Entire Display On/Off */
    0xa6,      /*Set Normal/Inverse Display*/
    0x20,      // Horizontal addressing mode
    0x00,

};

const uint8_t LCD_INIT_CMD_1[] = {
	0xaf,			/*Set Display On */
};

// ����� ������ ��������� ������ ����� ������� ���������� ������
//const uint8_t  LCD_REFRESH_CMD[]= {
//  0x22,     // Set Page Address
//  0x00,     // start
//  0x07,     // stop

//  0x21,		// Set Column Address
//  0x00,		// start
//  0x7f,     // stop
//};

volatile lcd_j_t lcd_j;

//    HAL_GPIO_WritePin(OLED_DC_GPIO_Port, OLED_DC_Pin, GPIO_PIN_RESET); // D/~C = 0, Command
//    HAL_GPIO_WritePin(OLED_DC_GPIO_Port, OLED_DC_Pin, GPIO_PIN_SET); // D/~C = 1, Data


//------------------------------------------------------------------------------
// ��������� ��������� LCD
//------------------------------------------------------------------------------
void lcd_init(void)
{
    
    dma_cr_reg_ut dma_cfg;
    spi_cr1_reg_ut spi_cr1;
    spi_cr2_reg_ut spi_cr2;

    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);  // GPIOA CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);  // GPIOB CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOGEN);  // GPIOG CLK EN
    rcc_clk_en(&RCC->APB2ENR, RCC_APB2ENR_SPI1EN);   // SPI1 CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_DMA2EN);   // DMA2 CLK EN

    gpio_init(OLED_CLK_PORT,  OLED_CLK,   GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF5_SPI1);
    gpio_init(OLED_MOSI_PORT, OLED_MOSI,  GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF5_SPI1);
//    gpio_init(OLED_MISO_PORT, OLED_MISO,  GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF5_SPI1); // �� ���������

    gpio_set_1(OLED_CS_PORT,     OLED_CS_L);
    gpio_set_1(OLED_RES_PORT,    OLED_RES_L);
    gpio_set_0(OLED_DH_CL_PORT,  OLED_DH_CL);
    gpio_set_1(OLED_PWR_ON_PORT, OLED_PWR_ON_L);
    
    gpio_init(OLED_CS_PORT,     OLED_CS_L,   GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(OLED_RES_PORT,    OLED_RES_L,  GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(OLED_DH_CL_PORT,  OLED_DH_CL,  GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(OLED_PWR_ON_PORT, OLED_PWR_ON_L, GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    
// SPI Init --------------------------------------------------------------------    
    //spi_cr1.data = 0;
    spi_cr1.reg.cpha     = 0; // 0: The first clock transition is the first data capture edge
    spi_cr1.reg.cpol     = 0; // 0: CK to 0 when idle
    spi_cr1.reg.mstr     = 0; // 1: Master configuration 
    spi_cr1.reg.br       = 5; // APB2(108MHz)/64 (101: fPCLK/64) = 1 MHz
    spi_cr1.reg.spe      = 0; // 0: Peripheral disable 
    spi_cr1.reg.lsbfirst = 0; // 0: data is transmitted / received with the MSB first
    spi_cr1.reg.ssi      = 0; // ???Internal slave select
    spi_cr1.reg.ssm      = 1; // Software NSS management (SSM = 1),  0: Software slave management disabled
    spi_cr1.reg.rxonly   = 0; // 0: Full-duplex (Transmit and receive)
    spi_cr1.reg.crcl     = 0; // 0: 8-bit CRC length
    spi_cr1.reg.crcnext  = 0; // 0: Next transmit value is from Tx buffer.
    spi_cr1.reg.crcen    = 0; // 0: CRC calculation disabled
    spi_cr1.reg.bidioe   = 0; // 0: Output disabled (receive-only mode)  
    spi_cr1.reg.bidimode = 0; // 0: 2-line unidirectional data mode selected 
    
    SPI1->CR1 = spi_cr1.data;
    SPI1->CR1 |= SPI_CR1_MSTR; // 1: Master configuration 
    
    
    //spi_cr2.data = 0;
    spi_cr2.reg.rxdmaen = 0; // 0: Rx buffer DMA disabled
    spi_cr2.reg.txdmaen = 0; // 0: Tx buffer DMA disabled
    spi_cr2.reg.ssoe    = 1; // 1: SS output is enabled in master mode and when the SPI interface is enabled.
    spi_cr2.reg.nssp    = 0; // 0: No NSS pulse
    spi_cr2.reg.frf     = 0; // 0: SPI Motorola mode
    spi_cr2.reg.errie   = 0; // 0: Error interrupt is masked
    spi_cr2.reg.rxneie  = 0; // 0: RXNE interrupt masked
    spi_cr2.reg.txeie   = 0; // 0: TXE interrupt masked  
    spi_cr2.reg.ds      = 7; // Data size 0111: 8-bit
    spi_cr2.reg.frxth   = 1; //??? 0: RXNE event is generated if the FIFO level is greater than or equal to 1/2 (16-bit) 
    spi_cr2.reg.ldma_rx = 0; // 1: Number of data to transfer is odd
    spi_cr2.reg.ldma_tx = 0; // 1: Number of data to transfer is odd

    SPI1->CR2 = spi_cr2.data;
    
   
// DMA Init --------------------------------------------------------------------    
    dma_cfg.reg.en = 0;    // 0: stream disabled
    dma_cfg.reg.dmeie=1;   // 1: DME interrupt enabled
    dma_cfg.reg.teie=1;    // 1: TE interrupt enabled
    dma_cfg.reg.htie=0;    // 0: HT interrupt disabled
    dma_cfg.reg.tcie=1;    // 1: TC interrupt enabled
    dma_cfg.reg.pfctrl=0;  // peripheral flow controller(0: DMA is the flow controller)
    dma_cfg.reg.dir=1;     // 01: memory-to-peripheral
    dma_cfg.reg.circ=0;    // 0: circular mode disabled
    dma_cfg.reg.pinc=0;    // peripheral increment mode(0 - peripheral address pointer is fixed)
    dma_cfg.reg.minc=1;    // memory increment mode(memory address pointer is incremented after each data transfer)
    dma_cfg.reg.psize=0;   // peripheral data size(byte (8-bit))
    dma_cfg.reg.msize=0;   // memory data size (byte (8-bit))
    dma_cfg.reg.pincos=0;  // peripheral increment offset size
    dma_cfg.reg.pl=0;      // priority level=00
    dma_cfg.reg.dbm=0;     // double-buffer mode = 0
    dma_cfg.reg.ct=0;      // current target (only in double-buffer mode)
    dma_cfg.reg.pburst=0;  // single transfer
    dma_cfg.reg.mburst=0;  // single transfer
    dma_cfg.reg.chsel= 3;  // ch 3

    dma_init(DMA2_Stream3,          // DMA stream
             (uint32_t)&(SPI1->DR), // adr reg
             0,                     // adr ram 0
             0,                     // adr ram 1
             0,                     // size
             dma_cfg.data,          // dma config
             0                      // dma fifo config
             );
    
    SPI1->CR2 |= SPI_CR2_TXDMAEN; // enable DMA - SPI1
    SPI1->CR1 |= SPI_CR1_SPE;     // enable SPI1

    NVIC_SetPriority(DMA2_Stream3_IRQn, 5);
    NVIC_EnableIRQ(DMA2_Stream3_IRQn);
    
    lcd_j.lcd_st = LCD_INIT;
    lcd_j.lcd_tx = LCD_READY;

    gpio_set_1(OLED_PWR_ON_PORT, OLED_PWR_ON_L); // Enable +12v    
    
    gpio_set_1(OLED_CS_PORT, OLED_CS_L); // CS = 1
    //delay_ms(100); // delay 100 ms
    gpio_set_0(OLED_RES_PORT, OLED_RES_L); // ENABLE reset LCD
    delay_ms(10); // delay 10 ms
    gpio_set_1(OLED_RES_PORT, OLED_RES_L); // DISABLE reset LCD
    delay_ms(10); // delay 10 ms

    gpio_set_0(OLED_DH_CL_PORT, OLED_DH_CL); // D/~C = 0, Command
    gpio_set_0(OLED_CS_PORT, OLED_CS_L); // CS = 0
    
    lcd_j.lcd_tx = LCD_BUSY;
    
    dma_set_ram(DMA2_Stream3, (uint32_t)&LCD_INIT_CMD_0, 0, sizeof(LCD_INIT_CMD_0));
    dma_on(DMA2_Stream3);
    
    while(lcd_j.lcd_tx == LCD_BUSY);
    
    //gpio_set_1(OLED_PWR_ON_PORT, OLED_PWR_ON_L); // Enable +12v
    delay_ms(10); // delay 10 ms
    
    lcd_j.lcd_tx = LCD_BUSY;

    dma_set_ram(DMA2_Stream3, (uint32_t)&LCD_INIT_CMD_1, 0, sizeof(LCD_INIT_CMD_1));
    dma_on(DMA2_Stream3);
    
    while(lcd_j.lcd_tx == LCD_BUSY);
    
    delay_ms(10); // delay 10 ms
    
    gpio_set_1(OLED_CS_PORT, OLED_CS_L); // CS = 1
}


//------------------------------------------------------------------------------
// ����� �� LCD ������ ����
//------------------------------------------------------------------------------
void lcd_refresh(void)
{
    static uint8_t n = 0; // ���������� ����� ���������� ������
    uint32_t buf = 0;
    
    if (n == 0) buf = (uint32_t)&lcd_buf_p[0]; else buf = (uint32_t)&lcd_buf_n[0];
    
    n++;
    n = n & 1; // ����������� �� ����� ����� 

    if (lcd_j.lcd_tx == LCD_BUSY ) return;
    
    gpio_set_0(OLED_CS_PORT, OLED_CS_L); // CS = 0
    gpio_set_1(OLED_DH_CL_PORT, OLED_DH_CL); // D/~C = 1, Data

    lcd_j.lcd_st = LCD_END;
    lcd_j.lcd_tx = LCD_BUSY;
    dma_set_ram(DMA2_Stream3, buf, 0, LCD_PAGES * LCD_RES_X + 4);
    dma_on(DMA2_Stream3);
}


//------------------------------------------------------------------------------
// ����� ������ � �������� ����� (128*64 �����) ��� (128*8 ����)
// ��� ������, ������� �� �������.
// ������ ����� - ������� 
// ������ ����� - ��� ������� !
//
//
// pos_x - 0..127  - �������
// pos_y - 0..7    - ������
// inv   - �������� ��������� ������, 0 - ��� ��������, >0 - ��������
// blink - �������, 0 - ��� �������, >0 - �������
// *str  - ��������� ������
//
//
//
//
//
//------------------------------------------------------------------------------
void print_str( uint8_t pos_x, uint8_t pos_y, uint8_t inv, uint8_t blink, const char *str)
{
    size_t pos = 0, q; 
    size_t j;
    uint8_t c;
    
    if ( pos_x > LCD_RES_X - 1) return;
    if ( pos_y > LCD_PAGES - 1) return;
    
    pos = pos_x + pos_y * LCD_RES_X;
    
    while(*str != 0){
        for (j=0;j<5;j++){
            //lcd_buf[ pos ] = font_5x7[ (*str - 32) * 5 + j ];
            q = *str;
            if (q >= 0xc0) q = q - 0x40;
            c = font_5x7[ (q - 32) * 5 + j ];
            if (inv) c = ~c;
            lcd_buf_p[ pos ] = c;
            if (blink) lcd_buf_n[ pos ] = ~c; else lcd_buf_n[ pos ] = c;
            pos++;
        }
        str++;
        
        if (inv) lcd_buf_p[ pos ] = 0xff; else lcd_buf_p[ pos ] = 0;
        if (inv || blink) lcd_buf_n[ pos ] = 0xff; else lcd_buf_n[ pos ] = 0;
        pos++;
        if((pos & 127) == 126) pos += 2;
    }
}

//------------------------------------------------------------------------------
// ������� ������ ������
//------------------------------------------------------------------------------
void lcd_clear(void)
{
    memset(lcd_buf_p, 0, LCD_PAGES * LCD_RES_X);
    memset(lcd_buf_n, 0, LCD_PAGES * LCD_RES_X);
}
