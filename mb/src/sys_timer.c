#include "main.h"
#include <stdint.h>
#include "sys_timer.h"
#include "rcc_745.h"
#include "tim_6_7_745.h"
//#include "iwdg.h"

//extern IWDG_HandleTypeDef hiwdg;

//#pragma location="ccmram"
static volatile uint32_t sys_tick_counter    = 0;  // ������� ���������� �������
//#pragma location="ccmram"
static volatile uint32_t tick_sec_counter    = 0;  // ������� ������
//#pragma location="ccmram"
static volatile uint32_t tick_1000ms_counter = 0;  // ������� 1000�� ����������

//------------------------------------------------------------------------------
// ��������� �������
// T period = 1 ms
//------------------------------------------------------------------------------
void sys_timer_init(void)
{
    rcc_clk_en(&RCC->APB1ENR, RCC_APB1ENR_TIM7EN); // TIM7 CLK EN
    
    tim_6_7_cr1_reg_ut tim7_cr1;
    
    // APB1 - 108 MHz / 2000 = 54000
    TIM7->ARR  = 108000000 / 2000;
    TIM7->CNT  = 0;
    TIM7->DIER = TIM_DIER_UIE; // Update interrupt enable
    TIM7->EGR  = TIM_EGR_UG;   // UG: Update generation
    TIM7->PSC  = 1;            // The counter clock frequency CK_CNT is equal to fCK_PSC / (PSC[15:0] + 1).
    
    tim7_cr1.data = 0;
    tim7_cr1.reg.cen  = 1; // 1: Counter enabled
    tim7_cr1.reg.udis = 0; // 0: UEV enabled. 
    tim7_cr1.reg.urs  = 1; // 1: Only counter overflow/underflow generates an update interrupt or DMA request if enabled.
    tim7_cr1.reg.opm  = 0; // 0: Counter is not stopped at update event 
    tim7_cr1.reg.arpe = 0; // 0: TIMx_ARR register is not buffered.
    tim7_cr1.reg.uifremap = 0;// 0: No remapping. UIF status bit is not copied to TIMx_CNT register bit 31.

    TIM7->CR1 = tim7_cr1.data;
    
    NVIC_SetPriority(TIM7_IRQn, 5);
    NVIC_EnableIRQ(TIM7_IRQn);
}

//------------------------------------------------------------------------------
// ���������� ������� �������� ������� ������
//------------------------------------------------------------------------------
uint32_t time_get_sec_counter( void )
{
	return tick_sec_counter;
}


//------------------------------------------------------------------------------
// ���������� ������� �������� ���������� �������, ������� ms
//------------------------------------------------------------------------------
uint32_t time_get_ms_counter( void )
{
	return sys_tick_counter;
}

//------------------------------------------------------------------------------
// ���������� ���������� �� ���������� �������
//------------------------------------------------------------------------------
void sys_tick_handler( void )
{
	/* Reload IWDG counter */
//	HAL_IWDG_Refresh(&hiwdg);
    sys_tick_counter++;
    tick_1000ms_counter++;
    if (tick_1000ms_counter == 1000){
        tick_1000ms_counter = 0;
        tick_sec_counter++;       // sec + 1
    }
}
