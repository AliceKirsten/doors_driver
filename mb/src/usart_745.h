#ifndef USART_H_
#define USART_H_

typedef struct {
    uint32_t ue:1;
    uint32_t RES0:1;
    uint32_t re:1;
    uint32_t te:1;
    uint32_t idleie:1;
    uint32_t rxneie:1;
    uint32_t tcie:1;
    uint32_t txeie:1;
    uint32_t peie:1;
    uint32_t ps:1;
    uint32_t pce:1;
    uint32_t wake:1;
    uint32_t m0:1;
    uint32_t mme:1;
    uint32_t cmie:1;
    uint32_t over8:1;
    uint32_t dedt:5;
    uint32_t deat:5;
    uint32_t rtoie:1;
    uint32_t eobie:1;
    uint32_t m1:1;
    uint32_t RES1:3;
} usart_cr1_reg_t;

typedef union {
    uint32_t data;
    usart_cr1_reg_t reg;
} usart_cr1_reg_ut;


typedef struct {
    uint32_t RES0:4;
    uint32_t addm7:1;
    uint32_t lbdl:1;
    uint32_t lbdie:1;
    uint32_t RES1:1;
    uint32_t lbcl:1;
    uint32_t cpha:1;
    uint32_t cpol:1;
    uint32_t clken:1;
    uint32_t stop:2;
    uint32_t linen:1;
    uint32_t swap:1;
    uint32_t rxinv:1;
    uint32_t txinv:1;
    uint32_t datainv:1;
    uint32_t msbfirst:1;
    uint32_t abren:1;
    uint32_t abrmod:2;
    uint32_t rtoen:1;
    uint32_t add_l:4;
    uint32_t add_h:4;
} usart_cr2_reg_t;

typedef union {
    uint32_t data;
    usart_cr2_reg_t reg;
} usart_cr2_reg_ut;


typedef struct {
    uint32_t eie:1;
    uint32_t iren:1;
    uint32_t irlp:1;
    uint32_t hdsel:1;
    uint32_t nack:1;
    uint32_t scen:1;
    uint32_t dmar:1;
    uint32_t dmat:1;
    uint32_t rtse:1;
    uint32_t ctse:1;
    uint32_t ctsie:1;
    uint32_t onebit:1;
    uint32_t ovrdis:1;
    uint32_t ddre:1;
    uint32_t dem:1;
    uint32_t dep:1;
    uint32_t RES0:1;
    uint32_t scarcnt0:1;
    uint32_t scarcnt1:1;
    uint32_t scarcnt2:1;
    uint32_t RES1:12;
} usart_cr3_reg_t;

typedef union {
    uint32_t data;
    usart_cr3_reg_t reg;
} usart_cr3_reg_ut;



typedef struct {
    uint32_t psc:8;
    uint32_t gt:8;
    uint32_t RES:16;
} usart_gtpr_reg_t;

typedef union {
    uint32_t data;
    usart_gtpr_reg_t reg;
} usart_gtpr_reg_ut;



typedef struct {
    uint32_t rto:24;
    uint32_t blen:8;
} usart_rtor_reg_t;

typedef union {
    uint32_t data;
    usart_rtor_reg_t reg;
} usart_rtor_reg_ut;




typedef struct {
    uint32_t abrrq:1;
    uint32_t sbkrq:1;
    uint32_t mmrq:1;
    uint32_t rxfrq:1;
    uint32_t txfrq:1;
    uint32_t RES:27;
} usart_rqr_reg_t;

typedef union {
    uint32_t data;
    usart_rqr_reg_t reg;
} usart_rqr_reg_ut;


#endif