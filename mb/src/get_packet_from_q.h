#ifndef PACKET_CHECK_H_
#define PACKET_CHECK_H_

#include "msg_types.h"
#include "queue_buf.h"

#define PACKET_TIME_OUT_BUILD (50)   // 50 ms - ������� �� ����� ������
#define PACKET_TIME_OUT_PUT (1000)   // 1 s - ������� �� �������� ������, �� ��������� ������� ����� ����� ������

// ��������� �������� 
typedef enum {
	BUF_ZERO = 0,     // ��������� ���������, ������������� ���. ����, ������� ������� ������.
	WAIT_DATA,        // ����� �������� �������� �������, ������� ������� ���������� ����� ������
	COPY_DATA,        // �������� ������ �� ������� � ������������� ����� ������. ����� �����.
	PREAMBLE_CHECK,   // ��������� ������� ���������, ��� ������������ ����� ��������� � ������.
	CRC_CHECK         // �������� ����������� �����.
} packet_build_state_t;

// ���� ������� ������ ������ � ������
typedef enum {
	FP_NO = 0,
	FP_READY           // ����� ������, �������� � ����� � ����������� ���������.
} flag_packet_ready_t;

// ���� �������� CRC
typedef enum {
	FCRC_DISABLE = 0,
	FCRC_ENABLE           // ���������� �������� CRC � ������.
} flag_crc_check_t;

// ���������� ���������
typedef struct
{
//	queue_buf_t *q;                // ��������� �� ������� �� ������� ���������� ������
	void *q;                // ��������� �� ������� �� ������� ���������� ������
	packet_build_state_t st;       // ��������� ��������
	uint32_t n;                    // ���������� ������ � ������������� ������
	uint8_t buf[ PACKET_SIZE ];    // ������������� ���. � ��� ���������� �����
    uint32_t t;                    // �����, �������� ����� ��������
	flag_packet_ready_t flag;      // ���� - ���������� ������, ����� ����������� ���������� � �.�.
	flag_crc_check_t fcrc;         // ���� ���������� �������� CRC � ������ ��-���
} packet_build_t;


void get_packet_from_q(packet_build_t *p, uint32_t time);



#endif
