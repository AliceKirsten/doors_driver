#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f745xx.h"
    
/** @defgroup GPIO_pins_define GPIO pins define
  * @{
  */
#define GPIO_PIN_0                 ((uint16_t)0x0001U)  /* Pin 0 selected    */
#define GPIO_PIN_1                 ((uint16_t)0x0002U)  /* Pin 1 selected    */
#define GPIO_PIN_2                 ((uint16_t)0x0004U)  /* Pin 2 selected    */
#define GPIO_PIN_3                 ((uint16_t)0x0008U)  /* Pin 3 selected    */
#define GPIO_PIN_4                 ((uint16_t)0x0010U)  /* Pin 4 selected    */
#define GPIO_PIN_5                 ((uint16_t)0x0020U)  /* Pin 5 selected    */
#define GPIO_PIN_6                 ((uint16_t)0x0040U)  /* Pin 6 selected    */
#define GPIO_PIN_7                 ((uint16_t)0x0080U)  /* Pin 7 selected    */
#define GPIO_PIN_8                 ((uint16_t)0x0100U)  /* Pin 8 selected    */
#define GPIO_PIN_9                 ((uint16_t)0x0200U)  /* Pin 9 selected    */
#define GPIO_PIN_10                ((uint16_t)0x0400U)  /* Pin 10 selected   */
#define GPIO_PIN_11                ((uint16_t)0x0800U)  /* Pin 11 selected   */
#define GPIO_PIN_12                ((uint16_t)0x1000U)  /* Pin 12 selected   */
#define GPIO_PIN_13                ((uint16_t)0x2000U)  /* Pin 13 selected   */
#define GPIO_PIN_14                ((uint16_t)0x4000U)  /* Pin 14 selected   */
#define GPIO_PIN_15                ((uint16_t)0x8000U)  /* Pin 15 selected   */
#define GPIO_PIN_All               ((uint16_t)0xFFFFU)  /* All pins selected */
#define GPIO_PIN_MASK              ((uint32_t)0x0000FFFFU) /* PIN mask for assert test */

/**
  * @brief  Get the FLASH Latency.
  * @retval FLASH Latency
  *          The value of this parameter depend on device used within the same series
  */
#define __HAL_FLASH_GET_LATENCY()     (READ_BIT((FLASH->ACR), FLASH_ACR_LATENCY))

    

#define QUEUE_CONSOL_RX_SIZE	(128)// ������ �������� ������ �������

#define TASK_LCDREF_PRIORITY   ( tskIDLE_PRIORITY + 1 )
#define TASK_HARDPOST_PRIORITY ( tskIDLE_PRIORITY + 2 )
#define TASK_INPSENS_PRIORITY  ( tskIDLE_PRIORITY + 1 )
#define TASK_ROUTER_PRIORITY   ( tskIDLE_PRIORITY + 2 )
#define TASK_CONSOLE_PRIORITY  ( tskIDLE_PRIORITY + 1 )
#define TASK_DRV_PRIORITY      ( tskIDLE_PRIORITY + 2 )
#define TASK_LOG_PRIORITY      ( tskIDLE_PRIORITY + 1 )

void Error_Handler(void);

// USART1 Debug console
#define USART1_PORT (GPIOA)
#define USART1_RX (1 << 10)// bit
#define USART1_TX (1 << 9) // bit

// USART2 - PC
#define USART2_PORT_RX (GPIOA) 
#define USART2_RX      (1 << 3)
#define USART2_PORT_TX (GPIOD)
#define USART2_TX      (1 << 5)

// USART3 - DRV
#define USART3_PORT_RX (GPIOD) 
#define USART3_RX      (1 << 9)
#define USART3_PORT_TX (GPIOB)
#define USART3_TX      (1 << 10)


// SPI1 - OLED
#define OLED_CS_PORT  GPIOA
#define OLED_CLK_PORT GPIOA

#define OLED_RES_PORT  GPIOB
#define OLED_MOSI_PORT GPIOB

#define OLED_PWR_ON_PORT GPIOG
#define OLED_DH_CL_PORT  GPIOG

#define OLED_CS_L   (1 << 4)  // PA4
#define OLED_CLK    (1 << 5)  // PA5
#define OLED_RES_L  (1 << 3)  // PB3 
#define OLED_MOSI   (1 << 5)  // PB5
#define OLED_PWR_ON_L (1 << 14) // PG14
#define OLED_DH_CL  (1 << 13) // PG13





/* Private defines -----------------------------------------------------------*/
//#define SCK_RF_Pin GPIO_PIN_2
//#define SCK_RF_GPIO_Port GPIOE
//#define RF_IRQ_Pin GPIO_PIN_3
//#define RF_IRQ_GPIO_Port GPIOE
//#define CS_RF_Pin GPIO_PIN_4
//#define CS_RF_GPIO_Port GPIOE
//#define MISO_RF_Pin GPIO_PIN_5
//#define MISO_RF_GPIO_Port GPIOE
//#define MOSI_RF_Pin GPIO_PIN_6
//#define MOSI_RF_GPIO_Port GPIOE
//#define RF_TST_Pin GPIO_PIN_13
//#define RF_TST_GPIO_Port GPIOC

#define RDR2_L           (1 << 14)
#define RDR2_L_PORT      GPIOC
#define RDR1_L           (1 << 15)
#define RDR1_L_PORT      GPIOC
#define PDU_CLOSE_L      (1 << 0)
#define PDU_CLOSE_L_PORT GPIOF
#define PDU_AUTO_L       (1 << 1)
#define PDU_AUTO_L_PORT  GPIOF
#define PDU_EXIT_L       (1 << 2)
#define PDU_EXIT_L_PORT  GPIOF
#define PDU_WINTER_L     (1 << 3)
#define PDU_WINTER_L_PORT GPIOF
#define PDU_OPEN_L       (1 << 4)
#define PDU_OPEN_L_PORT  GPIOF
#define PDU_STOP_L       (1 << 5)
#define PDU_STOP_L_PORT  GPIOF
#define PDU_TEST_L       (1 << 8)
#define PDU_TEST_L_PORT  GPIOF
#define FIRE_L           (1 << 10)
#define FIRE_L_PORT      GPIOF
#define BAR1_L           (1 << 8)
#define BAR1_L_PORT      GPIOB
#define BAR2_L           (1 << 1)
#define BAR2_L_PORT      GPIOE

#define PG_VOUT_H        (1 << 1)
#define PG_VOUT_H_PORT   GPIOB
#define V20_ENA_H        (1 << 2)
#define V20_ENA_H_PORT   GPIOB


#define DR_NRST_PORT     GPIOD
#define DR_NRST          (1<<8)

/*
#define BUZZ_H_Pin GPIO_PIN_9
#define BUZZ_H_GPIO_Port GPIOF
#define DO_MEM_Pin GPIO_PIN_2
#define DO_MEM_GPIO_Port GPIOC
#define DI_MEM_Pin GPIO_PIN_3
#define DI_MEM_GPIO_Port GPIOC
#define OLED_CS_LOW_Pin GPIO_PIN_4
#define OLED_CS_LOW_GPIO_Port GPIOA
#define I_LOCK_Pin GPIO_PIN_0
#define I_LOCK_GPIO_Port GPIOB
#define ENA_DRV_H_Pin GPIO_PIN_11
#define ENA_DRV_H_GPIO_Port GPIOF
#define DR_RX_UART3_Pin GPIO_PIN_10
#define DR_RX_UART3_GPIO_Port GPIOB
#define DR_NRST_Pin GPIO_PIN_8
#define DR_NRST_GPIO_Port GPIOD
#define DR_TX_UART3_Pin GPIO_PIN_9
#define DR_TX_UART3_GPIO_Port GPIOD
#define SCK_MEM_Pin GPIO_PIN_3
#define SCK_MEM_GPIO_Port GPIOD
#define KEY_B0_L_Pin GPIO_PIN_9
#define KEY_B0_L_GPIO_Port GPIOG
#define KEY_B1_L_Pin GPIO_PIN_10
#define KEY_B1_L_GPIO_Port GPIOG
#define KEY_B2_L_Pin GPIO_PIN_11
#define KEY_B2_L_GPIO_Port GPIOG
#define KEY_B3_L_Pin GPIO_PIN_12
#define KEY_B3_L_GPIO_Port GPIOG
#define OLED_DC_Pin GPIO_PIN_13
#define OLED_DC_GPIO_Port GPIOG
#define OH_ENA_Pin GPIO_PIN_14
#define OH_ENA_GPIO_Port GPIOG
#define RES_OLED_LOW_Pin GPIO_PIN_3
#define RES_OLED_LOW_GPIO_Port GPIOB
#define CS_MEM_Pin GPIO_PIN_4
#define CS_MEM_GPIO_Port GPIOB
#define I2C_MAC_PWR_H_Pin GPIO_PIN_9
#define I2C_MAC_PWR_H_GPIO_Port GPIOB
#define TIM_IRQ_L_Pin GPIO_PIN_0
#define TIM_IRQ_L_GPIO_Port GPIOE
*/


#ifdef __cplusplus
}
#endif

/** 
  * @brief   AF 0 selection  
  */ 
#define GPIO_AF0_RTC_50Hz      ((uint8_t)0x00U)  /* RTC_50Hz Alternate Function mapping                       */
#define GPIO_AF0_MCO           ((uint8_t)0x00U)  /* MCO (MCO1 and MCO2) Alternate Function mapping            */
#define GPIO_AF0_SWJ           ((uint8_t)0x00U)  /* SWJ (SWD and JTAG) Alternate Function mapping             */
#define GPIO_AF0_TRACE         ((uint8_t)0x00U)  /* TRACE Alternate Function mapping                          */

/** 
  * @brief   AF 1 selection  
  */ 
#define GPIO_AF1_TIM1          ((uint8_t)0x01U)  /* TIM1 Alternate Function mapping */
#define GPIO_AF1_TIM2          ((uint8_t)0x01U)  /* TIM2 Alternate Function mapping */

/** 
  * @brief   AF 2 selection  
  */ 
#define GPIO_AF2_TIM3          ((uint8_t)0x02U)  /* TIM3 Alternate Function mapping */
#define GPIO_AF2_TIM4          ((uint8_t)0x02U)  /* TIM4 Alternate Function mapping */
#define GPIO_AF2_TIM5          ((uint8_t)0x02U)  /* TIM5 Alternate Function mapping */

/** 
  * @brief   AF 3 selection  
  */ 
#define GPIO_AF3_TIM8          ((uint8_t)0x03U)  /* TIM8 Alternate Function mapping  */
#define GPIO_AF3_TIM9          ((uint8_t)0x03U)  /* TIM9 Alternate Function mapping  */
#define GPIO_AF3_TIM10         ((uint8_t)0x03U)  /* TIM10 Alternate Function mapping */
#define GPIO_AF3_TIM11         ((uint8_t)0x03U)  /* TIM11 Alternate Function mapping */
#define GPIO_AF3_LPTIM1        ((uint8_t)0x03U)  /* LPTIM1 Alternate Function mapping */
#define GPIO_AF3_CEC           ((uint8_t)0x03U)  /* CEC Alternate Function mapping */
/** 
  * @brief   AF 4 selection  
  */ 
#define GPIO_AF4_I2C1          ((uint8_t)0x04U)  /* I2C1 Alternate Function mapping */
#define GPIO_AF4_I2C2          ((uint8_t)0x04U)  /* I2C2 Alternate Function mapping */
#define GPIO_AF4_I2C3          ((uint8_t)0x04U)  /* I2C3 Alternate Function mapping */
#define GPIO_AF4_I2C4          ((uint8_t)0x04U)  /* I2C4 Alternate Function mapping */
#define GPIO_AF4_CEC           ((uint8_t)0x04U)  /* CEC Alternate Function mapping */

/** 
  * @brief   AF 5 selection  
  */ 
#define GPIO_AF5_SPI1          ((uint8_t)0x05U)  /* SPI1 Alternate Function mapping        */
#define GPIO_AF5_SPI2          ((uint8_t)0x05U)  /* SPI2/I2S2 Alternate Function mapping   */
#define GPIO_AF5_SPI3          ((uint8_t)0x05U)  /* SPI3/I2S3 Alternate Function mapping   */
#define GPIO_AF5_SPI4          ((uint8_t)0x05U)  /* SPI4 Alternate Function mapping        */
#define GPIO_AF5_SPI5          ((uint8_t)0x05U)  /* SPI5 Alternate Function mapping        */
#define GPIO_AF5_SPI6          ((uint8_t)0x05U)  /* SPI6 Alternate Function mapping        */

/** 
  * @brief   AF 6 selection  
  */ 
#define GPIO_AF6_SPI3          ((uint8_t)0x06U)  /* SPI3/I2S3 Alternate Function mapping  */
#define GPIO_AF6_SAI1          ((uint8_t)0x06U)  /* SAI1 Alternate Function mapping       */

/** 
  * @brief   AF 7 selection  
  */ 
#define GPIO_AF7_USART1        ((uint8_t)0x07U)  /* USART1 Alternate Function mapping     */
#define GPIO_AF7_USART2        ((uint8_t)0x07U)  /* USART2 Alternate Function mapping     */
#define GPIO_AF7_USART3        ((uint8_t)0x07U)  /* USART3 Alternate Function mapping     */
#define GPIO_AF7_UART5         ((uint8_t)0x07U)  /* UART5 Alternate Function mapping      */
#define GPIO_AF7_SPDIFRX       ((uint8_t)0x07U)  /* SPDIF-RX Alternate Function mapping   */
#define GPIO_AF7_SPI2          ((uint8_t)0x07U)  /* SPI2 Alternate Function mapping       */
#define GPIO_AF7_SPI3          ((uint8_t)0x07U)  /* SPI3 Alternate Function mapping       */

/** 
  * @brief   AF 8 selection  
  */ 
#define GPIO_AF8_UART4         ((uint8_t)0x08U)  /* UART4 Alternate Function mapping  */
#define GPIO_AF8_UART5         ((uint8_t)0x08U)  /* UART5 Alternate Function mapping  */
#define GPIO_AF8_USART6        ((uint8_t)0x08U)  /* USART6 Alternate Function mapping */
#define GPIO_AF8_UART7         ((uint8_t)0x08U)  /* UART7 Alternate Function mapping  */
#define GPIO_AF8_UART8         ((uint8_t)0x08U)  /* UART8 Alternate Function mapping  */
#define GPIO_AF8_SPDIFRX       ((uint8_t)0x08U)  /* SPIDIF-RX Alternate Function mapping */
#define GPIO_AF8_SAI2          ((uint8_t)0x08U)  /* SAI2 Alternate Function mapping   */

/** 
  * @brief   AF 9 selection 
  */ 
#define GPIO_AF9_CAN1          ((uint8_t)0x09U)  /* CAN1 Alternate Function mapping    */
#define GPIO_AF9_CAN2          ((uint8_t)0x09U)  /* CAN2 Alternate Function mapping    */
#define GPIO_AF9_TIM12         ((uint8_t)0x09U)  /* TIM12 Alternate Function mapping   */
#define GPIO_AF9_TIM13         ((uint8_t)0x09U)  /* TIM13 Alternate Function mapping   */
#define GPIO_AF9_TIM14         ((uint8_t)0x09U)  /* TIM14 Alternate Function mapping   */
#define GPIO_AF9_QUADSPI       ((uint8_t)0x09U)  /* QUADSPI Alternate Function mapping */
/** 
  * @brief   AF 10 selection  
  */ 
#define GPIO_AF10_OTG_FS        ((uint8_t)0xAU)  /* OTG_FS Alternate Function mapping */
#define GPIO_AF10_OTG_HS        ((uint8_t)0xAU)  /* OTG_HS Alternate Function mapping */
#define GPIO_AF10_QUADSPI       ((uint8_t)0xAU)  /* QUADSPI Alternate Function mapping */
#define GPIO_AF10_SAI2          ((uint8_t)0xAU)  /* SAI2 Alternate Function mapping */

/** 
  * @brief   AF 11 selection  
  */ 
#define GPIO_AF11_ETH           ((uint8_t)0x0BU)  /* ETHERNET Alternate Function mapping */
   
/** 
  * @brief   AF 12 selection  
  */ 
#define GPIO_AF12_FMC           ((uint8_t)0xCU)  /* FMC Alternate Function mapping                      */
#define GPIO_AF12_OTG_HS_FS     ((uint8_t)0xCU)  /* OTG HS configured in FS, Alternate Function mapping */
#define GPIO_AF12_SDMMC1        ((uint8_t)0xCU)  /* SDMMC1 Alternate Function mapping                   */
   
/** 
  * @brief   AF 13 selection  
  */ 
#define GPIO_AF13_DCMI          ((uint8_t)0x0DU)  /* DCMI Alternate Function mapping */

/** 
  * @brief   AF 15 selection  
  */ 
#define GPIO_AF15_EVENTOUT      ((uint8_t)0x0FU)  /* EVENTOUT Alternate Function mapping */


#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
