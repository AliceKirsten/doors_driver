// DMA for STM32F745
#include "stm32f745xx.h"



//------------------------------------------------------------------------------
// Init DMA
//------------------------------------------------------------------------------
void dma_init(DMA_Stream_TypeDef * ch, uint32_t adr_hard, uint32_t adr_ram0, uint32_t adr_ram1, uint32_t size, uint32_t cfg, uint32_t cfg_fifo)
{
    ch->CR = 0;
    ch->PAR = adr_hard;
    ch->M0AR = adr_ram0;
    ch->M1AR = adr_ram1;
    ch->NDTR = size;
    ch->FCR = cfg_fifo;
    ch->CR = cfg;
}

//------------------------------------------------------------------------------
// DMA set adr ram + size DMA
//------------------------------------------------------------------------------
void dma_set_ram(DMA_Stream_TypeDef * ch, uint32_t adr_ram0, uint32_t adr_ram1, uint32_t size)
{
    ch->NDTR = size;
    ch->M0AR = adr_ram0;
    ch->M1AR = adr_ram1;
}

//------------------------------------------------------------------------------
//dma on
//------------------------------------------------------------------------------
void dma_on(DMA_Stream_TypeDef * ch)
{
    ch->CR |= (uint32_t) 1;
}

//------------------------------------------------------------------------------
// dma off
//------------------------------------------------------------------------------
void dma_off(DMA_Stream_TypeDef * ch)
{
    ch->CR &= ~((uint32_t)1);
}