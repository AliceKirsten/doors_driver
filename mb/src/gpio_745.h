#ifndef GPIO_H_
#define GPIO_H_

typedef enum {
    GPIO_MODE_IN = 0,      // 00:Input mode (reset state)
    GPIO_MODE_OUT = 1,     // 01: General purpose output mode
    GPIO_MODE_AF=2,       // 10: Alternate function mode
    GPIO_MODE_AN=3,       // 11: Analog mode
} gpio_mode_t;

typedef enum {
    GPIO_OTYPE_PUSHPULL=0, // 0: Output push-pull (reset state)
    GPIO_OTYPE_OPENDRAIN=1,// 1: Output open-drain
} gpio_otype_t;

typedef enum {
    GPIO_SPEEDF_LOW=0,     // 00: Low speed
    GPIO_SPEEDF_MED=1,     // 01: Medium speed
    GPIO_SPEEDF_HIGH=2,    // 10: High speed
    GPIO_SPEEDF_VERYHIGH=3,// 11: Very high speed
} gpio_speed_t;

typedef enum {
    GPIO_PULL_NO=0,   // 00: No pull-up, pull-down
    GPIO_PULL_UP=1,   // 01: Pull-up
    GPIO_PULL_DOWN=2, // 10: Pull-down
                      // 11: Reserved
} gpio_pull_t;

void gpio_init(GPIO_TypeDef *port, uint16_t pin_bit, gpio_mode_t mode, gpio_otype_t otype, gpio_speed_t speed, gpio_pull_t pull, uint8_t af);
//#pragma inline=forced 
void gpio_set_0(GPIO_TypeDef *port, uint16_t pin_bit);
//#pragma inline=forced 
void gpio_set_1(GPIO_TypeDef *port, uint16_t pin_bit);
//#pragma inline=forced 
uint32_t gpio_get(GPIO_TypeDef *port, uint16_t pin_bit);

#endif