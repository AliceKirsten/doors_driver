#ifndef LCD_H_
#define LCD_H_

#define LCD_RES_X (128)
#define LCD_RES_Y (64)
#define LCD_PAGES (8)


typedef enum { LCD_INIT = 0, /*LCD_REFRESH,*/ LCD_END } lcd_state_t;
typedef enum { LCD_READY = 0, LCD_BUSY } lcd_tx_t;

typedef struct {
    lcd_state_t lcd_st; // ������� ��������� - ���� � ��� �������� ������
    lcd_tx_t lcd_tx;    // ������� ��������� ��������.
} lcd_j_t;


void lcd_init(void);
void lcd_refresh(void);
void lcd_clear(void);
void print_str( uint8_t pos_x, uint8_t pos_y, uint8_t inv, uint8_t blink, const char *str);
#endif