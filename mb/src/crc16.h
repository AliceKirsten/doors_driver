#ifndef CRC16_H_
#define CRC16_H_

#include <stdint.h>

uint16_t crc16( uint8_t *adr, uint32_t len);
uint16_t ms_crc16( uint8_t *msg_p, uint32_t length);


#endif
