//------------------------------------------------------------------------------
// ������: ���������� ��������������� ����������� �������
// 2019 ���.
//
//
//
//
//
//------------------------------------------------------------------------------
#include "main.h"
#include "os.h"

#include "git_commit.h"
#include "queue_buf.h"
#include "printf_hal.h"
#include "msg_types.h"
#include "version.h"
#include "msg_types.h"
#include "get_packet_from_q.h"
#include "get_packet_from_osq.h"
#include "lcd.h"
#include "gpio_745.h"
#include "rcc_745.h"
#include "dma_745.h"
#include "hardware.h"
#include "sys_timer.h"
#include "time_hal.h"

#include "string.h"

extern xQueueHandle xPakTDrvToRouter;             // ������� ������ TASK Drv -> Router
extern xQueueHandle xPakTLogToRouter;             // ������� ������ TASK Log -> Router


const char txt_device_ver_soft[] = {"SV:"};	        // ������ �����
const char txt_device_ver_hard[] = {"HV:0.1.19"};	// ������ ������
const char txt_device_name[]     = {"DOOR"};     	// ��������� �������� ����������

uint8_t console_rx_char;     // ������ �������� � �������
uint8_t console_tx_char;     // ������ ������������ � �������

queue_buf_t q_consol_rx;
uint8_t consol_rx_buf[ QUEUE_CONSOL_RX_SIZE ];

#define CHAR_BUF_SIZE (256)
uint8_t char_buf[ CHAR_BUF_SIZE ];// ����� ��� sprintf

#define QUEUE_CONSOL_TX_BUF_SIZE (8192)
uint8_t consol_tx_buf[ QUEUE_CONSOL_TX_BUF_SIZE ]; // ����� ��� ������ ��������� �� �������
queue_buf_t q_consol_tx;
volatile uint8_t flag_consol_tx = 0;  // ���� ���� �������� = 1, =0 ��� ��������.
volatile QUEUE_VAR_TYPE consol_tx_size = 0; // ���������� ������������ ���� �� DMA

// ����� ������ � �������� ������ �� ����� ��������
#define QBUF_FROM_DRV_SIZE (5 * sizeof(packet_struct_t))
uint8_t qbuf_from_drv[ QBUF_FROM_DRV_SIZE ];
#define QBUF_TO_DRV_SIZE (5 * sizeof(packet_struct_t))
uint8_t qbuf_to_drv[ QBUF_TO_DRV_SIZE ];
queue_buf_t q_from_drv;
queue_buf_t q_to_drv;
volatile uint8_t uart_drv_flag_tx = 0; // ���� ���� �������� = 1, =0 ��� ��������.
volatile QUEUE_VAR_TYPE uart_drv_tx_size = 0; // ���������� ������������ ���� �� DMA

#define BUF_FROM_DRV_SIZE (sizeof(packet_struct_t))
uint8_t buf_from_drv[ BUF_FROM_DRV_SIZE ]; // ����� DMA RX

// ����� ������ � �������� ������ ��/� ��
#define QBUF_FROM_PC_SIZE (5 * sizeof(packet_struct_t))
uint8_t qbuf_from_pc[ QBUF_FROM_PC_SIZE ];
#define QBUF_TO_PC_SIZE (5 * sizeof(packet_struct_t))
uint8_t qbuf_to_pc[ QBUF_TO_PC_SIZE ];
queue_buf_t q_from_pc;
queue_buf_t q_to_pc;
volatile uint8_t uart_pc_flag_tx = 0; // ���� ���� �������� = 1, =0 ��� ��������.
volatile QUEUE_VAR_TYPE uart_pc_tx_size = 0; // ���������� ������������ ���� �� DMA

// ������������� �������� ����� ��� ������ ������ �� DMA �� PC
#define BUF_FROM_PC_SIZE (sizeof(packet_struct_t))
uint8_t buf_from_pc[ BUF_FROM_PC_SIZE ]; // ����� DMA RX

volatile status_sensors_t cur_send_st; // ���������� ��������� ������� ��������


packet_build_t packet_from_pc;
packet_build_t packet_from_drv;

packet_build_t packet_from_tdrv;
packet_build_t packet_from_tlog;

//device_config_t device_config; // ��������� �������
device_config_t device_config = {50,    //speed oprn 
                                 50,    //speed close
                                 3,     //delay open
                                 0,     //delay close
                                 0,     //lock pos
                                 1,     //reaction fire
                                 10,    //delay open fire
                                 1,     //batt job num
                                 1,     //lock type num
                                 1,     //motor stop
                                 20,    //motor I max
                                 40,    //motor U max
                                 20,    //speed open winter
                                 20,    //speed close winter
                                 1,     //delay open winter
                                 80,    //open width winter
                                 1,     //end position inversion
                                 200,   //max position error
                                 10,    //max error count
                                 1,     //calib delay
                                 1};    //barriers active

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

//******************************************************************************
// �������� � DRV, ������� �� DMA
//******************************************************************************
void uart_drv_tx_dma( void )
{
    uint8_t *adr_start = 0;

    if (DMA1_Stream3->CR & 1) return; 
    if (uart_drv_flag_tx) return;

    if (q_to_drv.in == q_to_drv.out) return;

    adr_start = &qbuf_to_drv[ q_to_drv.out ];
    
    if ( q_to_drv.in > q_to_drv.out) 
        uart_drv_tx_size = q_to_drv.in - q_to_drv.out;
    else 
        uart_drv_tx_size =  QBUF_TO_PC_SIZE - q_to_drv.out;

    uart_drv_flag_tx = 1;  // ���� - �������� ������
    dma_set_ram(DMA1_Stream3, (uint32_t)adr_start, 0, uart_drv_tx_size);
    dma_on(DMA1_Stream3);
}


//******************************************************************************
// �������� � ��, ������� �� DMA
//******************************************************************************
void uart_pc_tx_dma( void )
{
    uint8_t *adr_start = 0;

    if (DMA1_Stream6->CR & 1) return; 
    if (uart_pc_flag_tx) return;

    if (q_to_pc.in == q_to_pc.out) return;

    adr_start = &qbuf_to_pc[ q_to_pc.out ];
    
    if ( q_to_pc.in > q_to_pc.out) 
        uart_pc_tx_size = q_to_pc.in - q_to_pc.out;
    else 
        uart_pc_tx_size =  QBUF_TO_PC_SIZE - q_to_pc.out;

    uart_pc_flag_tx = 1;  // ���� - �������� ������
    dma_set_ram(DMA1_Stream6, (uint32_t)adr_start, 0, uart_pc_tx_size);
    dma_on(DMA1_Stream6);
}

//******************************************************************************
// ������� �������� ������� �� DMA
//******************************************************************************
void consol_tx_dma( void )
{
    uint8_t *adr_start = 0;

    if (DMA2_Stream7->CR & 1) return; 
    if (flag_consol_tx) return;

    if (q_consol_tx.in == q_consol_tx.out) return;

    adr_start = &consol_tx_buf[ q_consol_tx.out ];
    
    if ( q_consol_tx.in > q_consol_tx.out) 
        consol_tx_size = q_consol_tx.in - q_consol_tx.out;
    else 
        consol_tx_size =  QUEUE_CONSOL_TX_BUF_SIZE - q_consol_tx.out;

    flag_consol_tx = 1;  // ���� - �������� ������
    dma_set_ram(DMA2_Stream7, (uint32_t)adr_start, 0, consol_tx_size);
    dma_on(DMA2_Stream7);
}


//******************************************************************************
// �������� ������ ����� � ���� (�������)
// - ������ ������ � ����� � ��������� �������� !
//******************************************************************************
void tx_byte(uint8_t ch)
{
    while( get_free_size_queue( &q_consol_tx ) <= 1){ // ������� ����� ����������� �����
        if (flag_consol_tx == 0) consol_tx_dma();
    }
    
    push_data_queue_b( &q_consol_tx, ch);
    if (flag_consol_tx == 0) consol_tx_dma();
}


//******************************************************************************
//MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN
//******************************************************************************
int main(void)
{
    /* Set Interrupt Group Priority */
    NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    /* Init the low level hardware */
    rcc_clk_en(&RCC->APB1ENR, RCC_APB1ENR_PWREN);
    rcc_clk_en(&RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN);

    /* System interrupt init*/
    /* PendSV_IRQn interrupt configuration */
    NVIC_SetPriority(PendSV_IRQn, 15);
    
    /* Configure the system clock */
    SystemClock_Config();
    
    // ���� ��������� ����� ��� �������
    q_consol_rx.queue = consol_rx_buf;
    q_consol_rx.len   = QUEUE_CONSOL_RX_SIZE;
    reset_queue( &q_consol_rx );
    
    q_consol_tx.queue = consol_tx_buf;
    q_consol_tx.len   = QUEUE_CONSOL_TX_BUF_SIZE;
    reset_queue( &q_consol_tx );
    
    // ���� ��������� ����� ��� DRV
    q_from_drv.queue = qbuf_from_drv;
    q_from_drv.len   = QBUF_FROM_DRV_SIZE;
    reset_queue( &q_from_drv );
    
    q_to_drv.queue = qbuf_to_drv;
    q_to_drv.len   = QBUF_TO_DRV_SIZE;
    reset_queue( &q_to_drv );
    
    // ���� ��������� ����� ��� PC
    q_from_pc.queue = qbuf_from_pc;
    q_from_pc.len   = QBUF_FROM_PC_SIZE;
    reset_queue( &q_from_pc );
    
    q_to_pc.queue = qbuf_to_pc;
    q_to_pc.len   = QBUF_TO_PC_SIZE;
    reset_queue( &q_to_pc );

    // ��������� ����������� ������� �� ������� - ����� �� ��
    packet_from_pc.st = BUF_ZERO;
    packet_from_pc.q = &q_from_pc;
    packet_from_pc.fcrc = FCRC_DISABLE;
    packet_from_pc.flag = FP_NO;

    // ��������� ����������� ������� �� ������� - ����� �� ����� ��������
    packet_from_drv.st = BUF_ZERO;
    packet_from_drv.q = &q_from_drv;
    packet_from_drv.fcrc = FCRC_DISABLE;
    packet_from_drv.flag = FP_NO;

    // ��������� ����������� ������� �� ������� - ������ Task Drv
    packet_from_tdrv.st = BUF_ZERO;
    packet_from_tdrv.q = &xPakTDrvToRouter;
    packet_from_tdrv.fcrc = FCRC_DISABLE;
    packet_from_tdrv.flag = FP_NO;

    // ��������� ����������� ������� �� ������� - ������ Task Log
    packet_from_tlog.st = BUF_ZERO;
    packet_from_tlog.q = &xPakTLogToRouter;
    packet_from_tlog.fcrc = FCRC_DISABLE;
    packet_from_tlog.flag = FP_NO;
    
    dat_init(); // ��������� ������ ������� �������� � ���
    
    sys_timer_init(); // My timer for delay & second counter (TIM7 - 1 ms)

    xfunc_out = tx_byte;           // ��������� ��������� �� ������� ������ (��� xprintf.c)
    
    uart_console_init(); // usart1
    uart_pc_init();      // usart2
    uart_drv_init();     // usart3
    
    lcd_clear();
    lcd_init();  // ��������� ������������� LCD
    
//------------------------------------------------------------------------------------------------------------
// �������� �������� �� ���������, ���� ���. �������� ����� ����������� �� I2C ������ !
//    device_config.speed_open            = 10;  // �������� ��������   (��/���)
//    device_config.speed_close           = 10;  // �������� ��������  (��/���)
//    device_config.delay_open            = 5;   // �������� � �������� ��������� (���)
//    device_config.lock_pos    = LOCK_POS_CLOSE;// ��������� ����� � ������ "������ �����" (��-���)
//    device_config.reaction_fire_input   = 1;   // ������� �� �������� ���� 1,2,3 - ���� �� ���������� � ��
//    device_config.delay_open_fire       = 10;  // �������� � �������� ��������� �� ������� ��������� ����� (���)
//    device_config.batt_job_n            = 1;   // ������ � �������������  1,2,3,4 - ������ �� ��
//    device_config.lock_type_num         = 1;   // ��� ����� 1,2,3
//
//    device_config.braking_force         = 15;  // ������ ��� ����������  (%)
//    device_config.start_force           = 15;  // ������ ��� ������      (%)
//    device_config.len_slowdown_opening  = 20;  // ������ ���� ���������� ��� �������� (��)
//    device_config.len_slowdown_closing  = 20;  // ������ ���� ���������� ��� �������� (��)
//    device_config.motor_stop            = MOTOR_STOP_YES; // ��������� ���������� � �������� ��������� (��-���)
//    device_config.motor_stress_close    = 100; // ������ ��������� ���������� � �������� ��������� 
//    device_config.reverse_opening_force = 10;  // ������ �������������� �� �������� (%)
//    device_config.reverse_closing_force = 10;  // ������ �������������� �� �������� (%)
//    device_config.set_encoder_division  = 500; // ���-�� ������� �������� �� 1 ������ 
//    device_config.motor_i_max           = 3;   // ������ �� ���� ���������
//------------------------------------------------------------------------------------------------------------

    printf_d("\r\n\r\nDevice start.\n");

    printf_d("================================================================================\r\n");
    printf_d(" Soft Version = %s%d.%d.%d\r\n", txt_device_ver_soft, SOFT_VER_H, SOFT_VER_L, SOFT_VER_Y);
    printf_d(" Hard Version = %s\r\n", txt_device_ver_hard);
    printf_d(" Device Name  = %s\r\n", txt_device_name);
    printf_d(" GIT commit   = %s\r\n", git_commit_str);
    printf_d("================================================================================\r\n");

    /* Call init function for freertos objects (in freertos.c) */
    MX_FREERTOS_Init();

    /* Configure the source of time base considering new system clocks settings*/
    HAL_InitTick (TICK_INT_PRIORITY);
    
    /* Start scheduler */
    vTaskStartScheduler();
  
    while (1){
        printf_d("ERROR: OS not started.\n");
    }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_USART6
                              |RCC_PERIPHCLK_UART4|RCC_PERIPHCLK_UART5
                              |RCC_PERIPHCLK_UART7|RCC_PERIPHCLK_I2C1;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Uart4ClockSelection = RCC_UART4CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Uart5ClockSelection = RCC_UART5CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Uart7ClockSelection = RCC_UART7CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
