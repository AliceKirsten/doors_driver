// ������:
// �����������
// ��������� ���������
//
//
//
#include "os.h"
#include "printf_hal.h"
#include "lcd.h"
#include "msg_types.h"
#include "debug.h"
#include <math.h>

#ifndef DEBUG_LOG
#define DEBUG_LOG 0
#endif

extern xQueueHandle xPakRouterToTLog;     // ������� ������ Router -> TASK Log
extern xQueueHandle xPakTLogToRouter;     // ������� ������ TASK Log -> Router
extern xQueueHandle xLog;                 // ������� ������ ��� ������ Task Log (��� ������ ����������� � ������ �������)
extern status_sensors_t cur_send_st;      // ���������� ��������� ������� ��������
extern door_curent_state_t door_current;  // ������� ��������� ������

void door_leaf_string( char *buf, uint8_t pr);


static char str_sprintf[32]; // ����� ��� ����� sprint
static packet_struct_t packet_rx;
static packet_struct_t packet_tx;

void vTaskLog(void const * argument)
{
    uint8_t pr;
    portBASE_TYPE res;
    msg_t msg;
    portTickType xTimeTx;
    
    packet_tx.preamble = PREAMBLE_32_BIT;
    packet_tx.adr_dst  = ADR_PC;
    packet_tx.adr_src  = ADR_TLOG;

    if (DEBUG_LOG) printf_dos("Task Log - START.\n");
    
    xTimeTx = xTaskGetTickCount() + 1000;
    
    lcd_clear();

WHILE_BEGIN

    //print_str( 27, 0, 1, 0, "15-04-19 15:40"); - ���� �� ��������

    //-------------������              ������� ���������               ��������� ��������� - ��������
    if (cur_send_st.rdr1 == I_OFF) print_str( 0,  1, 0, 0, "P1"); else print_str( 0,  1, 1, 0, "P1");
    if (cur_send_st.rdr2 == I_OFF) print_str( 16, 1, 0, 0, "P2"); else print_str( 16, 1, 1, 0, "P2");
    if (cur_send_st.bar1 == I_OFF) print_str( 34, 1, 0, 0, "�1"); else print_str( 34, 1, 1, 0, "�1");
    if (cur_send_st.bar2 == I_OFF) print_str( 50, 1, 0, 0, "�2"); else print_str( 50, 1, 1, 0, "�2");
    if (cur_send_st.stop == I_OFF) print_str( 66, 1, 0, 0, "����"); else print_str( 66, 1, 0, 1, "����");
    if (cur_send_st.fire == I_OFF) print_str( 95, 1, 0, 0, "�����"); else print_str( 95, 1, 0, 1, "�����");
    
  //    print_str( 0, 2, 0, 0, "123456789012345678901");
    //print_str( 0, 2, 0, 0, "������.���:�� �������");
    switch(cur_send_st.pdu.current){
    case I_NONE:// ����� �� ���������
        print_str( 0, 2, 0, 0, "������.���:�� �������");
        break;
    case I_CLOSE:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    case I_AUTO:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    case I_EXIT:// ������ �����
        print_str( 0, 2, 0, 0, "������.���: �����    ");
        break;
    case I_WINTER:// ������
        print_str( 0, 2, 0, 0, "������.���: ������   ");
        break;
    case I_OPEN:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    }
    
    
    print_str( 0, 3, 0, 0, "������� ��: ����     ");
    //print_str( 0, 4, 0, 0, "���: 100%            ");
    //print_str( 0, 5, 0, 0, "������: ��� ������   ");


    //print_str( 0, 6, 0, 0, "�����: ������� 100%  ");
    pr = door_current.pos; // �������� ��������� 0-100%
    if (pr > 100) pr = 100;

    switch(door_current.state){
        case DOOR_OPEN: 
            sprintf(str_sprintf, "�����: �������  %3d%% ", pr);
            break;

        case DOOR_CLOSE:
            sprintf(str_sprintf, "�����: �������  %3d%% ", pr);
            break;

        case DOOR_OPEN_MOVE:
            sprintf(str_sprintf, "�����: �������� %3d%% ", pr);
            break;

        case DOOR_CLOSE_MOVE:
            sprintf(str_sprintf, "�����: �������� %3d%% ", pr);
            break;
            
        case DOOR_CHECK:
            sprintf(str_sprintf, "�����: ��������");
            break;
            
        case DOOR_CALIB:
            sprintf(str_sprintf, "�����: ����������");
            break;
            
       case DOOR_MANUAL:
            sprintf(str_sprintf, "�����: ������");
            break;
            
        case DOOR_ERROR:
            sprintf(str_sprintf, "�����: ������");
            break;

    }
    print_str( 0, 6, 0, 0, str_sprintf);

    //door_current.state = DOOR_CLOSE;    // �������
    //door_current.pos   = 100;           // 100 % - �.�. ���������
    
    //door_current.lock  = LOCK_POS_OPEN; // ����� ������


//  print_str( 0, 2, 0, 0, "123456789012345678901");
//  print_str( 0, 7, 0, 0, "|---------||--------|");
// ����� �������
    switch(door_current.state){
        case DOOR_OPEN: 
        case DOOR_OPEN_MOVE:
            door_leaf_string(str_sprintf, 100 - pr);
            break;

        case DOOR_CLOSE:
        case DOOR_CLOSE_MOVE:
            door_leaf_string(str_sprintf, pr);
            break;
          
        default:
            door_leaf_string(str_sprintf, pr);
            break;

    }
    print_str( 0, 7, 0, 0, str_sprintf);



// ��������� ������� ��������� � ������
    res = xQueueReceive(xLog, &msg, 0); // ���������� ��������� ���� ����, �� ������� !
    if (res == pdPASS){
        if (DEBUG_LOG) printf_dos("TASK LOG: get MSG = %d\n", msg);
    }


    res = xQueueReceive(xPakRouterToTLog, &packet_rx, 0); // ���������� ����� ���� ����, �� ������� !
    if (res == pdPASS){
        if (DEBUG_LOG) printf_dos("TASK LOG: get Packet\n");
    }

    if (xTimeTx <= xTaskGetTickCount()){
        res = xQueueSendToBack( xPakTLogToRouter, &packet_tx, 0);
        if (res != pdPASS){
           if (DEBUG_LOG) printf_dos("ERROR: TLOG: xQueueSendToBack( xPakTLogToRouter, &packet_tx, 0)\n");
        }else{
           if (DEBUG_LOG) printf_dos("TLOG: packet_tx to PC OK\n");
           xTimeTx = xTaskGetTickCount() + 1000;
        }
    }


    vTaskDelay(1);

WHILE_END

}



//-----------------------------------------------------------------------------
// ����� ������� ����� � �������� �����
//-----------------------------------------------------------------------------
void door_leaf_string( char *buf, uint8_t pr)
{
	size_t i = 0;
	size_t j = 0;
	uint8_t l,p;

	l = (uint8_t)nearbyint((pr * 10) / (float)125.0f); // ������ �������, ����������� ������ '='
	p = 8 - l;           // 8 - ������������ ����� ��� 100%
	p = p * 2;           // * 2 �.�. ��� ��������� ��������
    
	//printf("%% = %d\n", pr);
	//printf("l = %d\n", l);
        //printf("p = %d\n", p);

	buf[ i ] = '|'; // ����� ������� ����� �����
	i++;

	// ����� - ������� �����
	for ( j = 0; j<l; j++){
		buf[ i + j ] = '=';
	}
	i = i + l;

	buf[ i ] = '|';
	i++;

	// ������� - ������ ����� ����� ���������
	for ( j = 0; j < p; j++){
		buf[ i + j ] = ' ';
	}
	i = i + p;

	// ����� - ������� ������
	buf[ i ] = '|';
	i++;

	for ( j = 0; j < l; j++){
		buf[ i + j ] = '=';
	}
	i = i + l;

	buf[ i ] = '|';// ������ ������� ����� �����
}