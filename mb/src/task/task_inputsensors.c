/*******************************************************************************
* ������ ������ ������� ��������: 
* �������� 
* �������
* ���������� �������������
* �������� ������������
* ������ ���� � ����
* ������ ����������� ������� �� �������� PG
*******************************************************************************/

#include "os.h"
#include "main.h"
#include "printf_hal.h"
#include "task_inputsensors.h"
#include "hardware.h"
#include "msg_types.h"

#ifndef DEBUG_SENS
#define DEBUG_SENS 0
#endif

#define DEBUG_SENS_LEV 0

xQueueHandle xInSensors;

extern status_sensors_t cur_send_st;   // ���������� ��������� ������� ��������

//------------------------------------------------------------------------------
// ������ ������ ������� ��������.
//------------------------------------------------------------------------------
void vTaskInputSensors(void const * argument)
{
    portTickType xTimePDUcurrent, xTimePDUtimeout;
    pdu_rotary_avtomat_t pdu_rotary_av = R_WAIT_NEW;

    //if (DEBUG_SENS) printf_dos("vTaskInputSensors - start\n");
    
    xInSensors = xQueueCreate(32, sizeof(pdu_state_e));
    if (xInSensors == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xInSensors\n");
    
    cur_send_st.pdu.current = I_NONE;
    cur_send_st.pdu.prev = I_NONE;
    
WHILE_BEGIN
    //if (DEBUG_SENS) printf_dos("Input Sensors:\n");
        
    taskENTER_CRITICAL();
    cur_send_st.pg = get_sensor_power();

    cur_send_st.pdu.prev = cur_send_st.pdu.current;
    cur_send_st.pdu.current = get_pdu_state();
    xQueueSendToBack(xInSensors, &cur_send_st.pdu.current, 0); 
        
    cur_send_st.test = get_key_test_state();

    cur_send_st.stop = get_key_stop_state();

    cur_send_st.rdr1 = get_radar1_state();

    cur_send_st.rdr2 = get_radar2_state();

    cur_send_st.bar1 = get_bar1_state();

    cur_send_st.bar2 = get_bar2_state();

    cur_send_st.fire = get_fire_state();
    taskEXIT_CRITICAL();
        
    //printf_dos("stop = %d\n", cur_send_st.stop);
        
    // ����� �������������� ���������� - ���������� �������������
    switch (pdu_rotary_av){
    case R_WAIT_NEW:{
        if (cur_send_st.pdu.current != cur_send_st.pdu.prev){
            xTimePDUcurrent = xTaskGetTickCount();
            xTimePDUtimeout = xTimePDUcurrent + 1000; // �������� 1 �������
            cur_send_st.pdu.prev = cur_send_st.pdu.current;
            pdu_rotary_av = R_WAIT_TIME;
            if (DEBUG_SENS && DEBUG_SENS_LEV) printf_dos("\npdu.current != pdu.prev\n");
            if (DEBUG_SENS && DEBUG_SENS_LEV) printf_dos("xTimePDUcurrent=%ul\n", xTimePDUcurrent);
            if (DEBUG_SENS && DEBUG_SENS_LEV) printf_dos("xTimePDUtimeout=%ul\n", xTimePDUtimeout);
        }
        break;
    }

    case R_WAIT_TIME:{
        xTimePDUcurrent = xTaskGetTickCount();
        if (cur_send_st.pdu.current == cur_send_st.pdu.prev){
            if(xTimePDUcurrent >= xTimePDUtimeout){
                cur_send_st.pdu.prev = cur_send_st.pdu.current;
                if (DEBUG_SENS) printf_dos("PDU: Set NEW state = %d.\n", cur_send_st.pdu.current);
                pdu_rotary_av = R_WAIT_NEW;
            }
        } else pdu_rotary_av = R_WAIT_NEW;
        break;
    }
        
    } //switch

    vTaskDelay(100);
WHILE_END

}
