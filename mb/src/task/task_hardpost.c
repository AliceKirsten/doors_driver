#include "os.h"
#include "main.h"
#include "printf_hal.h"
#include "hardware.h"
#include "lcd.h"

#include "task_inputsensors.h"
#include "task_router.h"
#include "task_drv.h"
#include "task_log.h"

#ifndef DEBUG_TH
#define DEBUG_TH 0
#endif

extern xQueueHandle xLog;

TaskHandle_t xTaskConsoleHandle;

void vTaskHardPost(void const * argument)
{
    portBASE_TYPE xReturn;
    msg_t msg;

    driver_pcb_reset_init(); // ������������� ����� DR_NRST (����� ����� ��������)
    
    print_str( 27, 0, 0, 0, "�����������");

    
    if (DEBUG_TH) printf_dos("vTaskHardPost - Start.\n");
//    buzz( B_OFF );
    
    driver_pcb_reset(); // ����� ����� ��������

//sensor_power_on(); // ��������� ������� ��������������� ��������
//goto xxx; // PG �� ��������, ���� �����������
//------------------------------------------------------------------------------    
// �������� ��������������� ��� ������� ������� � ��������
    if (DEBUG_TH) printf_dos("Enable power for - RDR + BAR.\n");
    sensor_power_on();
    vTaskDelay(500);
wait_ok:
    if (I_ON != get_sensor_power()){
      //print_str( 0, 2, 0, 0, "123456789012345678901");
        print_str( 0, 1, 0, 0, "������: ��� �������");
        print_str( 0, 2, 0, 0, "�� ������-�������");
        print_str( 0, 3, 0, 0, "---------------------");
        print_str( 0, 4, 0, 1, "�������� ��������� !");
        // �������� �������� ������
//        buzz( B_ON );
        if (DEBUG_TH) printf_dos("ERROR: PG = 0 - KZ\n");
        vTaskDelay(1000);
        goto wait_ok;
    }
//    buzz( B_OFF );
//------------------------------------------------------------------------------    
//xxx:
    if (DEBUG_TH) printf_dos("Hard - Post OK.\n");
    
    // ������ ������ ������ ��������
    if (DEBUG_TH) printf_dos("RUN TASK input sensors.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskInputSensors, 
                          "InpSens",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_INPSENS_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Input sensor.");
    }
    
    // ������ ������ Router - ��������� �������
    if (DEBUG_TH) printf_dos("RUN TASK router.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskRouter, 
                          "Router",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_ROUTER_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Router.");
    }
    
    // ������ ������ ��������
    if (DEBUG_TH) printf_dos("RUN TASK DRV.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskDrv, 
                          "Drv",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_DRV_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Drv.");
    }

    // ������ ������ �����������
    if (DEBUG_TH) printf_dos("RUN TASK LOG.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskLog, 
                          "Log",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_LOG_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Log.");
    }
    
    
    print_str( 27, 1, 0, 0, "�����");
    
    msg = M_START;
    xReturn = xQueueSendToBack(xLog, &msg, 0);
    if (xReturn != pdPASS) printf_dos("TASK POST: error - xQueueSendToBack( xQueueSendToBack(xLog, &msg\n");
    
    vTaskDelete(NULL);
}