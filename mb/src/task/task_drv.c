// ������:
// ���������� ������ ��������
//
//
//
//
#include "os.h"
#include "printf_hal.h"
#include "msg_types.h"
#include "debug.h"
#include "string.h"
#include "task_drv.h"
#include "hardware.h"

#ifndef DEBUG_DRV
#define DEBUG_DRV 1
#endif

#define false (0)
#define true  (1)

#define HALL_SENSITIVITY 55

#define abs(x)  (x >= 0 ? x : -x)

typedef enum
{
    T_INITIALIZE = 0,
    T_CALIBRATE,
    T_WAIT_RESPONSE,
    T_GETDATA,
    T_PROCESS,
    T_SENDLOG,
} task_drv_state_t;

typedef struct
{
    int32_t current;
    float athres;
    int32_t pos;
    int32_t vel;
    int32_t acc;
    float   epos;
    int32_t upos;
    uint8_t ndata;
} device_state_t;

extern xQueueHandle xPakRouterToTDrv;             // ������� ������ Router -> TASK Drv
extern xQueueHandle xPakTDrvToRouter;             // ������� ������ TASK Drv -> Router

extern status_sensors_t cur_send_st;   // ���������� ��������� ������� ��������
// ��������� ������ �������-������� (��� �����������)
// ������� ��������� ������ 0-100%
// ����� ������-������ (�������� ������ - ��� ����� (���� ���))
// ��������: ��������-�������� ������

// ������� ������ ��� ������ �����������
extern xQueueHandle xLog;                         // ������� ������ ��� ������ Task Log (��� ������ ����������� � ������ �������)

extern xQueueHandle xInSensors;

// ��������� ������� ��������� ������� - ���������� �� I2C ������
// ����� ����������� � ��� ���������� �������� (���������� ��������� ������ ����������� �� ���������� ������ � I2C flash)
extern device_config_t device_config;

static packet_struct_t packet_rx; // �������� �����
static packet_struct_t packet_tx; // ����������� �����
static drv_config_t drv_config;

door_curent_state_t door_current; // ������� ��������� ������

//------------------------------------------------------------------------------
static void vSendCommandToDrv(command_t cmd, packet_struct_t* ptx)
{
    ptx->adr_dst  = ADR_DRV;
    ptx->type = cmd;
    xQueueSendToBack(xPakTDrvToRouter, ptx, portMAX_DELAY);
}

static void vCalcDrvParams(void)
{
    uint16_t a, u;
    float ao, ac, wo, wc, op;
    int8_t r;
    uint8_t h;
    
    a = device_config.motor_i_max*HALL_SENSITIVITY;
    u = device_config.motor_v_max*1000;
    if (a != drv_config.motor_i_max || u != drv_config.motor_u_max)
    {
        drv_config.motor_i_max = a;
        drv_config.motor_u_max = u;
        memcpy(&packet_tx.data, &drv_config.motor_i_max, 4);
        vSendCommandToDrv(SetElectData, &packet_tx);
    } 
    
    r = device_config.end_position_inversion ? -1 : 1;
    if (r != drv_config.reverse)
    {
        drv_config.reverse = r;
        memcpy(&packet_tx.data, &drv_config.reverse, 1);
        vSendCommandToDrv(ChangeReverse, &packet_tx);
    }
    
    h = device_config.motor_stop ? 1 : 0;
    if (h != drv_config.hold) drv_config.hold = h;
    
    ao = (float)device_config.speed_open/100.0f;
    ac = (float)device_config.speed_close/100.0f;
    wo = (float)device_config.speed_open_winter/100.0f;
    wc = (float)device_config.speed_close_winter/100.0f;
    op = (float)device_config.open_width_in_winter/100.0f;
    if (ao != drv_config.speed_coef_auto_open           ||
        ac != drv_config.speed_coef_auto_close          ||
        wo != drv_config.speed_coef_winter_open         ||
        wc != drv_config.speed_coef_winter_close        ||
        op != drv_config.open_part_winter)  
    {    
        drv_config.speed_coef_auto_open     = ao;   
        drv_config.speed_coef_auto_close    = ac;   
        drv_config.speed_coef_winter_open   = wo;
        drv_config.speed_coef_winter_close  = wc;
        drv_config.open_part_winter         = op;
        memcpy(&packet_tx.data, &drv_config.speed_coef_auto_open, 20);
        vSendCommandToDrv(SetDynData, &packet_tx);
    }
}

void vTaskDrv(void const * argument)
{ 
    device_state_t device_state = {0, 0, 0, 0, 0, 0, 0};
    task_drv_state_t task_state = 0, task_state_prev;
    
    pdu_state_e pdu_curr = I_NONE, pdu_prev = I_NONE;
    input_dat_state_e stop_curr = I_OFF, stop_prev = I_OFF;
    input_dat_state_e fire_curr = I_OFF, fire_prev = I_OFF;
    input_dat_state_e test_curr = I_OFF, test_prev = I_OFF;
       
    uint8_t clb_tries = 0;
    uint8_t mov_tries = 0;
    
    msg_t msg; // ������� ������
    portBASE_TYPE res;
    
    uint8_t self_check = 0;
    
    uint32_t len = 0, alen = 0, wlen = 0;      //������ ����� �������� ������
    
    portTickType xTimeTx, xTimeMv;

    packet_tx.preamble = PREAMBLE_32_BIT;
    packet_tx.adr_src  = ADR_TDRV;

    xTimeTx = xTaskGetTickCount() + 100;
    
    if (DEBUG_DRV) printf_dos("Task DRV - START.\n");

//------------------------------------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------------------------------------

//xTimeTx = xTaskGetTickCount() + 30000;
//do
//{
//    test_curr = cur_send_st.test;
//    if (test_curr != test_prev)     //����������
//    {
//        if (test_curr == I_ON)
//        {
//            if (DEBUG_DRV) printf_dos("Calibration on startup\n");
//            
//            drv_config.calibration = false;
//            drv_config.mechpart_config = false;
//            clb_tries = 0;
//            task_state = T_CALIBRATE;
//            break;
//        }
//        test_prev = test_curr;
//    }
//    vTaskDelay(10);
//} while (xTimeTx > xTaskGetTickCount());
//driver_pcb_reset();      

WHILE_BEGIN

  vCalcDrvParams();
  
  switch (task_state)
  {
      case T_WAIT_RESPONSE:     //�������� ������������� ������ �� ��������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_WAIT_RESPONSE\n");
        memset(&packet_rx, 0, sizeof(packet_rx));
        res = xQueueReceive(xPakRouterToTDrv, &packet_rx, 0); // ���������� ����� ���� ����, �� ������� !
        if (res == pdPASS && packet_rx.adr_src == ADR_DRV)
        {
          if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get Packet %d\n", packet_rx.type);
          if (packet_rx.type == packet_tx.type) //������� ���������� �����
          {
              switch (packet_rx.type)
              {
                  case GetElectData:                              //����� �� ������ ������������� - 1   
                    uint16_t   i_max = 0, u_max = 0;                
                    int8_t    rvs = 0;
                    
                    memcpy(&i_max, &packet_rx.data[0], 2);
                    memcpy(&u_max, &packet_rx.data[2], 2);
                    memcpy(&rvs, &packet_rx.data[4], 1);
                    memcpy(&drv_config.calibration, &packet_rx.data[5], 1);
                    if (i_max     != drv_config.motor_i_max       ||
                        u_max     != drv_config.motor_u_max)
                    {
                        memcpy(&packet_tx.data, &drv_config.motor_i_max, 4);
                        vSendCommandToDrv(SetElectData, &packet_tx);
                    }
                    else if (rvs  != drv_config.reverse) 
                    {
                        memcpy(&packet_tx.data, &drv_config.reverse, 1);
                        vSendCommandToDrv(ChangeReverse, &packet_tx);
                    }
                    else drv_config.elpart_config = true;
                    break;
                  case GetDynData:                              //����� �� ������ ������������� - 1   
                    float   ao = 0, ac = 0, wo = 0, wc = 0, op = 0;            
                    
                    memcpy(&ao, &packet_rx.data[0], 4);
                    memcpy(&ac, &packet_rx.data[4], 4);
                    memcpy(&wo, &packet_rx.data[8], 4);
                    memcpy(&wc, &packet_rx.data[12], 4);
                    memcpy(&op, &packet_rx.data[16], 4);
                    if (ao      != drv_config.speed_coef_auto_open      ||
                        ac      != drv_config.speed_coef_auto_close     ||
                        wo      != drv_config.speed_coef_winter_open    ||
                        wc      != drv_config.speed_coef_winter_close   ||
                        op      != drv_config.open_part_winter)
                    {
                        memcpy(&packet_tx.data, &drv_config.speed_coef_auto_open, 20);
                        vSendCommandToDrv(SetDynData, &packet_tx);
                    }
                    else drv_config.dynpart_config = true;
                    break;
                  case GetMechData:                               //����� �� ������ ������������� - 2
                    memcpy(&drv_config.nom_vel, &packet_rx.data, 10);
                    alen = abs(drv_config.position_open - drv_config.position_close);
                    wlen = drv_config.open_part_winter*alen;
                    drv_config.mechpart_config = true;
                    break;
                  case GetServiceParam:
                    packet_tx.adr_dst = ADR_PC;
                    packet_tx.type = GetServiceParam;
                    memcpy(&packet_tx.data[0], &packet_rx.data[0], 18);
                    memcpy(&packet_tx.data[18], &device_config.max_position_error, 2);
                    memcpy(&packet_tx.data[20], &device_config.barriers_active, 2);
                    xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                    break;
                  case GetCalibrate:                              //����� �� ������ ����������
                    drv_config.calibration = true;
                    drv_config.mechpart_config = false;
                    break; 
                  case GetSystemData:                             //����� �� ������ ��������� ������
                    memcpy(&device_state, packet_rx.data, 24);
                    device_state.ndata = true;
                    break;
              }
          }
          else  //������� ������������ �����
          {
              switch (packet_rx.type)
              {
                  case GetErrorData: 
                    memcpy(&msg, &packet_rx.data[0], 1);
                    if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get Error %d\n", msg);
                    switch (msg)
                    {
                        case M_CALIBRATE_ERR:   
                            int32_t fv, bv;
                            float ft, bt;
                            memcpy(&fv, &packet_rx.data[1], sizeof(int32_t));
                            memcpy(&bv, &packet_rx.data[5], sizeof(int32_t));
                            memcpy(&ft, &packet_rx.data[9], sizeof(float));
                            memcpy(&bt, &packet_rx.data[13], sizeof(float));
                            if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get fv = %d, \
                                            bv = %d, ft = %d, bt = %d\n", 
                                            fv, bv, (int)(ft*100), (int)(bt*100));
                            if (fv == 0 || abs(fv) > 200 || bv == 0 || abs(bv) > 200) msg = M_ENCODER_ERR;
                            else if (ft == 0 || ft > 0.1 || bt == 0 || bt > 0.1) msg = M_HIGH_WEIGHT; 
                            break;
                        case M_DRV_NO_DATA:
                            memcpy(&packet_tx.data, &drv_config.motor_i_max, 6);
                            vSendCommandToDrv(SetElectData, &packet_tx);
                            break;
                        default:
                            xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
                            break;
                            
                    }
                    xQueueSendToBack(xLog, &msg, portMAX_DELAY); 
                    break; 
                  default:
                    if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: wrong packet received\n");
                    //xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
                    break;
              }
          }
        }
        else
        {
            if (res == pdPASS) xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
            if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: no answer from drv\n");
            msg = M_DRV_SILENT;
            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
        }
        task_state = task_state_prev;       //������� � ���������� ���������
        break;
      case T_INITIALIZE:        //������������� ��������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_INITIALIZE\n");
        drv_config.state.prev = drv_config.state.current;
        drv_config.state.current = DRV_INIT;
        
        if (!drv_config.elpart_config)       //������ ������������� 1
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: request electrical data\n");
            vSendCommandToDrv(GetElectData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        }
        
        if (!drv_config.dynpart_config)       //������ ������������� 2
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: request dynamic data\n");
            vSendCommandToDrv(GetDynData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        }
        
        if (!drv_config.mechpart_config)     //������ ������������ 2
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: request mechanical data\n");
            vSendCommandToDrv(GetMechData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        }
        //��� ���������� - ����������
        if (drv_config.calibration) 
        {
            drv_config.state.prev = drv_config.state.current;
            drv_config.state.current = DRV_AUTO;
            vSendCommandToDrv(SetSelfCheck, &packet_tx);
            self_check = 1;
        }
        xTimeMv = xTaskGetTickCount(); 
        task_state = T_GETDATA;
        break;
      case T_CALIBRATE: //��������� ���������� - ��� �������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_CALIBRATE\n");
        if (clb_tries < 3 && !drv_config.calibration)
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: request calibration\n");
            clb_tries++;
            vSendCommandToDrv(GetCalibrate, &packet_tx);
            vTaskDelay(device_config.calib_delay*1000);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        }  //���� ���������� �������� - ������ ����� ������������� ������
        else if (drv_config.calibration && !drv_config.mechpart_config)
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: request mechanical data\n");
            vSendCommandToDrv(GetMechData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        } //���� ���������� ����������
        if (clb_tries == 3 &&
           !drv_config.calibration &&
           !drv_config.mechpart_config) 
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: calibration filed\n");
            drv_config.state.prev = drv_config.state.current;
            drv_config.state.current = DRV_MANUAL;   //��������� ���������� ������� - ������ �����
            vSendCommandToDrv(SetManCtrl, &packet_tx);
            msg = M_CALIBRATE_ERR;
            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
        }
        else 
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: calibration done\n");
            xTimeMv = xTaskGetTickCount();
            drv_config.state.prev = drv_config.state.current;
            drv_config.state.current = DRV_AUTO;
        }
        task_state = T_GETDATA;
        break;
      case T_GETDATA: //������ �������� ������ � �������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_GETDATA\n");
        if (!device_state.ndata)        //���� ��� ������ �� ��������, ������������ ������
        {
            if (DEBUG_DRV) printf_dos("T_GETDATA: request system data\n");
            vSendCommandToDrv(GetSystemData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
        }
        else task_state = T_PROCESS;    //���� ������ -> ������� � �������� ����
        break;
      case T_PROCESS:   //�������� ����
        if (DEBUG_DRV) printf_dos("TDRV: task state T_PROCESS\n");
        
        fire_curr = cur_send_st.fire;                           //����� "�������"
        if (fire_curr != fire_prev)
        {
            fire_prev = fire_curr;
            if (fire_curr == I_ON)                              //"�������" ���������
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: FIRE state ON\n");
                
                if (drv_config.state.current == DRV_WINTER)  
                {
                    vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                    vTaskDelay(5);
                }
                
                drv_config.state.prev = drv_config.state.current;
                drv_config.state.current = DRV_FIRE;
                
                switch (device_config.reaction_fire_input)
                {
                    case 1:
                      //������� �����
                      vSendCommandToDrv(SetDoorOpen, &packet_tx);
                      device_state.upos = drv_config.position_open;
                      break;
                   case 2:
                      //������� �����
                      vSendCommandToDrv(SetDoorClose, &packet_tx);
                      device_state.upos = drv_config.position_close;
                      break;
                  case 3:
                      //������� �����
                      vSendCommandToDrv(SetDoorClose, &packet_tx);
                      device_state.upos = drv_config.position_close;
                      break;
                }
                device_state.ndata = false;
                task_state = T_GETDATA;
                break;
            }
            else 
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: FIRE state OFF\n");
                drv_config.state.current = DRV_AUTO;
                
                switch (device_config.reaction_fire_input)      //"�������" ����������
                {
                    case 1:
                        vTaskDelay(device_config.delay_open_fire*1000);
                    case 2:
                    case 3:
                        drv_config.state.prev = DRV_ERROR;
                        drv_config.state.current = drv_config.state.prev;
                        break;
                }
                device_state.ndata = false;
                task_state = T_GETDATA;
                break;
            }        
        }
        
        if (drv_config.state.current != DRV_FIRE)   //���� �� ������ "�������"        
        {
            stop_curr = cur_send_st.stop;   //����� "����"
            if (stop_curr != stop_prev)
            {
                stop_prev = stop_curr;
                if (stop_curr == I_ON)           //"����" ���������
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: STOP state ON\n");
                    drv_config.state.prev = drv_config.state.current;
                    drv_config.state.current = DRV_MANUAL;
                    
                    vSendCommandToDrv(SetManCtrl, &packet_tx);
                    
                    device_state.ndata = false;
                    task_state = T_GETDATA;
                    break;
                }
                else                                            //"����" ����������
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: STOP state OFF\n");
                    drv_config.state.current = drv_config.state.prev;
                    
                    vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                    vTaskDelay(10);
                    vSendCommandToDrv(SetSelfCheck, &packet_tx);
                    self_check = 1;
                    
                    device_state.ndata = false;
                    task_state = T_GETDATA;
                    break;
                }
            }
           
            test_curr = cur_send_st.test;
            if (test_curr != test_prev)     //����� "����������"
            {
                test_prev = test_curr;
                if (test_curr == I_ON)     //"����������" �������
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: TEST state ON\n");

                    drv_config.calibration = false;
                    drv_config.mechpart_config = false;
                    clb_tries = 0;
                    task_state = T_CALIBRATE;
                    door_current.state = DOOR_CALIB;
                    break;
                }
            }
        }
        
        //����� ����������� ������������� - ������ � �������������� �������
        if (!self_check)
        {
            xQueueReceive(xInSensors, &pdu_curr, 0);
            if (drv_config.state.prev == DRV_ERROR || pdu_curr != pdu_prev)                               //����� "�����"
            {
                drv_config.state.prev = drv_config.state.current;
                switch (pdu_curr)
                {
                    case I_CLOSE:                                   //������� �����
                      if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state CLOSE\n");
                      drv_config.state.current = DRV_STOP;
                      
                      len = alen;
                      if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                      {
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");

                          drv_config.position_open = drv_config.position_open + alen;                              
                          vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                          vTaskDelay(5);
                      }    
                      if (drv_config.hold) vSendCommandToDrv(SetHoldMotor, &packet_tx);
                      vTaskDelay(5);
                      vSendCommandToDrv(SetDoorClose, &packet_tx);
                      device_state.upos = drv_config.position_close;
                      break;
                    case I_OPEN:                                    //������� �����
                      if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state OPEN\n");
                      drv_config.state.current = DRV_STOP;
                      
                      len = alen;
                      if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                      {
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");

                          drv_config.position_open = drv_config.position_open + alen;                              
                          vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                          vTaskDelay(5);
                      }
                      if (drv_config.hold) vSendCommandToDrv(ResetHoldMotor, &packet_tx);
                      vTaskDelay(5);
                      vSendCommandToDrv(SetDoorOpenSlow, &packet_tx);
                      device_state.upos = drv_config.position_open;
                      break;
                    case I_EXIT:                                    //������ �� �����
                     if (DEBUG_DRV)  printf_dos("T_PROCESS: PDU state EXIT\n");
                      drv_config.state.current = DRV_EXIT;
                      
                      len = alen;
                      if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                      {
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");

                          drv_config.position_open = drv_config.position_open + alen;                              
                          vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                          vTaskDelay(5);
                      } 
                      if (drv_config.hold) vSendCommandToDrv(ResetHoldMotor, &packet_tx);
                      break;
                    case I_WINTER:                                  //������ ����� - �������� ����������
                      if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state WINTER\n");
                      drv_config.state.current = DRV_WINTER;
                      
                      if (pdu_prev == I_OPEN)                           //����� �� ������ "�������"
                      {
                          float ow = 1.0f - (device_state.epos / (float)alen);
                          if (ow < 0.35f) ow = 0.35f;
                          if (ow < 0.9f)
                          {
                              drv_config.open_part_winter = ow;
                              vSendCommandToDrv(SetDoorStop, &packet_tx);
                              vTaskDelay(5);
                              wlen = (uint32_t)alen*drv_config.open_part_winter;
                              device_config.open_width_in_winter = (uint16_t)(drv_config.open_part_winter*100);
                              memcpy(&packet_tx.data, &drv_config.speed_coef_auto_open, 20);
                              vSendCommandToDrv(SetDynData, &packet_tx);
                              vTaskDelay(1000);
                              
                              vSendCommandToDrv(SetSelfCheck, &packet_tx);
                              vTaskDelay(10);
                              self_check = 1;
                              device_state.ndata = false;
                          }
                      }
                      len = wlen;
                      
                      if (drv_config.hold) vSendCommandToDrv(ResetHoldMotor, &packet_tx);
                      vTaskDelay(5);
                      drv_config.position_open = drv_config.position_close + len;                          
                      vSendCommandToDrv(SetWintCtrl, &packet_tx);
                      break;
                    case I_AUTO:                                    //�������������� �����
                      if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state AUTO\n");
                      drv_config.state.current = DRV_AUTO;
                      
                      len = alen;
                      if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                      {
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");
                          
                          drv_config.position_open = drv_config.position_open + alen;                              
                          vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                          vTaskDelay(5);
                      }
                      if (drv_config.hold) vSendCommandToDrv(ResetHoldMotor, &packet_tx);
                      vTaskDelay(5);
                      vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                      break;
                    default:
                      break;
                }
                pdu_prev = pdu_curr;
            }
   
            input_dat_state_e barriers = device_config.barriers_active         ? (cur_send_st.bar1 | cur_send_st.bar2) :
                                         door_current.state != DOOR_CLOSE      ? (cur_send_st.bar1 | cur_send_st.bar2) :
                                         I_OFF; 
            //����� ��������/������� - ������ � �������������� �������
            if (drv_config.state.current == DRV_EXIT)
            {
                  if (cur_send_st.rdr1 == I_ON || barriers)    
                  {
                      if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in EXIT mode\n");
                      vSendCommandToDrv(SetDoorOpen, &packet_tx);
                      device_state.upos = drv_config.position_open;
                      if (device_config.delay_open > 0) xTimeMv = xTaskGetTickCount() + (len/drv_config.nom_vel) + device_config.delay_open*1000;
                  }
                  else if (cur_send_st.bar1 == I_OFF &&           //������ �������� � ����� �������
                           cur_send_st.bar2 == I_OFF &&
                           cur_send_st.rdr1 == I_OFF &&
                           door_current.state == DOOR_OPEN)
                  {
                      if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in EXIT mode\n");
                      xTimeTx = xTaskGetTickCount();
                      if (xTimeTx > xTimeMv)
                      {
                          vSendCommandToDrv(SetDoorClose, &packet_tx);
                          device_state.upos = drv_config.position_close;
                      }
                  }
            }
            else if (drv_config.state.current == DRV_WINTER)
            {
                if (cur_send_st.rdr1 == I_ON           ||
                    cur_send_st.rdr2 == I_ON           ||
                    barriers)
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in WINTER mode\n");
                    vSendCommandToDrv(SetDoorOpen, &packet_tx);
                    device_state.upos = drv_config.position_open;
                    if (device_config.delay_open_winter > 0) xTimeMv = xTaskGetTickCount() + (len/drv_config.nom_vel) + device_config.delay_open_winter*1000;
                }
                else if (cur_send_st.bar1 == I_OFF &&
                         cur_send_st.rdr1 == I_OFF &&
                         cur_send_st.bar2 == I_OFF &&
                         cur_send_st.rdr2 == I_OFF &&
                         door_current.state == DOOR_OPEN)
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in WINTER mode\n");
                    xTimeTx = xTaskGetTickCount();
                    if (xTimeTx > xTimeMv)
                    {
                        vSendCommandToDrv(SetDoorClose, &packet_tx);
                        device_state.upos = drv_config.position_close;
                    }
                }
            }  
            else if (drv_config.state.current == DRV_AUTO)
            {
                if (cur_send_st.rdr1 == I_ON     ||
                    cur_send_st.rdr2 == I_ON     ||
                    barriers)
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in AUTO mode\n");
                    vTaskDelay(device_config.delay_close*1000);
                    vSendCommandToDrv(SetDoorOpen, &packet_tx);
                    device_state.upos = drv_config.position_open;
                    if (device_config.delay_open > 0) xTimeMv = xTaskGetTickCount() + (len/drv_config.nom_vel) + device_config.delay_open*1000;
                }
                else if (cur_send_st.bar1 == I_OFF &&
                         cur_send_st.rdr1 == I_OFF &&
                         cur_send_st.bar2 == I_OFF &&
                         cur_send_st.rdr2 == I_OFF &&
                         door_current.state == DOOR_OPEN)
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in AUTO mode\n");
                    xTimeTx = xTaskGetTickCount();
                    if (xTimeTx > xTimeMv)
                    {
                        vSendCommandToDrv(SetDoorClose, &packet_tx); 
                        device_state.upos = drv_config.position_close;
                    }
                }
            }
        }
        
        //����� ��������� �����
        if (abs(device_state.epos) > device_config.max_position_error)
        {
            if (DEBUG_DRV) printf_dos("T_PROCESS: door is mooving\n");
            //����� � ��������
            if (self_check) door_current.state = DOOR_CHECK;
            else
            {
                if (device_state.upos == drv_config.position_close) door_current.state = DOOR_CLOSE_MOVE;
                else door_current.state = DOOR_OPEN_MOVE;
            }
            
            //��������� ������ ����������� ������ 
            if (abs(device_state.epos) > (len + device_config.max_position_error)) 
            {
                drv_config.state.prev = drv_config.state.current;
                drv_config.state.current = DRV_MANUAL;
                
                vSendCommandToDrv(SetManCtrl, &packet_tx);
                msg = M_DOOR_POS;
                xQueueSendToBack(xLog, &msg, portMAX_DELAY);
            }
            
            //�������� �� ��������� ���������������
            //if (drv_config.c1*device_state.pos + 
            //    drv_config.c2*device_state.vel + 
            //    drv_config.c3*device_state.acc > drv_config.c4)
            //{
            //    drv_config.calibration = false;
            //    drv_config.mechpart_config = false;
            //    clb_tries = 0;
            //    task_state = T_CALIBRATE;
            //    break;
            //}
        }
        else
        {
            if (DEBUG_DRV) printf_dos("T_PROCESS: door is stopped\n");
            //����� � �������� ��������� - �������/������
            if (drv_config.state.current == DRV_MANUAL)
            {
                door_current.state = DOOR_MANUAL;
            }
            if (self_check)
            {
                self_check = 0;
                if (drv_config.state.current == DRV_ERROR) mov_tries = 0;
                drv_config.state.prev = drv_config.state.current;
                drv_config.state.current = DRV_AUTO;
                vSendCommandToDrv(ResetSelfCheck, &packet_tx);
                vTaskDelay(10);
                vSendCommandToDrv(GetMechData, &packet_tx);
                task_state_prev = task_state;
                task_state = T_WAIT_RESPONSE;
                break;
            }
            else if (!drv_config.calibration || drv_config.state.current == DRV_ERROR)
            {
                door_current.state = DOOR_ERROR;
            }
            else
            {
                if (device_state.upos == drv_config.position_close) door_current.state = DOOR_CLOSE;
                else door_current.state = DOOR_OPEN;
            }
        }
        door_current.pos = (1 - abs(device_state.epos / (float)len)) * 100;
        device_state.ndata = false;
        
        // ���������� ��� ������ 
        while (uxQueueMessagesWaiting(xPakRouterToTDrv))
        {
            res = xQueueReceive(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); 
            if (res == pdPASS)
            {
                if (packet_rx.adr_src == ADR_DRV)
                {
                    switch (packet_rx.type)
                    {
                        case GetErrorData:     //��������� ������ �� ��������
                            memcpy(&msg, packet_rx.data, 1);
                            switch (msg)
                            {
                                case M_DRV_NO_DATA:     // � ������ �������� ��� ���������� ������
                                    memcpy(&packet_tx.data, &drv_config.motor_i_max, 6);
                                    vSendCommandToDrv(SetElectData, &packet_tx);
                                    break;
                                case M_CALIBRATE_NO:    // ���������� �� ���������
                                    if (!self_check || mov_tries > device_config.max_error_count)
                                    {
                                        drv_config.state.current = DRV_ERROR;    //FATAL!!!
                                        vSendCommandToDrv(SetManCtrl, &packet_tx);
                                        break;
                                    }
                                    else msg = M_MOT_OVERCUR;
                                case M_MOT_OVERCUR:     // ���������� �� ���� - �����������
                                    if (mov_tries < device_config.max_error_count)
                                    {
                                        mov_tries++;
                                        vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                                        vTaskDelay(10);
                                        vSendCommandToDrv(SetSelfCheck, &packet_tx);
                                        self_check = 1;
                                        drv_config.state.current = DRV_ERROR;
                                    }
                                    else
                                    {
                                        drv_config.state.current = DRV_ERROR;    //FATAL!!!
                                        vSendCommandToDrv(SetManCtrl, &packet_tx);
                                    }
                                    break;
                                case M_DRV_PERIPH:
                                case M_IN_VOL_LOW:
                                case M_IN_VOL_NEG:
                                case M_ACC_VOL_NEG:
                                case M_CALIBRATE_ERR:
                                    msg = M_DRV;
                                    drv_config.state.current = DRV_ERROR;    //FATAL!!!
                                    vSendCommandToDrv(SetManCtrl, &packet_tx);
                                    break;
                                default:    
                                    if (DEBUG_DRV) printf_dos("T_PROCESS: get Error %d\n", msg);
                                    break;
                            }
                            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
                            break;
                        default:
                            break;
                    }
                }
                else if (packet_rx.adr_src == ADR_PC)
                {
                    switch (packet_rx.type)
                    {
                        case SetElectData:  // ������� ��������� ������������� ����� ������� �� ��
                            uint16_t   u0 = 0, u1 = 0, u2 = 0, u3 = 0, u4 = 0, u5 = 0, u6 = 0, u7 = 0, u8 = 0, u9 = 0;
    
                            memcpy(&u1, &packet_rx.data[0], 2);
                            memcpy(&u2, &packet_rx.data[2], 2);
                            memcpy(&u3, &packet_rx.data[4], 2);
                            memcpy(&u4, &packet_rx.data[6], 2);
                            memcpy(&u5, &packet_rx.data[8], 2);
                            memcpy(&u6, &packet_rx.data[10], 2);
                            memcpy(&u7, &packet_rx.data[12], 2);
                            memcpy(&u8, &packet_rx.data[14], 2);
                            memcpy(&u9, &packet_rx.data[16], 2);
                            if (u1 != device_config.motor_i_max            ||
                                u2 != device_config.motor_v_max            ||
                                u3 != device_config.speed_open             ||
                                u4 != device_config.speed_open_winter      ||
                                u5 != device_config.speed_close            ||  
                                u6 != device_config.speed_close_winter     ||  
                                u7 != device_config.open_width_in_winter   ||
                                u8 != device_config.end_position_inversion)
                            {
                                device_config.motor_i_max = u1;
                                device_config.motor_v_max = u2;
                                device_config.speed_open = u3;
                                device_config.speed_open_winter = u4;
                                device_config.speed_close = u5;
                                device_config.speed_close_winter = u6;
                                device_config.open_width_in_winter = u7;
                                device_config.end_position_inversion = u8;
                                vCalcDrvParams();
                            }
                            if (u9 != device_config.motor_stop)
                            {
                                device_config.motor_stop = u9;
                                drv_config.hold = u9;
                                if (drv_config.hold) vSendCommandToDrv(SetHoldMotor, &packet_tx);
                                else vSendCommandToDrv(ResetHoldMotor, &packet_tx);
                            }
                            break;
                        case GetElectData:
                            packet_tx.adr_dst = ADR_PC;
                            packet_tx.type = GetElectData;
                            memcpy(&packet_tx.data[0], &device_config.motor_i_max, 2);
                            memcpy(&packet_tx.data[2], &device_config.motor_v_max, 2);
                            memcpy(&packet_tx.data[4], &device_config.speed_open, 2);
                            memcpy(&packet_tx.data[6], &device_config.speed_open_winter, 2);
                            memcpy(&packet_tx.data[8], &device_config.speed_close, 2);
                            memcpy(&packet_tx.data[10], &device_config.speed_close_winter, 2);
                            memcpy(&packet_tx.data[12], &device_config.open_width_in_winter, 2);
                            memcpy(&packet_tx.data[14], &device_config.end_position_inversion, 2);
                            memcpy(&packet_tx.data[16], &device_config.motor_stop, 2);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case SetMechData:
                            memcpy(&u0, &packet_rx.data[0], 2);
                            memcpy(&u1, &packet_rx.data[2], 2);
                            memcpy(&u2, &packet_rx.data[4], 2);
                            memcpy(&u3, &packet_rx.data[6], 2);
                            memcpy(&u4, &packet_rx.data[8], 2);
                            memcpy(&u5, &packet_rx.data[10], 2);
                            memcpy(&u6, &packet_rx.data[12], 2);
                            memcpy(&u7, &packet_rx.data[14], 2);
                            if (u0 != device_config.delay_close            ||
                                u1 != device_config.delay_open             ||
                                u2 != device_config.delay_open_winter      ||
                                u3 != device_config.delay_open_fire        ||
                                u4 != device_config.lock_pos               ||
                                u5 != device_config.reaction_fire_input    ||
                                u6 != device_config.batt_job_n             ||
                                u7 != device_config.lock_type_num)  
                            {
                                device_config.delay_open = u1;
                                device_config.delay_open_winter = u2;
                                device_config.delay_open_fire = u3;
                                device_config.lock_pos = u4;
                                device_config.reaction_fire_input = u5;
                                device_config.batt_job_n = u6;
                                device_config.lock_type_num = u7;
                            }
                            break;
                        case GetMechData:
                            packet_tx.adr_dst = ADR_PC;
                            packet_tx.type = GetMechData;
                            memcpy(&packet_tx.data[0], &device_config.delay_close, 2);
                            memcpy(&packet_tx.data[2], &device_config.delay_open, 2);
                            memcpy(&packet_tx.data[4], &device_config.delay_open_winter, 2);
                            memcpy(&packet_tx.data[6], &device_config.delay_open_fire, 2);
                            memcpy(&packet_tx.data[8], &device_config.lock_pos, 2);
                            memcpy(&packet_tx.data[10], &device_config.reaction_fire_input, 2);
                            memcpy(&packet_tx.data[12], &device_config.batt_job_n, 2);
                            memcpy(&packet_tx.data[14], &device_config.lock_type_num, 2);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case SetServiceParam:
                            memcpy(&u1, &packet_rx.data[14], 2);
                            memcpy(&u2, &packet_rx.data[16], 2);
                            memcpy(&u3, &packet_rx.data[18], 2);
                            memcpy(&u4, &packet_rx.data[20], 2);
                            if (u1 != device_config.max_position_error  ||
                                u2 != device_config.max_error_count     ||
                                u3 != device_config.calib_delay         ||
                                u4 != device_config.barriers_active)
                            {
                                device_config.max_position_error = u1;
                                device_config.max_error_count = u2;
                                device_config.calib_delay = u3;
                                device_config.barriers_active = u4;
                            }
                            memcpy(&packet_tx.data[0], &packet_rx.data[0], 14);
                            vSendCommandToDrv(SetServiceParam, &packet_tx);
                            break;
                        case GetServiceParam:
                            vSendCommandToDrv(GetServiceParam, &packet_tx);
                            task_state_prev = task_state;
                            task_state = T_WAIT_RESPONSE;
                            break;
                        case SetDiagData:  // ������� ������ ��������������� ������������� �� ��
                            memcpy(&drv_config.c1, &packet_rx.data[0], 16);
                            break;
                        case GetDiagData:  // ������� ������ ��������������� ������������� �� ��
                            packet_tx.adr_dst = ADR_PC;
                            packet_tx.type = GetDiagData;
                            memcpy(&packet_tx.data[0], &drv_config.c1, 16);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case GetCalibrate:
                            drv_config.calibration = false;
                            drv_config.mechpart_config = false;
                            clb_tries = 0;
                            task_state = T_CALIBRATE;
                            door_current.state = DOOR_CALIB;
                            break;
                        case GetErrorData:     //��������� ������ �� ��������
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        //�������� ������ �� �� ��� � 100 ��
        if(task_state == T_PROCESS) 
        {
            if (DEBUG_DRV) printf_dos("TDRV: task state T_SENDLOG\n");
            packet_tx.adr_dst = ADR_PC;
            packet_tx.type = GetSystemData;
            memcpy(packet_tx.data, &device_state.current, 24);
            res = xQueueSendToBack( xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
            if (res != pdPASS){
              if (DEBUG_DRV) printf_dos("ERROR: TDRV: xQueueSendToBack( xPakTDrvToRouter, &packet_tx, 0)\n");
            }else{
              if (DEBUG_DRV) printf_dos("TDRV: packet_tx to DRV OK\n");
             //xTimeTx = xTaskGetTickCount() + 100;
            }
            task_state = T_GETDATA;
        }
        break;
    default:    
       break;
  }
  vTaskDelay(10);
    

   
/*
    res = xQueueReceive(xPakRouterToTDrv, &packet_rx, 0);
    if (res == pdPASS){
        printf_dos("TASK DRV:������� �����:\n");
    }

    res = xQueueSendToBack(xPakTDrvToRouter, &packet_tx, 5);
    if (res != pdPASS) printf_dos("TASK DRV: error - xQueueSendToBack( packet_from_tdrv.q, packet_from_tdrv.buf\n");
*/

    // ������� ������, ���� ����
/*    er = ER_DRV;
    res = xQueueSendToBack( xLog, &er, 0);
    if (res != pdPASS) printf_dos("TASK DRV: error - xQueueSendToBack( xLog, er)\n");

// ������������ ������ ������
    door_current.state = DOOR_CLOSE;   // �������
    door_current.pos   = 100;          // 100 % - �.�. ���������
    vTaskDelay(3000);


    door_current.state = DOOR_OPEN;        // �������
    door_current.counter_open++;           // ������� �������� +1
    vTaskDelay(3000);

    door_current.state = DOOR_CLOSE;       // �������
    door_current.counter_close++;          // ������� �������� +1
    vTaskDelay(3000);
*/

WHILE_END

}

