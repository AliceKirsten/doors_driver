#ifndef TASK_DRV_H_
#define TASK_DRV_H_

// ��������� ������� - ����/������
typedef enum
{
    DRV_ERROR = 0,
    DRV_INIT,
    DRV_FIRE,
    DRV_STOP,
    DRV_EXIT,
    DRV_WINTER,
    DRV_MANUAL,
    DRV_AUTO  
} drv_control_st_e;

typedef struct
{
    drv_control_st_e current;
    drv_control_st_e prev;
} drv_control_st_t;

typedef __packed struct
{
    uint8_t   elpart_config;       // ���� ������������ ������������� ����� ����� DRV
    uint16_t  motor_i_max;         // ������ �� ���� ���������
    uint16_t  motor_u_max;         // ������ �� ���������� ���������          
    int8_t    reverse;             // ������ ������  
    uint8_t   hold;                // ��������� � �������� ���������
    uint8_t   calibration;
    
    uint8_t   dynpart_config;       // ���� ������������ ������������ ����� ����� DRV
    float     speed_coef_auto_open;     // ����������� ���������   
    float     speed_coef_auto_close;    // ����������� ���������   
    float     speed_coef_winter_open;   // ����������� ��������� � ������ ������ 
    float     speed_coef_winter_close;  // ����������� ��������� � ������ ������
    float     open_part_winter;         // ������� ��������� � ������ ������
    
    uint8_t   mechpart_config;     // ���� ������������ ������������ ����� ����� DRV
    uint16_t  nom_vel;             // ����������� �������� (�.�.)     
    int32_t   position_close;      // ��������� ��������  (�.�.)
    int32_t   position_open;       // ��������� ��������  (�.�)

    drv_control_st_t state;        // ����� ���������� (������� � ����������) - ����/������

  // �� ����������� - ������������ ��������� ���������������
    float c1;
    float c2;
    float c3;
    float c4;
    
} drv_config_t;


void vTaskDrv(void const * argument);

#endif