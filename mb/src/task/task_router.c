// ������:
// ������ � ��������� �������
// ��������� ���������
//
//
//
#include "os.h"
#include "printf_hal.h"
#include "queue_buf.h"
#include "get_packet_from_q.h"
#include "get_packet_from_osq.h"
#include "msg_types.h"
#include "debug.h"


#ifndef DEBUG_ROUTER
#define DEBUG_ROUTER 0
#endif

extern packet_build_t packet_from_pc;
extern packet_build_t packet_from_drv;
extern packet_build_t packet_from_tdrv;
extern packet_build_t packet_from_tlog;

extern queue_buf_t q_to_pc;
extern queue_buf_t q_to_drv;

extern xQueueHandle xPakRouterToTDrv;
extern xQueueHandle xPakRouterToTLog;

extern void uart_pc_tx_dma( void );
extern void uart_drv_tx_dma( void );
extern volatile uint8_t uart_pc_flag_tx;
extern volatile uint8_t uart_drv_flag_tx;

// ������� ����� � �������� �������
typedef enum {
    QO_FULL = 0,
    QO_READY
} q_out_ready_t;



packet_build_t *pb[] = {&packet_from_pc, &packet_from_drv, &packet_from_tlog, &packet_from_tdrv};

void vTaskRouter(void const * argument)
{
    portBASE_TYPE res;
    portTickType time;
    size_t i;
    packet_struct_t *packet;
    q_out_ready_t qo;
    
    if (DEBUG_ROUTER) printf_dos("Task Router - START.\n");

WHILE_BEGIN

    time = xTaskGetTickCount();
    get_packet_from_q( &packet_from_pc, time);
    get_packet_from_q( &packet_from_drv, time);
    
    get_packet_from_osq( &packet_from_tlog, time);
    get_packet_from_osq( &packet_from_tdrv, time);


    // ����� ������� ��������, � �������� ���� ��� ���������� ?
    for (i=0; i< sizeof(pb)/sizeof(packet_build_t *); i++){
        if (pb[ i ]->flag == FP_NO ) continue;
        packet = (packet_struct_t*)pb[ i ]->buf;
        
        //----------------------------------------------------------------------
        switch (packet->adr_dst){
        case ADR_PC:{
            if (get_free_size_queue( &q_to_pc ) > PACKET_SIZE) qo = QO_READY; else qo = QO_FULL;
            break;
        }
        case ADR_DRV:{
            if (get_free_size_queue( &q_to_drv ) > PACKET_SIZE) qo = QO_READY; else qo = QO_FULL;
            break;
        }
        case ADR_TDRV:{
            if (uxQueueSpacesAvailable( xPakRouterToTDrv )) qo = QO_READY; else qo = QO_FULL;
            break;
        }
        case ADR_TLOG:{
            if (uxQueueSpacesAvailable( xPakRouterToTLog )) qo = QO_READY; else qo = QO_FULL;
            break;
        }
        default:{
            pb[ i ]->flag = FP_NO;
            break;
        }
        } // switch
        
        if (qo == QO_FULL) goto net_mesta;
                
        //----------------------------------------------------------------------
        switch (packet->adr_dst){
        case ADR_PC:{
            if (DEBUG_ROUTER) printf_dos("Copy paket to PC\n");
            q_to_pc.w_buf = pb[i]->buf;
            q_to_pc.w_len = PACKET_SIZE;
            push_data_queue( &q_to_pc );
            break;
        }
        case ADR_DRV:{
            if (DEBUG_ROUTER) printf_dos("Copy paket to DRV\n");
            q_to_drv.w_buf = pb[i]->buf;
            q_to_drv.w_len = PACKET_SIZE;
            push_data_queue( &q_to_drv );
            break;
        }
        case ADR_TDRV:{
            if (DEBUG_ROUTER) printf_dos("Copy paket to TASK DRV\n");
            res = xQueueSendToBack( xPakRouterToTDrv, pb[i]->buf, 0);
            if (res != pdPASS) printf_dos("TASK ROUTE: error - xQueueSendToBack( packet_from_tdrv.q, packet_from_tdrv.buf\n");
            break;
        }
        case ADR_TLOG:{
            if (DEBUG_ROUTER) printf_dos("Copy paket to TASK LOG\n");
            res = xQueueSendToBack( xPakRouterToTLog, pb[i]->buf, 0);
            if (res != pdPASS) printf_dos("TASK ROUTE: error - xQueueSendToBack( packet_from_tdrv.q, packet_from_tdrv.buf\n");
            break;
        }
        default:{
            printf_dos("TASK ROUTER: error2 - packet adr dst[%d] error !\n", packet->adr_dst);
            hex_out((char const*)pb[ i ]->buf, PACKET_SIZE);
            pb[ i ]->flag = FP_NO;
            break;
        }
        } // switch
                
        pb[ i ]->flag = FP_NO;
        continue; // for FOR

    net_mesta:
        if (pb[i]->t <= time){
            printf_dos("TASK ROUTE: error 3 - Paket slihkom dolgo nevoshono otoslat, Udalaem paket iz ocheridi.\n");
            pb[ i ]->flag = FP_NO;
        }
    
    } // for -------------------------------------------------------------------

    if( get_data_size_queue(&q_to_drv) > 0 && uart_drv_flag_tx == 0){
        if (DEBUG_ROUTER) printf_dos("TX packet to DRV.\n");
        uart_drv_tx_dma();
    }

    if( get_data_size_queue(&q_to_pc) > 0 && uart_pc_flag_tx == 0){
        if (DEBUG_ROUTER) printf_dos("TX packet to PC.\n");
        uart_pc_tx_dma();
    }

    vTaskDelay(1);

WHILE_END

}

