// ������������ ������ � �������

#include "main.h"
#include "hardware.h"
#include "printf_hal.h"
#include "gpio_745.h"
#include "usart_745.h"
#include "rcc_745.h"
#include "gpio_745.h"
#include "dma_745.h"
#include "time_hal.h"

#ifndef DEBUG_HARD
#define DEBUG_HARD 0
#endif

//------------------------------------------------------------------------------
// usart3( 54 MHz ) - DRV 
//------------------------------------------------------------------------------
void uart_drv_init(void)
{
    usart_cr1_reg_ut usart_cr1;
    usart_cr2_reg_ut usart_cr2;
    usart_cr3_reg_ut usart_cr3;
    dma_cr_reg_ut dma_cfg;
    
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->APB1ENR, RCC_APB1ENR_USART3EN); // USART3 CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_DMA1EN);   // DMA1 CLK EN
    
    
    // gpio_init
    gpio_init(USART3_PORT_RX, USART3_RX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART3);
    gpio_init(USART3_PORT_TX, USART3_TX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART3);
    

    dma_cfg.reg.en = 0;    // 0: stream disabled
    dma_cfg.reg.dmeie=1;   // 1: DME interrupt enabled
    dma_cfg.reg.teie=1;    // 1: TE interrupt enabled
    dma_cfg.reg.htie=0;    // 0: HT interrupt disabled
    dma_cfg.reg.tcie=1;    // 1: TC interrupt enabled
    dma_cfg.reg.pfctrl=0;  // peripheral flow controller(0: DMA is the flow controller)
    dma_cfg.reg.dir=1;     // 01: memory-to-peripheral
    dma_cfg.reg.circ=0;    // 0: circular mode disabled
    dma_cfg.reg.pinc=0;    // peripheral increment mode(0 - peripheral address pointer is fixed)
    dma_cfg.reg.minc=1;    // memory increment mode(memory address pointer is incremented after each data transfer)
    dma_cfg.reg.psize=0;   // peripheral data size(byte (8-bit))
    dma_cfg.reg.msize=0;   // memory data size (byte (8-bit))
    dma_cfg.reg.pincos=0;  // peripheral increment offset size
    dma_cfg.reg.pl=0;      // priority level=00
    dma_cfg.reg.dbm=0;     // double-buffer mode = 0
    dma_cfg.reg.ct=0;      // current target (only in double-buffer mode)
    dma_cfg.reg.pburst=0;  // single transfer
    dma_cfg.reg.mburst=0;  // single transfer
    dma_cfg.reg.chsel= 4;  // ch 4

    dma_init(DMA1_Stream3,             // DMA stream
             (uint32_t)&(USART3->TDR), // adr reg
             0,                     // adr ram 0
             0,                     // adr ram 1
             0,                     // size
             dma_cfg.data,          // dma config
             0                      // dma fifo config
             );

    NVIC_SetPriority(DMA1_Stream3_IRQn, 5);
    NVIC_EnableIRQ(DMA1_Stream3_IRQn);
    
    // USART INIT --------------------------------------------------------------
    
    USART3->CR1 = 0; // clear
    USART3->CR2 = 0; // clear
    USART3->CR3 = 0; // clear
    USART3->BRR = 0; // clear
    
    // Tx/Rx baud = fCK / USARTDIV - for 16 oversampling
    // USARTDIV = fCK / Tx/Rx baud
    USART3->BRR = (54000000UL / 115200UL) & 0xffff;
    
    
    usart_cr2.data = 0;    
    usart_cr2.reg.addm7   = 0; // ADDM7:7-bit Address Detection/4-bit Address Detection
    usart_cr2.reg.lbdl    = 0; // LBDL: LIN break detection length
    usart_cr2.reg.lbdie   = 0; // LBDIE: LIN break detection interrupt enable
    usart_cr2.reg.lbcl    = 0; // LBCL: Last bit clock pulse
    usart_cr2.reg.cpha    = 0; // CPHA: Clock phase
    usart_cr2.reg.cpol    = 0; // CPOL: Clock polarity
    usart_cr2.reg.clken   = 0; // CLKEN: Clock enable
    usart_cr2.reg.stop    = 0; // STOP[1:0]: STOP bits, 00: 1 stop bit
    usart_cr2.reg.linen   = 0; // LINEN: LIN mode enable
    usart_cr2.reg.swap    = 0; // SWAP: Swap TX/RX pins
    usart_cr2.reg.rxinv   = 0; // RXINV: RX pin active level inversion
    usart_cr2.reg.txinv   = 0; // TXINV: TX pin active level inversion
    usart_cr2.reg.datainv = 0; // DATAINV: Binary data inversion
    usart_cr2.reg.msbfirst= 0; // MSBFIRST: Most significant bit first
    usart_cr2.reg.abren   = 0; // Receiver timeout enable
    usart_cr2.reg.abrmod  = 0; // Auto baud rate mode
    usart_cr2.reg.rtoen   = 0; // RTOEN: Receiver timeout enable
    usart_cr2.reg.add_l   = 0; // Address of the USART node
    usart_cr2.reg.add_h   = 0; // Address of the USART node
    USART3->CR2 = usart_cr2.data;
    
    usart_cr3.data = 0;    
    usart_cr3.reg.eie      = 0; // EIE: Error interrupt enable
    usart_cr3.reg.iren     = 0; // IREN: IrDA mode enable
    usart_cr3.reg.irlp     = 0; // IRLP: IrDA low-power
    usart_cr3.reg.hdsel    = 0; // HDSEL: Half-duplex selection
    usart_cr3.reg.nack     = 0; // NACK: Smartcard NACK enable
    usart_cr3.reg.scen     = 0; // SCEN: Smartcard mode enable
    usart_cr3.reg.dmar     = 0; // DMAR: DMA enable receiver
    usart_cr3.reg.dmat     = 1; // DMAT: DMA enable transmitter
    usart_cr3.reg.rtse     = 0; // RTSE: RTS enable
    usart_cr3.reg.ctse     = 0; // CTSE: CTS enable
    usart_cr3.reg.ctsie    = 0; // CTSIE: CTS interrupt enable
    usart_cr3.reg.onebit   = 1; // ONEBIT: One sample bit method enable, 1: One sample bit method NF-disable
    usart_cr3.reg.ovrdis   = 1; // OVRDIS: Overrun Disable,1: Overrun functionality is disabled.
    usart_cr3.reg.ddre     = 1; // DDRE: DMA Disable on Reception Error
    usart_cr3.reg.dem      = 0; // DEM: Driver enable mode
    usart_cr3.reg.dep      = 0; // DEP: Driver enable polarity selection
    usart_cr3.reg.scarcnt0 = 0; //
    usart_cr3.reg.scarcnt1 = 0; //
    usart_cr3.reg.scarcnt2 = 0; // SCARCNT[2:0]: Smartcard auto-retry count
    USART3->CR3 = usart_cr3.data;
    
    usart_cr1.data      = 0;
    usart_cr1.reg.ue    = 1; // UE: USART enable, 
    usart_cr1.reg.re    = 1; // RE: Receiver enable, 1: Receiver is enabled
    usart_cr1.reg.te    = 1; // TE: Transmitter enable, 1: Transmitter is enabled 
    usart_cr1.reg.idleie = 0;// IDLEIE: IDLE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.rxneie = 1;// RXNEIE: RXNE interrupt enable, 1: A USART interrupt is generated whenever ORE=1 or RXNE=1
    usart_cr1.reg.tcie  = 0; // TCIE: Transmission complete interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.txeie = 0; // TXEIE: interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.peie  = 0; // PEIE: PE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.ps    = 0; // PS: Parity selection, 0: Even parity
    usart_cr1.reg.pce   = 0; // PCE: Parity control enable, 0: Parity control disabled
    usart_cr1.reg.wake  = 0; // WAKE: Receiver wakeup method, 0: Idle line
    usart_cr1.reg.m0    = 0; // M0: Word length, ������ ��� m1 !
    usart_cr1.reg.mme   = 0; // MME: Mute mode enable, 0: Receiver in active mode permanently
    usart_cr1.reg.cmie  = 0; // CMIE: Character match interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.over8 = 0; // OVER8: Oversampling mode, 0: Oversampling by 16; 1: Oversampling by 8
    usart_cr1.reg.dedt  = 0; // DEDT[4:0]: Driver Enable de-assertion time
    usart_cr1.reg.deat  = 0; // DEAT[4:0]: Driver Enable assertion time, 
    usart_cr1.reg.rtoie = 0; // RTOIE: Receiver timeout interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.eobie = 0; // EOBIE: End of Block interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.m1    = 0; // M1: Word length, 00: 1 Start bit, 8 data bits, n stop bits
    USART3->CR1 = usart_cr1.data;
    
    NVIC_SetPriority(USART3_IRQn, 5);
    NVIC_EnableIRQ(USART3_IRQn);
}


//------------------------------------------------------------------------------
// usart2( 54 MHz ) - PC 
//------------------------------------------------------------------------------
void uart_pc_init(void)
{
    usart_cr1_reg_ut usart_cr1;
    usart_cr2_reg_ut usart_cr2;
    usart_cr3_reg_ut usart_cr3;
    dma_cr_reg_ut dma_cfg;
    
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->APB1ENR, RCC_APB1ENR_USART2EN); // USART2 CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_DMA1EN);   // DMA1 CLK EN
    
    
    // gpio_init
    gpio_init(USART2_PORT_RX, USART2_RX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART2);
    gpio_init(USART2_PORT_TX, USART2_TX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART2);
    

    dma_cfg.reg.en = 0;    // 0: stream disabled
    dma_cfg.reg.dmeie=1;   // 1: DME interrupt enabled
    dma_cfg.reg.teie=1;    // 1: TE interrupt enabled
    dma_cfg.reg.htie=0;    // 0: HT interrupt disabled
    dma_cfg.reg.tcie=1;    // 1: TC interrupt enabled
    dma_cfg.reg.pfctrl=0;  // peripheral flow controller(0: DMA is the flow controller)
    dma_cfg.reg.dir=1;     // 01: memory-to-peripheral
    dma_cfg.reg.circ=0;    // 0: circular mode disabled
    dma_cfg.reg.pinc=0;    // peripheral increment mode(0 - peripheral address pointer is fixed)
    dma_cfg.reg.minc=1;    // memory increment mode(memory address pointer is incremented after each data transfer)
    dma_cfg.reg.psize=0;   // peripheral data size(byte (8-bit))
    dma_cfg.reg.msize=0;   // memory data size (byte (8-bit))
    dma_cfg.reg.pincos=0;  // peripheral increment offset size
    dma_cfg.reg.pl=0;      // priority level=00
    dma_cfg.reg.dbm=0;     // double-buffer mode = 0
    dma_cfg.reg.ct=0;      // current target (only in double-buffer mode)
    dma_cfg.reg.pburst=0;  // single transfer
    dma_cfg.reg.mburst=0;  // single transfer
    dma_cfg.reg.chsel= 4;  // ch 4

    dma_init(DMA1_Stream6,             // DMA stream
             (uint32_t)&(USART2->TDR), // adr reg
             0,                     // adr ram 0
             0,                     // adr ram 1
             0,                     // size
             dma_cfg.data,          // dma config
             0                      // dma fifo config
             );

    NVIC_SetPriority(DMA1_Stream6_IRQn, 5);
    NVIC_EnableIRQ(DMA1_Stream6_IRQn);
    
    // USART INIT --------------------------------------------------------------
    
    USART2->CR1 = 0; // clear
    USART2->CR2 = 0; // clear
    USART2->CR3 = 0; // clear
    USART2->BRR = 0; // clear
    
    // Tx/Rx baud = fCK / USARTDIV - for 16 oversampling
    // USARTDIV = fCK / Tx/Rx baud
    USART2->BRR = (54000000UL / 115200UL) & 0xffff;
    
    
    usart_cr2.data = 0;    
    usart_cr2.reg.addm7   = 0; // ADDM7:7-bit Address Detection/4-bit Address Detection
    usart_cr2.reg.lbdl    = 0; // LBDL: LIN break detection length
    usart_cr2.reg.lbdie   = 0; // LBDIE: LIN break detection interrupt enable
    usart_cr2.reg.lbcl    = 0; // LBCL: Last bit clock pulse
    usart_cr2.reg.cpha    = 0; // CPHA: Clock phase
    usart_cr2.reg.cpol    = 0; // CPOL: Clock polarity
    usart_cr2.reg.clken   = 0; // CLKEN: Clock enable
    usart_cr2.reg.stop    = 0; // STOP[1:0]: STOP bits, 00: 1 stop bit
    usart_cr2.reg.linen   = 0; // LINEN: LIN mode enable
    usart_cr2.reg.swap    = 0; // SWAP: Swap TX/RX pins
    usart_cr2.reg.rxinv   = 0; // RXINV: RX pin active level inversion
    usart_cr2.reg.txinv   = 0; // TXINV: TX pin active level inversion
    usart_cr2.reg.datainv = 0; // DATAINV: Binary data inversion
    usart_cr2.reg.msbfirst= 0; // MSBFIRST: Most significant bit first
    usart_cr2.reg.abren   = 0; // Receiver timeout enable
    usart_cr2.reg.abrmod  = 0; // Auto baud rate mode
    usart_cr2.reg.rtoen   = 0; // RTOEN: Receiver timeout enable
    usart_cr2.reg.add_l   = 0; // Address of the USART node
    usart_cr2.reg.add_h   = 0; // Address of the USART node
    USART2->CR2 = usart_cr2.data;
    
    usart_cr3.data = 0;    
    usart_cr3.reg.eie      = 0; // EIE: Error interrupt enable
    usart_cr3.reg.iren     = 0; // IREN: IrDA mode enable
    usart_cr3.reg.irlp     = 0; // IRLP: IrDA low-power
    usart_cr3.reg.hdsel    = 0; // HDSEL: Half-duplex selection
    usart_cr3.reg.nack     = 0; // NACK: Smartcard NACK enable
    usart_cr3.reg.scen     = 0; // SCEN: Smartcard mode enable
    usart_cr3.reg.dmar     = 0; // DMAR: DMA enable receiver
    usart_cr3.reg.dmat     = 1; // DMAT: DMA enable transmitter
    usart_cr3.reg.rtse     = 0; // RTSE: RTS enable
    usart_cr3.reg.ctse     = 0; // CTSE: CTS enable
    usart_cr3.reg.ctsie    = 0; // CTSIE: CTS interrupt enable
    usart_cr3.reg.onebit   = 1; // ONEBIT: One sample bit method enable, 1: One sample bit method NF-disable
    usart_cr3.reg.ovrdis   = 1; // OVRDIS: Overrun Disable,1: Overrun functionality is disabled.
    usart_cr3.reg.ddre     = 1; // DDRE: DMA Disable on Reception Error
    usart_cr3.reg.dem      = 0; // DEM: Driver enable mode
    usart_cr3.reg.dep      = 0; // DEP: Driver enable polarity selection
    usart_cr3.reg.scarcnt0 = 0; //
    usart_cr3.reg.scarcnt1 = 0; //
    usart_cr3.reg.scarcnt2 = 0; // SCARCNT[2:0]: Smartcard auto-retry count
    USART2->CR3 = usart_cr3.data;
    
    usart_cr1.data      = 0;
    usart_cr1.reg.ue    = 1; // UE: USART enable, 
    usart_cr1.reg.re    = 1; // RE: Receiver enable, 1: Receiver is enabled
    usart_cr1.reg.te    = 1; // TE: Transmitter enable, 1: Transmitter is enabled 
    usart_cr1.reg.idleie = 0;// IDLEIE: IDLE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.rxneie = 1;// RXNEIE: RXNE interrupt enable, 1: A USART interrupt is generated whenever ORE=1 or RXNE=1
    usart_cr1.reg.tcie  = 0; // TCIE: Transmission complete interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.txeie = 0; // TXEIE: interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.peie  = 0; // PEIE: PE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.ps    = 0; // PS: Parity selection, 0: Even parity
    usart_cr1.reg.pce   = 0; // PCE: Parity control enable, 0: Parity control disabled
    usart_cr1.reg.wake  = 0; // WAKE: Receiver wakeup method, 0: Idle line
    usart_cr1.reg.m0    = 0; // M0: Word length, ������ ��� m1 !
    usart_cr1.reg.mme   = 0; // MME: Mute mode enable, 0: Receiver in active mode permanently
    usart_cr1.reg.cmie  = 0; // CMIE: Character match interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.over8 = 0; // OVER8: Oversampling mode, 0: Oversampling by 16; 1: Oversampling by 8
    usart_cr1.reg.dedt  = 0; // DEDT[4:0]: Driver Enable de-assertion time
    usart_cr1.reg.deat  = 0; // DEAT[4:0]: Driver Enable assertion time, 
    usart_cr1.reg.rtoie = 0; // RTOIE: Receiver timeout interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.eobie = 0; // EOBIE: End of Block interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.m1    = 0; // M1: Word length, 00: 1 Start bit, 8 data bits, n stop bits
    USART2->CR1 = usart_cr1.data;
    
    NVIC_SetPriority(USART2_IRQn, 5);
    NVIC_EnableIRQ(USART2_IRQn);
}

//------------------------------------------------------------------------------
// usart1( 108 MHz ) - consol init 
//------------------------------------------------------------------------------
void uart_console_init(void)
{
    usart_cr1_reg_ut usart_cr1;
    usart_cr2_reg_ut usart_cr2;
    usart_cr3_reg_ut usart_cr3;
    dma_cr_reg_ut dma_cfg;
    
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->APB2ENR, RCC_APB2ENR_USART1EN); // USART1 CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_DMA2EN);   // DMA2 CLK EN
    
    
    // gpio_init
    gpio_init(USART1_PORT, USART1_RX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART1);
    gpio_init(USART1_PORT, USART1_TX, GPIO_MODE_AF, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF7_USART1);
    




   
    dma_cfg.reg.en = 0;    // 0: stream disabled
    dma_cfg.reg.dmeie=1;   // 1: DME interrupt enabled
    dma_cfg.reg.teie=1;    // 1: TE interrupt enabled
    dma_cfg.reg.htie=0;    // 0: HT interrupt disabled
    dma_cfg.reg.tcie=1;    // 1: TC interrupt enabled
    dma_cfg.reg.pfctrl=0;  // peripheral flow controller(0: DMA is the flow controller)
    dma_cfg.reg.dir=1;     // 01: memory-to-peripheral
    dma_cfg.reg.circ=0;    // 0: circular mode disabled
    dma_cfg.reg.pinc=0;    // peripheral increment mode(0 - peripheral address pointer is fixed)
    dma_cfg.reg.minc=1;    // memory increment mode(memory address pointer is incremented after each data transfer)
    dma_cfg.reg.psize=0;   // peripheral data size(byte (8-bit))
    dma_cfg.reg.msize=0;   // memory data size (byte (8-bit))
    dma_cfg.reg.pincos=0;  // peripheral increment offset size
    dma_cfg.reg.pl=0;      // priority level=00
    dma_cfg.reg.dbm=0;     // double-buffer mode = 0
    dma_cfg.reg.ct=0;      // current target (only in double-buffer mode)
    dma_cfg.reg.pburst=0;  // single transfer
    dma_cfg.reg.mburst=0;  // single transfer
    dma_cfg.reg.chsel= 4;  // ch 4

    dma_init(DMA2_Stream7,             // DMA stream
             (uint32_t)&(USART1->TDR), // adr reg
             0,                     // adr ram 0
             0,                     // adr ram 1
             0,                     // size
             dma_cfg.data,          // dma config
             0                      // dma fifo config
             );

    NVIC_SetPriority(DMA2_Stream7_IRQn, 5);
    NVIC_EnableIRQ(DMA2_Stream7_IRQn);
    
    // USART INIT --------------------------------------------------------------
    
    USART1->CR1 = 0; // clear
    USART1->CR2 = 0; // clear
    USART1->CR3 = 0; // clear
    USART1->BRR = 0; // clear
    
    // Tx/Rx baud = fCK / USARTDIV - for 16 oversampling
    // USARTDIV = fCK / Tx/Rx baud
    USART1->BRR = (108000000UL / 115200UL) & 0xffff;
    
    
    usart_cr2.data = 0;    
    usart_cr2.reg.addm7   = 0; // ADDM7:7-bit Address Detection/4-bit Address Detection
    usart_cr2.reg.lbdl    = 0; // LBDL: LIN break detection length
    usart_cr2.reg.lbdie   = 0; // LBDIE: LIN break detection interrupt enable
    usart_cr2.reg.lbcl    = 0; // LBCL: Last bit clock pulse
    usart_cr2.reg.cpha    = 0; // CPHA: Clock phase
    usart_cr2.reg.cpol    = 0; // CPOL: Clock polarity
    usart_cr2.reg.clken   = 0; // CLKEN: Clock enable
    usart_cr2.reg.stop    = 0; // STOP[1:0]: STOP bits, 00: 1 stop bit
    usart_cr2.reg.linen   = 0; // LINEN: LIN mode enable
    usart_cr2.reg.swap    = 0; // SWAP: Swap TX/RX pins
    usart_cr2.reg.rxinv   = 0; // RXINV: RX pin active level inversion
    usart_cr2.reg.txinv   = 0; // TXINV: TX pin active level inversion
    usart_cr2.reg.datainv = 0; // DATAINV: Binary data inversion
    usart_cr2.reg.msbfirst= 0; // MSBFIRST: Most significant bit first
    usart_cr2.reg.abren   = 0; // Receiver timeout enable
    usart_cr2.reg.abrmod  = 0; // Auto baud rate mode
    usart_cr2.reg.rtoen   = 0; // RTOEN: Receiver timeout enable
    usart_cr2.reg.add_l   = 0; // Address of the USART node
    usart_cr2.reg.add_h   = 0; // Address of the USART node
    USART1->CR2 = usart_cr2.data;
    
    usart_cr3.data = 0;    
    usart_cr3.reg.eie      = 0; // EIE: Error interrupt enable
    usart_cr3.reg.iren     = 0; // IREN: IrDA mode enable
    usart_cr3.reg.irlp     = 0; // IRLP: IrDA low-power
    usart_cr3.reg.hdsel    = 0; // HDSEL: Half-duplex selection
    usart_cr3.reg.nack     = 0; // NACK: Smartcard NACK enable
    usart_cr3.reg.scen     = 0; // SCEN: Smartcard mode enable
    usart_cr3.reg.dmar     = 0; // DMAR: DMA enable receiver
    usart_cr3.reg.dmat     = 1; // DMAT: DMA enable transmitter
    usart_cr3.reg.rtse     = 0; // RTSE: RTS enable
    usart_cr3.reg.ctse     = 0; // CTSE: CTS enable
    usart_cr3.reg.ctsie    = 0; // CTSIE: CTS interrupt enable
    usart_cr3.reg.onebit   = 1; // ONEBIT: One sample bit method enable, 1: One sample bit method NF-disable
    usart_cr3.reg.ovrdis   = 1; // OVRDIS: Overrun Disable,1: Overrun functionality is disabled.
    usart_cr3.reg.ddre     = 1; // DDRE: DMA Disable on Reception Error
    usart_cr3.reg.dem      = 0; // DEM: Driver enable mode
    usart_cr3.reg.dep      = 0; // DEP: Driver enable polarity selection
    usart_cr3.reg.scarcnt0 = 0; //
    usart_cr3.reg.scarcnt1 = 0; //
    usart_cr3.reg.scarcnt2 = 0; // SCARCNT[2:0]: Smartcard auto-retry count
    USART1->CR3 = usart_cr3.data;
    
    usart_cr1.data      = 0;
    usart_cr1.reg.ue    = 1; // UE: USART enable, 
    usart_cr1.reg.re    = 1; // RE: Receiver enable, 1: Receiver is enabled
    usart_cr1.reg.te    = 1; // TE: Transmitter enable, 1: Transmitter is enabled 
    usart_cr1.reg.idleie = 0;// IDLEIE: IDLE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.rxneie = 1;// RXNEIE: RXNE interrupt enable, 1: A USART interrupt is generated whenever ORE=1 or RXNE=1
    usart_cr1.reg.tcie  = 0; // TCIE: Transmission complete interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.txeie = 0; // TXEIE: interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.peie  = 0; // PEIE: PE interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.ps    = 0; // PS: Parity selection, 0: Even parity
    usart_cr1.reg.pce   = 0; // PCE: Parity control enable, 0: Parity control disabled
    usart_cr1.reg.wake  = 0; // WAKE: Receiver wakeup method, 0: Idle line
    usart_cr1.reg.m0    = 0; // M0: Word length, ������ ��� m1 !
    usart_cr1.reg.mme   = 0; // MME: Mute mode enable, 0: Receiver in active mode permanently
    usart_cr1.reg.cmie  = 0; // CMIE: Character match interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.over8 = 0; // OVER8: Oversampling mode, 0: Oversampling by 16; 1: Oversampling by 8
    usart_cr1.reg.dedt  = 0; // DEDT[4:0]: Driver Enable de-assertion time
    usart_cr1.reg.deat  = 0; // DEAT[4:0]: Driver Enable assertion time, 
    usart_cr1.reg.rtoie = 0; // RTOIE: Receiver timeout interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.eobie = 0; // EOBIE: End of Block interrupt enable, 0: Interrupt is inhibited
    usart_cr1.reg.m1    = 0; // M1: Word length, 00: 1 Start bit, 8 data bits, n stop bits
    USART1->CR1 = usart_cr1.data;
    
    
    NVIC_SetPriority(USART1_IRQn, 5);
    NVIC_EnableIRQ(USART1_IRQn);

}



//-----------------------------------------------------------------------------
// �������� - ��������� �������� ������
//-----------------------------------------------------------------------------
//void buzz(buzz_t b)
//{
//    if (DEBUG_HARD) printf_dos("BUZZ:(ne rabotaet, nado dopisat) %d\n", b);
//}

//-----------------------------------------------------------------------------
// �������� ��������� PG - ������� ������� �������� (������� - ������)
// ��� ���������� �� ���� - ��
//-----------------------------------------------------------------------------
input_dat_state_e get_sensor_power( void )
{
    if (gpio_get(PG_VOUT_H_PORT, PG_VOUT_H))
        return I_ON;
    else
        return I_OFF;
}

//-----------------------------------------------------------------------------
// �������� ������� ������� �������� (������� - ������)
//-----------------------------------------------------------------------------
void sensor_power_on( void )
{
    gpio_set_1(V20_ENA_H_PORT, V20_ENA_H);
}

//-----------------------------------------------------------------------------
// ��������� ������� ������� �������� (������� - ������)
//-----------------------------------------------------------------------------
void sensor_power_off( void )
{
    gpio_set_0(V20_ENA_H_PORT, V20_ENA_H);
}

//-----------------------------------------------------------------------------
// ������ ��������� ����������� ������������� - ��� �������� ������ !
//-----------------------------------------------------------------------------
pdu_state_e get_pdu_state( void )
{
    uint32_t i_close, i_auto, i_exit, i_winter, i_open;
    int c0 = 0; // ������� ���������� ��������� ��������� - ����������� �������������(�����������)

    i_close = gpio_get(PDU_CLOSE_L_PORT, PDU_CLOSE_L);
    i_auto  = gpio_get(PDU_AUTO_L_PORT, PDU_AUTO_L);
    i_exit  = gpio_get(PDU_EXIT_L_PORT, PDU_EXIT_L);
    i_winter = gpio_get(PDU_WINTER_L_PORT, PDU_WINTER_L);
    i_open  = gpio_get(PDU_OPEN_L_PORT, PDU_OPEN_L);

    if (i_close == 0) c0++;
    if (i_auto == 0) c0++;
    if (i_exit == 0) c0++;
    if (i_winter == 0) c0++;
    if (i_open == 0) c0++;

    if (c0 > 1 || c0 == 0) return I_NONE; // ������ ���������� ���������� ��������� �������������

    if (i_close == 0) return I_CLOSE;
    if (i_auto == 0) return I_AUTO;
    if (i_exit == 0) return I_EXIT;
    if (i_winter == 0) return I_WINTER;
    if (i_open == 0) return I_OPEN;

    return I_NONE;
}

//-----------------------------------------------------------------------------
// ��������� ������ - ������� � �������
//-----------------------------------------------------------------------------
void dat_init(void)
{
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOCEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOEEN);  // GPIO CLK EN
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOFEN);  // GPIO CLK EN
    
    
    // gpio_init
    gpio_init(RDR1_L_PORT, RDR1_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(RDR2_L_PORT, RDR2_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(BAR1_L_PORT, BAR1_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(BAR2_L_PORT, BAR2_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    
    gpio_init(FIRE_L_PORT, FIRE_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);

    gpio_init(PDU_CLOSE_L_PORT, PDU_CLOSE_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_AUTO_L_PORT, PDU_AUTO_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_EXIT_L_PORT, PDU_EXIT_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_WINTER_L_PORT, PDU_WINTER_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_OPEN_L_PORT, PDU_OPEN_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_STOP_L_PORT, PDU_STOP_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(PDU_TEST_L_PORT, PDU_TEST_L, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    
    gpio_init(PG_VOUT_H_PORT, PG_VOUT_H, GPIO_MODE_IN, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_init(V20_ENA_H_PORT, V20_ENA_H, GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    gpio_set_0(V20_ENA_H_PORT, V20_ENA_H); // ���� ������� ��������
    

   
}


//-----------------------------------------------------------------------------
// ������ ��������� ������ 1
//-----------------------------------------------------------------------------
input_dat_state_e get_radar1_state( void )
{
    if (0 == gpio_get(RDR1_L_PORT, RDR1_L))
        return I_ON;
    else
        return I_OFF;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������ 2
//-----------------------------------------------------------------------------
input_dat_state_e get_radar2_state( void )
{
    if (0 == gpio_get(RDR2_L_PORT, RDR2_L))
        return I_ON;
    else
        return I_OFF;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������� 1
// ��������:
// ���� �� ������ ������ ����, ���� = 0 - ��� �����������
//                   ��� ����, ���� = 1 - ���� �����������
// �.�. ��������
//-----------------------------------------------------------------------------
input_dat_state_e get_bar1_state( void )
{
    if (0 == gpio_get(BAR1_L_PORT, BAR1_L))
        return I_OFF;
    else
        return I_ON;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������� 2
// ��������:
// ���� �� ������ ������ ����, ���� = 0 - ��� �����������
//                   ��� ����, ���� = 1 - ���� �����������
// �.�. ��������
//-----------------------------------------------------------------------------
input_dat_state_e get_bar2_state( void )
{
    if (0 == gpio_get(BAR2_L_PORT, BAR2_L))
        return I_OFF;
    else
        return I_ON;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������ - STOP
// ��������� ���������
// �.�. ��������
//-----------------------------------------------------------------------------
input_dat_state_e get_key_stop_state( void )
{
    if (0 == gpio_get(PDU_STOP_L_PORT, PDU_STOP_L))
        return I_OFF;
    else
        return I_ON;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������ - TEST
//-----------------------------------------------------------------------------
input_dat_state_e get_key_test_state( void )
{
    if (0 == gpio_get(PDU_TEST_L_PORT, PDU_TEST_L))
        return I_ON;
    else
        return I_OFF;
}

//-----------------------------------------------------------------------------
// ������ ��������� ������� - �������� ������������
//-----------------------------------------------------------------------------
input_dat_state_e get_fire_state( void )
{
    if (0 == gpio_get(FIRE_L_PORT, FIRE_L))
        return I_ON;
    else
        return I_OFF;
}

//-----------------------------------------------------------------------------
// RESET (���������� �����) - ����� ��������
//-----------------------------------------------------------------------------
void driver_pcb_reset( void )
{
    gpio_set_0(DR_NRST_PORT, DR_NRST);
    delay_ms(100); // delay 100 ms
    gpio_set_1(DR_NRST_PORT, DR_NRST);
    delay_ms(100); // delay 100 ms
}
//-----------------------------------------------------------------------------
// Init RESET ����� ��������
// ����� ����� �� ������ 0 - �.�. �����
//-----------------------------------------------------------------------------
void driver_pcb_reset_init( void )
{
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);  // GPIO CLK EN
    gpio_init(DR_NRST_PORT, DR_NRST, GPIO_MODE_OUT, GPIO_OTYPE_OPENDRAIN, GPIO_SPEEDF_HIGH, GPIO_PULL_NO, 0);
}
