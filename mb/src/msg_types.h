#ifndef MSG_TYPES_H_
#define MSG_TYPES_H_

#define RET_S_OK         (0)
#define RET_S_ERR        (1)
#define RET_S_ERR_BLOCK  (2) // ������ ������ �����
#define RET_S_ERR_BLOCKW (3) // ������ ������ �����

typedef enum {
    RET_OK = 0,
    RET_ERR = 1
} result_t;

// �������� ��� �������� ������ �� ����� ������ ������� �� �������� � �� ������
// � ����� ������ ������� ��������� ������ ������� ����� ����������� ����� ���� ��� �� (��� �� �����������)
typedef struct {
    uint32_t i2c_u20:1; 
    uint32_t i2c_u4:1; 
    uint32_t log:1; 
    uint32_t rtc:1; 

} device_post_t;


// ��������������� ��������� � ������ � ������ �������
typedef enum {
    M_NONE = 0,
    M_START,          //1 ����� ������� - ����� ������� ����� ��������� ������
    M_PG,             //2 ����������� ������� �� ��������
    M_RTC_BATT,       //3 ����������� ��������� U3
    M_I2C_MAC,        //4 ������ ��� ������ U20
    M_I2C_FRAM,       //5 ������ ��� ������ U4
    M_SPI_FLASH,      //6 ������ ��� ������ U6
    M_DRV,            //7 ����� DRV �� ��������, ������� ��� �� �������.

    M_DRV_PERIPH,     //8 ����� DRV ����� ���������
    M_DRV_NO_DATA,    //9 ����� DRV �� �������
    M_IN_VOL_LOW,     //10 ������ ������� ����������
    M_IN_VOL_NEG,     //11 ������������� ������� ���������� 
    M_ACC_VOL_NEG,    //12 ������������� ���������� ������������
    M_MOT_OVERCUR,    //13 ���������� �� ���� ������
    M_CALIBRATE_NO,   //14 ����� DRV �� �������������
    M_CALIBRATE_ERR,  //15 ������ ��������� ����������
    
    M_ENCODER_ERR,    //16 ������ ������ � ��������
    M_HIGH_WEIGHT,    //17 ����� ������� ������� 
    
    M_DRV_SILENT,     //18 ����� DRV �� �������� ��� �������
    M_DOOR_POS,       //19 ���������� ������� �������� �������
    M_ZAMOK_NONE,     //20 ����� �� �������� - ��� ��� (������)
    
    M_CALIBRATE,      //21 ������ ��������� ����������
    M_MOT_ENCODER,    //22 ��� ������� � �������� ������
    M_MOT_NONE,       //23 ����� �� ���������
    M_LOCK_NONE,      //24 ����� �� �������� - ��� ��� (������)
        
    
    
} msg_t;


// ��������� ����������� �������������
typedef enum {
    I_NONE = 0,  // �� ���������� - ����� �� ���������, ��� �������� ��������� �������� ������
    I_CLOSE,   // �������
    I_AUTO,    // �������
    I_EXIT,    // ������ �����
    I_WINTER,  // ������
    I_OPEN,    // �������
} pdu_state_e;

// ��������� ������� �������� �� ��������(��������. ������� � ������)
typedef enum {
    I_OFF = 0,  // �� ���������� - ����� �� ���������, ��� �������� ��������� �������� ������, ������ - ������
    I_ON,       // ������-������, ������ - ��������.
} input_dat_state_e;

//typedef enum {B_OFF, B_ON} buzz_t; // �������� ������ ������� - ��������


// ���������� � ������� ��������� - ��������, ������.
typedef struct{ 
    pdu_state_e prev;
    pdu_state_e current;
} prev_next_st_pdu_t;


// ��������� - ����������, ������� �������� ��������
typedef struct {
    prev_next_st_pdu_t pdu;
    input_dat_state_e test;
    input_dat_state_e stop;
    input_dat_state_e rdr1;
    input_dat_state_e rdr2;
    input_dat_state_e bar1;
    input_dat_state_e bar2;
    input_dat_state_e fire;
    input_dat_state_e pg;
} status_sensors_t;

// ��������� ����� � ������ "������ �����" (��-���)
typedef enum {
    LOCK_POS_OPEN  = 0, // ����� ������
    LOCK_POS_CLOSE = 1  // ����� ������
} lock_position_t;

// �������� �������� ��������� (��-���) END_POS_INV_YES, END_POS_INV_NO
typedef enum {
    END_POS_INV_NO  = 0,
    END_POS_INV_YES = 1 
} end_pos_env_t;

// ��������� ���������� � �������� ��������� (��-���)
typedef enum {
    MOTOR_STOP_NO  = 0, // ����� ���-������������
    MOTOR_STOP_YES = 1  // ����� ������������
} motor_stoping_pos_t;

// ������� �� ������� � �������� ��������� (��-���) 
typedef enum {
    BARRIERS_ACTIVE_NO  = 0,  // ������� �� ������� ���
    BARRIERS_ACTIVE_YES = 1   // ������� �� ������� ����
} barriers_active_t;

// �������� ��������� ������� �������� � ������ ����������� ������ ���������� (������������ �� ������ � ������ ��������� ����������, �������� �� ���� ��� ����� ��)
#pragma pack(1)
typedef struct {
    uint16_t speed_open;            // �������� ��������  (��/���)
    uint16_t speed_close;           // �������� ��������  (��/���)
    uint16_t delay_open;            // �������� � �������� ��������� (���)
    uint16_t delay_close;           // �������� ����� ��������� (���)
    uint16_t lock_pos;              // ��������� ����� � ������ "������ �����" (��-���)
    uint16_t reaction_fire_input;   // ������� �� �������� ���� 1,2,3 - ���� �� ���������� � ��
    uint16_t delay_open_fire;       // �������� � �������� ��������� �� ������� ��������� ����� (���)
    uint16_t batt_job_n;            // ������ � �������������  1,2,3,4 - ������ �� ��
    uint16_t lock_type_num;         // ��� ����� 1,2,3

    uint16_t motor_stop;              // ��������� ���������� � �������� ��������� (��-���)
    uint16_t motor_i_max;             // ������������ ��� ��������� (�) - �� ��������
    uint16_t motor_v_max;             // ������������ ���������� ��������� (������) - �� ��������
    uint16_t speed_open_winter;       // �������� �������� ������ � ������ ������ (�.�)
    uint16_t speed_close_winter;      // �������� �������� ������ � ������ ������ (�.�)
    uint16_t delay_open_winter;       // �������� �������� ������ � ������ ������ (���)
    uint16_t open_width_in_winter;    // ������ �������� ������ � ������ ������ (%)
    uint16_t end_position_inversion;  // �������� �������� ��������� (��-���) END_POS_INV_YES, END_POS_INV_NO
    uint16_t max_position_error;      // ������������ ������ ������� (�.�)
    uint16_t max_error_count;         // ������������ ���������� ������ ������ (�.�)
    uint16_t calib_delay;             // ������ �������� ���������� ���������� (���)
    uint16_t barriers_active;         // ������� �� ������� � �������� ��������� (��-���) BARRIERS_ACTIVE_YES, BARRIERS_ACTIVE_NO
// �� ������������ - ���� �� ������������
// ��������� ����
//    uint32_t ip4_addr;
//    uint32_t ip4_net_mask;
//    uint32_t ip4_gw;
// ������� �������� - �����������

    uint16_t crc16;                // ����������� ����� - �� ��� ��������� ������������ ������

} device_config_t;
#pragma pack()

// ��������� ������
typedef enum {
    DOOR_OPEN = 0,   // ����� �������
    DOOR_CLOSE,      // ����� �������
    DOOR_OPEN_MOVE,  // ����� � �������� (�����������)
    DOOR_CLOSE_MOVE, // ����� � �������� (�����������)
    DOOR_CHECK,      // ����� ��������� ���� 
    DOOR_CALIB,      // ����� �����������
    DOOR_MANUAL,     // ����� �����������
    DOOR_ERROR       // ����� � ������
} door_state_t;

// ������� ��������� - ��������� ������.
typedef struct {
    door_state_t    state;  // ��������� ������
    uint8_t         pos;    // ������� ��������� ������ � %, �� 0 �� 100 % (�� ������� �������/�������)
    lock_position_t lock;   // ��������� �����, ������-������
    uint32_t        counter_open;  // ������� ���������� �������� ������
    uint32_t        counter_close; // ������� ���������� �������� ������
} door_curent_state_t;


#define PREAMBLE_32_BIT        (0xf3c3f00c)
#define PACKET_DATA_FIELD_SIZE (24)

#pragma pack(1)
typedef struct
{
    uint32_t    preamble;                     // ��������� ������������ ����� 32 ����
    uint8_t     adr_src:4;                    // ����� ���������
    uint8_t     adr_dst:4;                    // ����� ����������
    uint8_t     type;                         // ��� ������
    uint8_t     data[PACKET_DATA_FIELD_SIZE]; // ������
    uint16_t    crc;                          // crc-16
} packet_struct_t;
#pragma pack()

#define PACKET_SIZE            (sizeof(packet_struct_t))

typedef enum {
    ADR_PC = 0,                         // ��
    ADR_DRV,                            // ��������� �������� ������
    ADR_TDRV,                           // ������ ���������� ��������� ������
    ADR_TLOG,                           // ������ �����, ��������� ���������� � �.�.
    ADR_TPOST,                          // ������ ������������ ����������
        
//    ADR_MB,                             // �������� �����
//    ADR_DRV,                            // ����� ��������
//    ADR_SK                              // ������ ��������� - ���� �� �����������
} packet_adr_t;

//��� �������
typedef enum 
{
    SetElectData = 5,               //5- ��������� ���������� ������������� �������
    GetElectData,                   //6- ������ ���������� ������������� �������
    SetMechData,                    //7- ��������� ���������� ������������ �������
    GetMechData,                    //8- ������ ���������� ������������ �������
    SetDynData,                     //9- ��������� ���������� ������������ �������
    GetDynData,                     //10- ������ ���������� ������������ �������
    SetManCtrl,                     //11- ��������� ������� ������ ���������� ������
    SetAutoCtrl,                    //12- ��������� ��������������� ������ ���������� ������
    SetWintCtrl,                    //13- ��������� ��������������� ������ ���������� ������
    ResetWintCtrl,                  //14- ��������� ��������������� ������ ���������� ������
    GetCalibrate,                   //15- ���������� ����������
    SetDoorOpenSlow,                //16- ������� ����� ��������    
    SetDoorOpen,                    //17- ������� �����
    SetDoorClose,                   //18- ������� �����
    GetAccVolt,                     //19- ������ ���������� �� ������������
    SetAccOpen,                     //20- ������� ���������� ������������
    SetAccClose,                    //21- ������� ���������� ������������
    SetTime,                        //22- ��������� �������
    GetTime,                        //23- ������ �������
    GetErrorCount,                  //24-- ������ ���������� ������
    GetVoltageData,                 //25- ������ ���� ����������
    GetSystemData,                  //26- ������ ���������� ��������� �������
    GetErrorData,                   //27- ������ ���� ������
    ChangeReverse,                  //28- ������� ���������� ������
    SetHoldMotor,                   //29 - �������� ������� ����
    ResetHoldMotor,                 //30 - ��������� ������� ����
    SetSelfCheck,                   //31- �������� ������� ���������
    ResetSelfCheck,                 //32- �������� ������� ���������
    SetDoorStop,                    //33- ��������� �����
    SetServiceParam,                //34- ��������� ��������� ����������
    GetServiceParam,                //35- ������ ��������� ����������

    SetDiagData = 50,               //50- ������ ��������������� �������������
    GetDiagData,                    //51- ������ ��������������� �������������


    CMD_LOG_QUA = 100,              // ������ ���������� ������� �������� � ������� �����������. ������������ ������� 32 ����
    CMD_LOG_REC,                    // ������ 1 ������ �� ������� ����������� 


    CMD_VERSION = 200,              // ������ ������ �� � ��� �������� �� (��������� ��� ���������)(������ packet_soft_version_t)
    CMD_REBOOT,                     // ������������ (��� ������ � ������)
    CMD_FW_PARAM,                   // ��������� ������������ ����� �� (������ packet_fw_param_t)
    CMD_FW_DATA,                    // ������ �� ����� ��  (������ packet_fw_data_t)
    CMD_FW_DATA_WRITE,              // �������� ���� �� � FLASH ������ (������ packet_fw_data_write_t)
    CMD_FW_UPGRADE,                 // ������ ����� ���������� �� (������ packet_fw_upgrade_t)
    
        
} command_t; // ��� ������

//------------------------------------------------------------------------------
// FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW

// �������-�������� �����, FirmWare - ��� ��������� (fw_types.h) + ������ ��
#pragma pack(1)
typedef struct {
    uint8_t fw_types;   // ��� ��������� (��������� ��� ���������� ��).
    uint8_t soft_ver_h; // ������� ����� ������ �� - ���������� ��� ������ ���������� !!!
    uint8_t soft_ver_l; // ������� ����� ������ ��.
    uint8_t soft_ver_y; // ���, ��������� ��� ����� ����.
    char get_commit_str[]; // ������, ������� ����� GIT � ������� "(2019-09-06 07acc48)"
} packet_soft_version_t;
#pragma pack()



// �������-�������� �����, FirmWare ��������� ������������� �����
#pragma pack(1)
typedef struct {
	uint32_t file_size;   // ������ �����
	uint32_t block_size;  // ������ ����� ��� ������ � FLASH
	uint8_t  result;      // ����� - ��������� ���������� ��������
} packet_fw_param_t;
#pragma pack()

// ������� �����, FirmWare ������ - ������ �� ������
#pragma pack(1)
typedef struct {
	uint8_t data[24]; // ������ Firmware
} packet_fw_data_t;
#pragma pack()


// �������-�������� �����, �������� ���� � FLASH-������, 
// ���� �������� �� ��� ������ - ������������ ������ � ������������ ��������� �������� ����� � ������
#pragma pack(1)
typedef struct {
    uint8_t result;
} packet_fw_data_write_t;
#pragma pack()

// �������-�������� �����, FirmWare ��������� ���������� ��
#pragma pack(1)
typedef struct {
	uint8_t result; // ����� - ��������� ���������� ��������
} packet_fw_upgrade_t;
#pragma pack()

// FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW FW
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG

// LOG ��������� ������
#pragma pack(1)
typedef struct {
	uint32_t time;  // ����� � ��������
	uint8_t msg;    // �������
} log_record_t;
#pragma pack()

// �������� ���������� ������� �������� � flash ������
#pragma pack(1)
typedef struct {
    uint32_t count;
} packet_get_log_rec_quantity_t;
#pragma pack()

// �������� ������ �������� � flash ������
#pragma pack(1)
typedef struct {
    uint8_t count;       // ���������� ���������� ������� = 0 ��� 1
    log_record_t rec[1]; // ���� ������, ���� �����
} packet_get_log_rec_t;
#pragma pack()

// LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG
//------------------------------------------------------------------------------


#endif
