
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "port.h"
#include "main.h"
#include "string.h"
/* USER CODE BEGIN Includes */
#include "version.h"
#include "fw_types.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c2;
DMA_HandleTypeDef hdma_i2c2_rx;
DMA_HandleTypeDef hdma_i2c2_tx;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
static void MX_SYSTICK_Init(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_ADC_Convert(uint8_t chan, adc_chan_t conv);
static void MX_ADC_Start_IT(uint8_t chan, adc_chan_t conv);
static void MX_ADC_Stop_IT(void);
static void MX_I2C2_Init(void);
static void MX_USART1_UART_Init(uint8_t* rxbuf, uint8_t* txbuf);
static void MX_USART1_Transmit(packet_adr_t dst);
static void MX_FLASH_Init(void);
static void MX_FLASH_Backup(void);
static void MX_TIM1_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM6_Start_IT(void);
static void MX_TIM6_Stop_IT(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
trx_struct_t tx_data;
uint8_t rx_buffer[256];

sys_state_t sys_err;
motor_t motor_instance;
pid_t PID_V = {0.0f, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0};
pid_t PID_S = {0.0f, 0.0f, 0.0f, 0, 0.0f, 0.0f, 0};

int motor_cur = 0;
uint32_t motor_n_samples = 1;
bool calibration = false;

float k_mu = 0.0f;
float scf_vel = 0.0f;
int32_t len = 0, alen = 0, wlen = 0;  
uint8_t winter_mode = 0;

service_t service_params = {8, 1, 2, 3, 20, 400, 800, 400, 500};
/* USER CODE END 0 */

/* Error report to the master board function */
void Process_Error(ErrorType err, StatusType stat)
{
    //log last error in structure
    sys_err.sys_error = err;
    motor_instance.err_cnt++;
    
    //if fatal -> stop main cycle
    if (stat == FatalError) putevent(FATAL_ERROR, NULL);
    
    //send message to master
    tx_data.trx_packet.type = GetErrorData;
    memcpy(&tx_data.trx_packet.data[0], &sys_err, sizeof(sys_state_t));
    MX_USART1_Transmit(ADR_TDRV);
}

/* Get reference voltages and currents */
static void get_ref_ADCValues(void)
{
  //Get positive input voltage
  MX_ADC_Convert(ADC_CHAN_VREF, ADC_VREF);  
  //Get accumulator voltage
  MX_ADC_Convert(ADC_CHAN_ACC, ADC_ACC);
  //Get motor zero current voltage
  MX_ADC_Convert(ADC_CHAN_MCUR, ADC_CALIBC);
  //Get device temperature
  MX_ADC_Convert(ADC_CHAN_TMP, ADC_TEMP);
}

/* process new parameters from master board */
static inline void set_new_elec_params()
{
    //Set Umax/Uref transfer coefficient
    k_mu = motor_instance.max_U / (float)ADC_DATA[ADC_VREF]; 
    float ak = k_mu - motor_instance.k_mu;
    if (abs(ak) > 0.1f)
    {
        motor_instance.calibration = 0;
        Process_Error(CalibrationUndone, FatalError);
    }
    
    //save new torque value
    motor_instance.k_mu = k_mu; 

    //sliding length
     wlen = (int32_t)motor_instance.open_part*alen;
}

/* calculate new regulators values */
static inline void set_pid_coefficients(float tmech)
{
    float den = 1.0f;
    
    //-> velocity regulator
    den = 10.0f * motor_instance.nom_vel / motor_instance.calib_vel;
    PID_V.Kp = 1.0f / den;
    PID_V.Ki = den < 1.0f ? 2.0f * den : den;
    PID_V.Kd = tmech / den; 
    PID_V.limit = (float)PWM_PERIOD;
    //-> position regulator
    PID_S.Kp = 10.0f * tmech;
    PID_S.Ki = 0.0f;
    PID_S.Kd = tmech;
    PID_S.limit = (float)Wxx;
}

/* Save last position function */
static inline void set_mode(bool is_winter_mode)
{
    wlen = (int32_t)alen*motor_instance.open_part;
    if (is_winter_mode)
    {
        motor_instance.open_pos = motor_instance.close_pos + wlen;
        len = wlen;
    }
    else
    {
        motor_instance.open_pos = motor_instance.close_pos + alen;
        len = alen;
    }
}

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  bool self_check = false;
  bool door_closed = false;
  bool hold = false;
  int l = 0, ll = 0;
  bool winter_mode = false;
  bool current_monitor = true;
  bool auto_ctrl = false;
  float lim = 0.0f;
  float athres = 0.0f;
  int pos = 0, prev_pos = 0;
  float se = 0.0f, se2 = 0.0f, se4 = 0.0f;
  
  uint64_t t_start = 0, t_stop = 0;
  float ft_mech = 0, bt_mech = 0, fm = 0, bm = 0;
  int32_t a_max = 0, fv_max = 0, bv_max = 0, v = 0;
  
  int32_t start_pos = 0;
  float dc = 0.0f, crack = 0.0f;
    
  event_t e = {IDLE, NULL};
  /* USER CODE END 1 */

//  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
//  RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
//  memcpy((void*)0x20000000UL, (void const*)0x08008000UL, 512);
//  SYSCFG->CFGR1 |= SYSCFG_CFGR1_MEM_MODE_0 | SYSCFG_CFGR1_MEM_MODE_1;
//  __enable_irq();

  /* USER CODE BEGIN Init */
  MX_SYSTICK_Init();
  /* USER CODE END Init */

  /* Initialize all configured peripherals */
  //Init GPIO
  MX_GPIO_Init(); 
  
  //clear event queue
  clearevents();
  
  //Init UART connection to master
  memset(tx_data.trx_message, 0, PACKET_SIZE);
  tx_data.trx_packet.preamble = MAGIC_32;
  tx_data.trx_packet.adr_src = ADR_DRV;
  tx_data.trx_packet.adr_dst = ADR_TDRV;
  MX_USART1_UART_Init(rx_buffer, tx_data.trx_message);
  packet_soft_version_t *psv = (packet_soft_version_t*)tx_data.trx_packet.data;
  
  //Init ADC
  MX_ADC_Init(); 
  //Init I2C2 EEPROM
  //MX_DMA_Init();
  //MX_I2C2_Init();
  //Init PWM Timer
  MX_TIM1_Init();
  //Init Encoder Timer
  MX_TIM3_Init();
  //Init main cycle Timer
  MX_TIM6_Init();
  
  /* Start main cycle */
  if (motor_instance.err_cnt == 0) putevent(INITIALIZATION, NULL);      
  else putevent(FATAL_ERROR, NULL);
 
  /* Infinite loop */
  /* USER CODE BEGIN 3 */
  while (1)
  {
      HAL_UART_RxCpltCallback(&huart1);
      getevent(&e);
      if (e.type != IDLE)
      {
          switch((int)e.type)
          {
            case INITIALIZATION:
              //Get ADC calibration offset
              ADC_DATA[ADC_CALIBV] = GET_VOLTAGE(hadc.Instance->DR);
              //Check positive input voltage value
              MX_ADC_Convert(ADC_CHAN_VREF, ADC_VREF);
              //No positive voltage -> debugger on -> error
              if (ADC_DATA[ADC_VREF] < (ADC_VTRES << 4))   
              {
                  Process_Error(InVoltageLow, FatalError);
                  break;
              }
              //Check negative input voltage
              MX_ADC_Convert(ADC_CHAN_VIN, ADC_VIN);
              //High negative voltage -> reverse polarity -> error
              if (ADC_DATA[ADC_VIN] > ADC_VTRES)
              {
                  Process_Error(InVoltageNegative, FatalError);
                  break;
              }
              //Release input voltage 
              HAL_GPIO_WritePin(IN_ON_GPIO_Port, IN_ON_Pin, GPIO_PIN_SET);
              //Release H-brige driver 
              HAL_GPIO_WritePin(DIS_GPIO_Port, DIS_Pin, GPIO_PIN_RESET);
              //Get reference voltages
              get_ref_ADCValues();
              
              //Start PWM on channels
              HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
              HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
              HAL_TIM_PWM_Start(&htim1, TIM_CHAN_ALI);
              HAL_TIM_PWM_Start(&htim1, TIM_CHAN_BLI);
              
              //Start encoder
              HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
              //start current monitoring
              MX_ADC_Start_IT(ADC_CHAN_MCUR, ADC_MCURR);
              
              //Get driver params from memory
              MX_FLASH_Init();
              //no valid params in memory -> halt
              if (motor_instance.max_A == 0 || motor_instance.max_A == 0xFFFF || motor_instance.reverse == 0) 
              {
                  current_monitor = false;
                  Process_Error(NoDataInFlash, FatalError);
              }
              if (!motor_instance.calibration) 
              {
                  Process_Error(CalibrationUndone, FatalError);
              }
              //set len
              l = motor_instance.close_pos - motor_instance.open_pos;
              alen = abs(l);
              //main board set parameters
              set_new_elec_params();
              set_mode(winter_mode);
             
              //auto control - true
              auto_ctrl = true;
              //start the main cycle
              MX_TIM6_Start_IT();
              
              break;
            case CALIBRATION:
              //check if there are valid params
              if (!current_monitor)
              {
                  Process_Error(NoDataInFlash, FatalError);
                  break;
              }
              //stop main cycle
              MX_TIM6_Stop_IT();
              //stop motor
              drive(0, &htim1); 
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);
              //clear calibration data    
              memset(&motor_instance.calibration, 0, 18);
              memset(&motor_instance.pos, 0, 40);
              *pos_count.ticks = 0;
              
              /**** calibration algorithm start ****/ 
              calibration = true;
              
              t_start = 0; t_stop = 0;
              ft_mech = 0; bt_mech = 0;
              a_max = 0; fv_max = 0; bv_max = 0; 
              do
              {
                  motor_instance.calib_vel += 10;
                  drive(motor_instance.calib_vel, &htim1);
                  HAL_Delay(10);
                  get_encoder_values(&motor_instance);
              } while (abs(motor_instance.vel) < V_DES && 
                       motor_instance.calib_vel < PWM_PERIOD);
              
              if (abs(motor_instance.vel) < V_DES) goto calib_fail;
              
              drive(0, &htim1);                                                     //set velocity to zero
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);  //wait for stop
              
              t_stop = 0;
              drive(motor_instance.calib_vel, &htim1);                              //set velocity to max 
              t_start = GetTickCnt();                                               //get starting time
              do
              {
                  if (abs(ADC_DATA[ADC_MCURR])> a_max) a_max = abs(ADC_DATA[ADC_MCURR]);
                  HAL_Delay(10);
                  get_encoder_values(&motor_instance);
                  if (motor_instance.vel == 0) continue;
                  else if (motor_instance.acc == 0 && t_stop == 0)
                  {
                      t_stop = GetTickCnt();                                        //if the door gets constant velocity
                      ft_mech = (t_stop - t_start) / (5.0f * (float)TICKS_IN_SECOND);  //calc mecanical time constant 
                      fv_max = motor_instance.vel;                       	        //save max velocity value
                  }
                  else fm = ADC_DATA[ADC_MCURR] / (float)(motor_instance.acc + 1.0f);
              } while (t_stop == 0);
              
              if (t_stop == 0) goto calib_fail;
              
              drive(0, &htim1);                                                     //set velocity to zero
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);  //wait for stop
              
              t_stop = 0;
              drive(-motor_instance.calib_vel, &htim1);                              //set velocity to max 
              t_start = GetTickCnt();                                               //get starting time
              do
              {
                  if (abs(ADC_DATA[ADC_MCURR])> a_max) a_max = abs(ADC_DATA[ADC_MCURR]);
                  HAL_Delay(10);
                  get_encoder_values(&motor_instance);
                  if (motor_instance.vel == 0) continue;
                  else if (motor_instance.acc == 0 && t_stop == 0)
                  {
                      t_stop = GetTickCnt();                                        //if the door gets constant velocity
                      bt_mech = (t_stop - t_start) / (5.0f * (float)TICKS_IN_SECOND);  //calc mecanical time constant 
                      bv_max = motor_instance.vel;                       	        //save max velocity value
                  }
                  else bm = ADC_DATA[ADC_MCURR] / (float)(motor_instance.acc + 1.0f);
              } while (t_stop == 0);
              
              if (t_stop == 0) goto calib_fail;
              
              drive(0, &htim1);                                                     //set velocity to zero
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);  //wait for stop
               
              v = 0;      
              do
              {
                  if (v < motor_instance.calib_vel) v += 10;
                  drive(v, &htim1);
                  HAL_Delay(100);
                  get_encoder_values(&motor_instance);
              } while (abs(ADC_DATA[ADC_MCURR]) < a_max);       
              drive(0, &htim1);                                                     //set velocity to zero
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);  //wait for stop
              motor_instance.open_pos = motor_instance.pos; 	                //save max coord
              
              v = 0;      
              do
              {
                  if (v < motor_instance.calib_vel) v += 10;
                  drive(-v, &htim1);
                  HAL_Delay(100);
                  get_encoder_values(&motor_instance);
              } while (abs(ADC_DATA[ADC_MCURR]) < a_max);
          
          calib_fail:  //check calibration errors
              drive(0, &htim1);                                                     //set velocity to zero
              HAL_Delay(1000);
              while (motor_instance.vel != 0) get_encoder_values(&motor_instance);  //wait for stop
              motor_instance.close_pos = motor_instance.pos;                        //save min coord
              /**** calibration algotithm end ****/
              calibration = false;
              
              if  (len > 100000                  ||
                  (sign(fv_max) == sign(bv_max)) ||
                  (abs(fv_max) < V_MIN)          ||     //forward velosity low
                  (ft_mech > T_DES))
              {
                  motor_instance.calibration = 0;
                  memcpy(&tx_data.trx_packet.data[1], &fv_max, sizeof(int32_t));
                  memcpy(&tx_data.trx_packet.data[5], &bv_max, sizeof(int32_t));
                  memcpy(&tx_data.trx_packet.data[9], &ft_mech, sizeof(float));
                  memcpy(&tx_data.trx_packet.data[13], &bt_mech, sizeof(float));
                  memcpy(&tx_data.trx_packet.data[17], &len, sizeof(uint32_t));
                  Process_Error(CalibrationFailed, FatalError);
                  break;
              }
              
              //main board set params
              //reverse
              if (motor_instance.reverse < 0)
              {
                  motor_instance.close_pos = motor_instance.open_pos;
                  motor_instance.open_pos = motor_instance.pos;
              }
              //sliding length
              l = motor_instance.close_pos - motor_instance.open_pos;
              alen = abs(l);
              wlen = (int32_t)motor_instance.open_part*alen;
              set_mode(winter_mode);
              
              //new mechanical params
              //mechanical constant
              float t_mech = (ft_mech + bt_mech) / 2.0f;
              motor_instance.t_mech = (abs(fm) + abs(bm)) / 2.0f;
              //nominal velocity
              motor_instance.nom_vel = (uint16_t)(abs(fv_max) + abs(bv_max)) >> 1;      
              //set new regulator values
              set_pid_coefficients(t_mech);
              
              //go to the last position
              prev_pos = pos = motor_instance.pos;
              //auto control - true
              auto_ctrl = true; 
              //save new calibration status
              motor_instance.calibration = 1;
              //write into flash new calibration data
              if (winter_mode) set_mode(false);
              MX_FLASH_Backup();
              if (winter_mode) set_mode(true);
              
              //send report 
              tx_data.trx_packet.type = GetCalibrate;
              MX_USART1_Transmit(ADR_TDRV);
              
              //start the main cycle
              MX_TIM6_Start_IT();            
              break;
            case REG_CYCLE:     
              //auto position control
              if (auto_ctrl && motor_instance.calibration)    
              {
                  //speeding down when reverse
                  if (!self_check && pos != prev_pos)
                  {
                      MX_TIM6_Stop_IT();
                      v = PID_V.out;
                      do
                      {
                          v = v - (sign(v)*50);
                          drive(v, &htim1);
                          HAL_Delay(service_params.turn_delay);
                          get_encoder_values(&motor_instance);
                      } while (abs(motor_instance.vel) > (V_DES << 1));
                      prev_pos = pos;
                      MX_TIM6_Start_IT();
                      break;
                  }
                  //process position regulator
                  process_pid(&PID_S, pos, motor_instance.pos);
                  //get normalized error                  
                  se = abs(PID_S.error) / (float)len;
                  if (se > 1) se = 1;
                  se2 = se * se;
                  se4 = se2 * se2;
                      
                  if (!self_check)
                  {   
                      //calc new velocity regulator limit
                      lim = (service_params.dynamic_coef * scf_vel * PWM_PERIOD * (-se4 + se2)) + (motor_instance.calib_vel >> 1);
                      PID_V.limit = lim;
                  }
                  else PID_V.limit = motor_instance.calib_vel;
                  //process velocity regulator
                  process_pid(&PID_V, PID_S.out, motor_instance.vel);
                                  
                  PID_V.out = PID_V.out * motor_instance.reverse;
                  
                  if (!self_check)
                  {
                      crack = winter_mode ? service_params.crack_w / 100.0f : 
                                            service_params.crack_a / 100.0f;
                      if (se < 0.05 && se > crack) v = abs(PID_V.out);
                      if (se < crack && pos == motor_instance.close_pos)
                      {
                          if (v < (3*motor_instance.calib_vel) >> 2) v += 2;
                          PID_V.out = - v * motor_instance.reverse;
                      }
                      if (se < 0.005 && pos != motor_instance.open_part) door_closed = true;
                      
                      if (door_closed && hold) 
                      {
                          dc = ADC_DATA[ADC_MCURR] - (motor_cur/(float)motor_n_samples);
                          if (dc > service_params.hold_current) PID_V.out = (int32_t)(- dc * motor_instance.reverse);
                          else if (dc < -service_params.hold_current){ motor_cur = ADC_DATA[ADC_MCURR]; motor_n_samples = 1; }
                      }
                  }
                  //set output PWM value
                  drive(PID_V.out, &htim1); 
              }
              else
              {
                  athres = motor_instance.max_A;
                  //check motor velosity
                  int av = abs(motor_instance.vel);
                  int sv = sign(motor_instance.vel);
                  //if some, add small contrforce
                  if (av != 0) drive(-sv*20, &htim1);
                  else drive(0, &htim1);
              }
              
              if (door_closed)
              {
                  if (hold) athres = motor_instance.max_A << 8;
                  else athres = motor_instance.max_A << 1;
              }
              else
              {
                  athres = (abs(motor_instance.acc + 1) * motor_instance.max_A * se2); 
                  if (!self_check) athres += service_params.current_thres_a;
                  else athres += service_params.current_thres_c;
              }
              
              //check motor current  
              motor_cur = motor_cur + ADC_DATA[ADC_MCURR]; motor_n_samples++; 
              if (abs(ADC_DATA[ADC_MCURR]) > athres  &&  
                  motor_instance.vel == 0            &&
                  motor_instance.acc == 0)
              {
                  if (self_check)
                  {
                      if (pos == (alen+MAX_LEN))
                      {
                          drive(0, &htim1);
                          MX_TIM6_Stop_IT();
                          HAL_Delay(100);
                          MX_TIM6_Start_IT();
                          start_pos = motor_instance.pos;
                          motor_instance.open_pos = motor_instance.pos;
                          pos = motor_instance.pos - alen - MAX_LEN;
                      }
                      else 
                      {
                          l = motor_instance.pos - start_pos;
                          ll = abs(l) - alen;
                          if (ll < service_params.length_error && ll > -service_params.length_error)
                          {
                              pos = motor_instance.pos;
                              motor_instance.close_pos = motor_instance.pos;
                              alen = abs(l);
                              wlen = (int32_t)motor_instance.open_part*alen;
                              set_mode(winter_mode);
                          }
                          else
                          {
                              Process_Error(CalibrationUndone, FatalError);
                              break;
                          }
                      }
                  }
                  else
                  {
                      led_on(LED_ALL);
                      putevent(OVERCURRENT, NULL);
                  }
                  
              }
              else led_off(LED_ALL);
              break;
            case COMMUNICATION:
              //check header and rx address 
              if (e.data->trx_packet.adr_dst == ADR_DRV) 
              {
                  //select packet type
                  switch((uint32_t)e.data->trx_packet.type)
                  {
                      case SetVelPid:  
                        //Set velocity regulator values
                        memcpy(&PID_V.Kp, &e.data->trx_packet.data[0], 4);
                        memcpy(&PID_V.Ki, &e.data->trx_packet.data[4], 4);
                        memcpy(&PID_V.Kd, &e.data->trx_packet.data[8], 4);
                        memcpy(&PID_V.limit, &e.data->trx_packet.data[12], 4);
                        break;
                      case SetPosPid:                      
                        //Set position regulator values
                        memcpy(&PID_S.Kp, &e.data->trx_packet.data[0], 4);
                        memcpy(&PID_S.Ki, &e.data->trx_packet.data[4], 4);
                        memcpy(&PID_S.Kd, &e.data->trx_packet.data[8], 4);
                        memcpy(&PID_S.limit, &e.data->trx_packet.data[12], 4);
                        break;
                      case SetElectData:
                        //Set electrical system params
                        current_monitor = true;
                        memcpy(&motor_instance.max_A, &e.data->trx_packet.data[0], 4);
                        set_new_elec_params();
                        //write into flash new calibration data
                        if (winter_mode) set_mode(false);
                        MX_FLASH_Backup();
                        if (winter_mode) set_mode(true);
                        break;
                      case SetMechData:
                        //Set mechanical system params
                        //not implemented
                        break;
                      case SetDynData:
                        memcpy(&motor_instance.scf_vel_ao, &e.data->trx_packet.data[0], 20);
                        if (winter_mode) set_mode(false);
                        MX_FLASH_Backup();
                        if (winter_mode) set_mode(true);
                        break;
                      case SetServiceParam:
                        memcpy(&service_params.dynamic_coef, &e.data->trx_packet.data[0], 14);
                        break;
                      case SetManCtrl:
                        drive(0, &htim1);
                        MX_TIM6_Stop_IT();
                        HAL_Delay(100);
                        MX_TIM6_Start_IT();
                        //Set manual position control
                        auto_ctrl = false; 
                        break;
                      case SetAutoCtrl:
                        //release H-bridge
                        HAL_GPIO_WritePin(DIS_GPIO_Port, DIS_Pin, GPIO_PIN_RESET);
                        //Set auto position control
                        auto_ctrl = true;
                        break;
                      case SetWintCtrl:
                        //set winter opening position
                        winter_mode = true;
                        set_mode(winter_mode);
                        break;
                      case ResetWintCtrl:
                        //set winter opening position
                        winter_mode = false;
                        set_mode(winter_mode);
                        break; 
                      case ChangeReverse:
                        memcpy(&motor_instance.reverse, &e.data->trx_packet.data[0], 1);
                        int32_t cp = motor_instance.close_pos;
                        motor_instance.close_pos = motor_instance.open_pos;
                        motor_instance.open_pos = cp;
                        //write into flash new calibration data
                        if (winter_mode) set_mode(false);
                        MX_FLASH_Backup();
                        if (winter_mode) set_mode(true);
                        break;
                      case GetCalibrate:
                        //Calibrate automation system
                        putevent(CALIBRATION, NULL);
                        break;
                      case SetSelfCheck:
                        //Check door bounds
                        memset(&motor_instance.pos, 0, 40);
                        *pos_count.ticks = 0;
                        pos = alen+MAX_LEN;
                        self_check = true;
                        door_closed = false;
                        break;
                      case ResetSelfCheck:
                        self_check = false;
                        break;
                      case SetDoorOpenSlow:
                        //Open the door slowly
                        door_closed = false;
                        scf_vel = 0.1;
                        pos = motor_instance.open_pos;
                        break;
                      case SetDoorOpen:
                        //Open the door
                        door_closed = false;
                        if (winter_mode) scf_vel = motor_instance.scf_vel_wo;
                        else scf_vel = motor_instance.scf_vel_ao;
                        pos = motor_instance.open_pos;
                        break;
                      case SetDoorClose:
                        //Close the door
                        door_closed = false; 
                        if (winter_mode) scf_vel = motor_instance.scf_vel_wc;
                        else scf_vel = motor_instance.scf_vel_ac;
                        pos = motor_instance.close_pos;
                        break;
                      case SetDoorStop:
                        drive(0, &htim1);
                        pos = motor_instance.pos;
                        break;
                      case SetAccOpen:
                        //Release accumulator transistor
                        HAL_GPIO_WritePin(ACC_ON_GPIO_Port, ACC_ON_Pin, GPIO_PIN_SET);
                        break;
                      case SetAccClose:
                        //Hold accumulator transistor
                        HAL_GPIO_WritePin(ACC_ON_GPIO_Port, ACC_ON_Pin, GPIO_PIN_RESET);
                        break;
                      case SetTime:
                        //Set system time
                        //Not implemented 
                        break;
                      case SetHoldMotor:
                        hold = true;
                        break;
                      case ResetHoldMotor:
                        hold = false;
                        break;
                      case GetVelPid:
                        //Get velocity regulator values 
                        tx_data.trx_packet.type = GetVelPid;
                        memcpy(&tx_data.trx_packet.data[0], &PID_V.Kp, 20);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetPosPid:
                        //Get position regulator values
                        tx_data.trx_packet.type = GetPosPid;
                        memcpy(&tx_data.trx_packet.data[0], &PID_S.Kp, 20);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                       case GetElectData:
                        //Get electrical system params
                        tx_data.trx_packet.type = GetElectData;
                        memcpy(&tx_data.trx_packet.data[0],  &motor_instance.max_A, 6);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetMechData:
                        //Get mechanical system params
                        tx_data.trx_packet.type = GetMechData;
                        memcpy(&tx_data.trx_packet.data[0], &motor_instance.nom_vel, 10);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetDynData:
                        tx_data.trx_packet.type = GetDynData;
                        memcpy(&tx_data.trx_packet.data[0], &motor_instance.scf_vel_ao, 20);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetServiceParam:
                        tx_data.trx_packet.type = GetServiceParam;
                        memcpy(&tx_data.trx_packet.data[0], &PID_V.Ki, 4);
                        memcpy(&tx_data.trx_packet.data[4], &service_params.dynamic_coef, 14);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetAccVolt:
                        //Get accumulator voltage 
                        MX_ADC_Convert(ADC_CHAN_ACC, ADC_ACC);
                        tx_data.trx_packet.type = GetAccVolt;
                        memcpy(&tx_data.trx_packet.data[0], &ADC_DATA[ADC_ACC], 4);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetTime:
                        //Get system time
                        tx_data.trx_packet.type = GetTime;
                        //Not implemented
                        break;
                      case GetErrorCount:
                        //Get error count
                        tx_data.trx_packet.type = GetErrorCount;
                        memcpy(&tx_data.trx_packet.data[0], &motor_instance.err_cnt, 2);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                      case GetErrorData:
                        //Get error log
                        tx_data.trx_packet.type = GetErrorData;
                        memcpy(&tx_data.trx_packet.data[0], &sys_err, sizeof(sys_state_t));
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetVoltageData:
                        //Get system voltages
                        get_ref_ADCValues();
                        tx_data.trx_packet.type = GetVoltageData;
                        memcpy(&tx_data.trx_packet.data[0], &ADC_DATA[ADC_CALIBV], 24);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case GetSystemData:
                        //Get system data
                        tx_data.trx_packet.type = GetSystemData;
                        memcpy(&tx_data.trx_packet.data[0], &ADC_DATA[ADC_MCURR], 4);
                        memcpy(&tx_data.trx_packet.data[4], &athres, 4);
                        memcpy(&tx_data.trx_packet.data[8], &motor_instance.pos, 12);
                        memcpy(&tx_data.trx_packet.data[20], &PID_S.error, 4);
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                        break;
                      case CMD_VERSION: // Get Type Soft & Version
                        tx_data.trx_packet.type = CMD_VERSION;
                        psv->fw_types   = FW_TYPE;
                        psv->soft_ver_h = SOFT_VER_H;
                        psv->soft_ver_l = SOFT_VER_L;
                        psv->soft_ver_y = SOFT_VER_Y;
                        MX_USART1_Transmit((packet_adr_t)e.data->trx_packet.adr_src);
                      default:
                        //Unknown packet type
                        break;
                  }
              }
              break;
           case OVERCURRENT:
              MX_TIM6_Stop_IT();
              HAL_Delay(10);
              //check motor current
              MX_ADC_Convert(ADC_CHAN_MCUR, ADC_MCURR);
              if (abs(ADC_DATA[ADC_MCURR]) > athres && current_monitor)
              {
                  //stop motor - hold H-bridge in disable
                  HAL_GPIO_WritePin(DIS_GPIO_Port, DIS_Pin, GPIO_PIN_SET);
                  Process_Error(Overcurrent, FatalError);
              }
              MX_TIM6_Start_IT();
              break;
            case FATAL_ERROR:
              //stop auto control
              auto_ctrl = false;
              break;
            default:
              break;
          }
          e.type = IDLE;
      }
  }
  /* USER CODE END 3 */


}

/* SysTick init function */
static void MX_SYSTICK_Init(void)
{
  /*Configure the Systick interrupt time */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/TICKS_IN_UNIT);
  /*Configure the Systick*/
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  /*Configure the SysTick IRQ priority */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0U, 0U);
  /*Enable SysTick IRQ */
  HAL_NVIC_EnableIRQ(SysTick_IRQn);
}

/* ADC init function */
static void MX_ADC_Init(void)
{
    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
    hadc.Instance = ADC1;
    hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
    hadc.Init.Resolution = ADC_RESOLUTION_12B;
    hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
    hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    hadc.Init.LowPowerAutoWait = DISABLE;
    hadc.Init.LowPowerAutoPowerOff = DISABLE;
    hadc.Init.ContinuousConvMode = DISABLE;
    hadc.Init.DiscontinuousConvMode = DISABLE;
    hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc.Init.DMAContinuousRequests = DISABLE;
    hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
    if (HAL_ADC_Init(&hadc) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
    if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
}

/* ADC star5t conversion - polling */
static void MX_ADC_Convert(uint8_t chan, adc_chan_t conv)
{
    int32_t res = 0;
    
    if (chan != ADC_CHAN_TMP)
    {
        //Configure for the selected ADC regular channel to be converted. 
        hadc.Instance->SMPR = ADC_SAMPLETIME_239CYCLES_5;
        hadc.Instance->CHSELR = (1U << (chan));
        conv_ind = conv;
        //start conversion
        hadc.Instance->CR |= ADC_CR_ADEN;
        hadc.Instance->ISR = (ADC_FLAG_EOC | ADC_FLAG_EOS | ADC_FLAG_OVR);
        hadc.Instance->CR |= ADC_CR_ADSTART;
        //poll for conversion
        while (!DelayCnt(10, &hadc.Instance->ISR, (ADC_FLAG_EOC | ADC_FLAG_EOS)));

        res = (int32_t)GET_VOLTAGE(hadc.Instance->DR);
        if (conv_ind != ADC_MCURR) ADC_DATA[conv_ind] = res;
        else 
        {
            ADC_DATA[conv_ind] += res - ADC_DATA[ADC_CALIBC];
            ADC_DATA[conv_ind] = ADC_DATA[conv_ind] / 2;
        }
    }
    else
    {
        ADC_ChannelConfTypeDef sConf;
        
        //Configure for the selected ADC regular channel to be converted. 
        sConf.Rank = ADC_RANK_CHANNEL_NUMBER;
        sConf.Channel = ADC_CHANNEL_TEMPSENSOR;
        sConf.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
        if (HAL_ADC_ConfigChannel(&hadc, &sConf) != HAL_OK)
        {
            //Process_Error(ADCConvertionFailed, SystemError);
        }
        conv_ind = conv;
        //poll for conversion
        if (HAL_ADC_PollForConversion(&hadc, 10) != HAL_OK)
        {
            //Process_Error(ADCConvertionFailed, SystemError);
        }
        ADC_DATA[conv_ind] = ((uint32_t) *TEMP30_CAL_ADDR - (hadc.Instance->DR * VDD_APPLI / VDD_VALUE)) * 1000;
        ADC_DATA[conv_ind] = (ADC_DATA[conv_ind] / AVG_SLOPE) + 30;
    }
}

/* ADC start conversion - interrupt mode */
static void MX_ADC_Start_IT(uint8_t chan, adc_chan_t conv)
{
    //Configure for the selected ADC regular channel to be converted. 
    hadc.Instance->SMPR = ADC_SAMPLETIME_71CYCLES_5;
    hadc.Instance->CHSELR = (1U << (chan));
    conv_ind = conv;
    //start conversion
    hadc.Instance->CR |= ADC_CR_ADEN;
    hadc.Instance->ISR = (ADC_FLAG_EOC | ADC_FLAG_EOS | ADC_FLAG_OVR);
    hadc.Instance->IER = (ADC_IT_EOC | ADC_IT_EOS | ADC_IT_OVR);
    hadc.Instance->CR |= ADC_CR_ADSTART;
}

static void MX_ADC_Stop_IT(void)
{
    //start conversion
    hadc.Instance->ISR = (ADC_FLAG_EOC | ADC_FLAG_EOS | ADC_FLAG_OVR);
    hadc.Instance->IER &= ~(ADC_IT_EOC | ADC_IT_EOS | ADC_IT_OVR);
    hadc.Instance->CR &= ~ADC_CR_ADSTART;
    hadc.Instance->CR &= ~ADC_CR_ADEN;
}

/* I2C2 init function */
static void MX_I2C2_Init(void)
{

    hi2c2.Instance = I2C2;
    hi2c2.Init.Timing = 0x20303E5D;
    hi2c2.Init.OwnAddress1 = 0;
    hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c2.Init.OwnAddress2 = 0;
    hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c2) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

      /**Configure Analogue filter 
      */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

      /**Configure Digital filter 
      */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
}

/* FLASH init function */
static void MX_FLASH_Init(void)
{
    uint32_t* data;
    uint32_t motor_t_size = MOTOR_POSITION_FLASH_ADDRESS - MOTOR_INSTANCE_FLASH_ADDRESS;
    uint32_t pid_t_size = sizeof(float) << 2;
    
    HAL_FLASH_Unlock();
    
    data = (uint32_t*)MOTOR_INSTANCE_FLASH_ADDRESS;
    if (*data == MAGIC_32) 
    {
        data += 1;
        memcpy(&motor_instance.k_mu, data, motor_t_size);
        data += (motor_t_size >> 2);
        memcpy(&PID_S.Kp, data, pid_t_size);
        data += (pid_t_size >> 2);
        memcpy(&PID_V.Kp, data, pid_t_size);
    }
    else Process_Error(NoDataInFlash, FatalError);
    
    HAL_FLASH_Lock();
}

/* FLASH data backup function */
static void MX_FLASH_Backup(void)
{
    FLASH_EraseInitTypeDef FlashEx;
    uint32_t addr;
    uint16_t* data;
    
    uint32_t motor_t_size = MOTOR_POSITION_FLASH_ADDRESS - MOTOR_INSTANCE_FLASH_ADDRESS;
    uint32_t pid_t_size = sizeof(float) << 2;
    
    HAL_FLASH_Unlock();
     
    FlashEx.PageAddress = MOTOR_INSTANCE_FLASH_ADDRESS;
    FlashEx.NbPages = 1;
    FlashEx.TypeErase = FLASH_TYPEERASE_PAGES;
    if (HAL_FLASHEx_Erase(&FlashEx, &addr) != HAL_OK || addr != 0xFFFFFFFF)
    {

        Process_Error(PeriphInitFailed, SystemError);
    }
    
    addr = MOTOR_INSTANCE_FLASH_ADDRESS;
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, MAGIC_32);
    
    //write 'motor_instance' data
    addr += 4;
    data = (uint16_t*)&motor_instance.k_mu;
    for (int i = 0; i < (motor_t_size >> 1); i++)
    {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *data);
        data++;
        addr += 2;
    }
    
    //write 'PID_S' data
    data = (uint16_t*)&PID_S.Kp;
    for (int i = 0; i < (pid_t_size >> 1); i++)
    {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *data);
        data++;
        addr += 2;
    }
    
    //write 'PID_V' data
    data = (uint16_t*)&PID_V.Kp;
    for (int i = 0; i < (pid_t_size >> 1); i++)
    {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, addr, *data);
        data++;
        addr += 2;
    }
    HAL_FLASH_Lock();
}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    TIM_OC_InitTypeDef sConfigOC;
    TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

    htim1.Instance = TIM1;
    htim1.Init.Prescaler = 0;
    htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim1.Init.Period = PWM_PERIOD;
    htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim1.Init.RepetitionCounter = 0;
    htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    sConfigOC.OCMode = TIM_OCMODE_PWM2;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
    if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
    sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
    sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
    sBreakDeadTimeConfig.DeadTime = 0;
    sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
    sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
    sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
    if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
    HAL_TIM_MspPostInit(&htim1);
}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{
    TIM_Encoder_InitTypeDef sConfig;
    TIM_MasterConfigTypeDef sMasterConfig;

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 0;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = ENC_PERIOD;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
    sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
    sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC1Filter = 0;
    sConfig.IC2Polarity = TIM_ICPOLARITY_FALLING;
    sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
    sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
    sConfig.IC2Filter = 0;
    if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
}

/* TIM6 init function */
static void MX_TIM6_Init(void)
{
    htim6.Instance = TIM6;
    htim6.Init.Prescaler = CYCLE_DIV;
    htim6.Init.Period = CYCLE_LENGTH;
    htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
    {
        Process_Error(PeriphInitFailed, SystemError);
        return;
    }
}

static void MX_TIM6_Start_IT(void)
{
    __HAL_TIM_ENABLE_IT(&htim6, TIM_IT_UPDATE);
    __HAL_TIM_ENABLE(&htim6);
}

static void MX_TIM6_Stop_IT(void)
{
    __HAL_TIM_DISABLE_IT(&htim6, TIM_IT_UPDATE);
    __HAL_TIM_DISABLE(&htim6);
}

/* USART1 init function */
static void MX_USART1_UART_Init(uint8_t* rxbuf, uint8_t* txbuf)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
      //UART init error -> no communicatiion with master
      //hope we'll never meet this case
      sys_err.sys_error = PeriphInitFailed;
      motor_instance.err_cnt++;
  }
  else
  {
      huart1.pRxBuffPtr = rxbuf;
      huart1.pTxBuffPtr = txbuf;
      CONSOLE_ENABLE
  }
}

/* USART1 transmit function */
static void MX_USART1_Transmit(packet_adr_t dst)
{
    packet_t* p = (packet_t*)huart1.pTxBuffPtr;
    p->adr_dst = dst;
    
    while(huart1.TxXferCount != PACKET_SIZE)
    {
        while (!(huart1.Instance->ISR & UART_FLAG_TXE) &&
               !(huart1.Instance->ISR & UART_FLAG_TC));
        huart1.Instance->TDR = huart1.pTxBuffPtr[huart1.TxXferCount++];
    }
    huart1.TxXferCount = 0;
}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_IRQn, 4U, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, AHI_Pin|BHI_Pin|LED_Pin|DIS_Pin 
                          |IN_ON_Pin|ACC_ON_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : OVER_C_Pin */
  GPIO_InitStruct.Pin = OVER_C_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OVER_C_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : AHI_Pin BHI_Pin LED_Pin DIS_Pin 
                           IN_ON_Pin ACC_ON_Pin */
  GPIO_InitStruct.Pin = AHI_Pin|BHI_Pin|LED_Pin|DIS_Pin 
                          |IN_ON_Pin|ACC_ON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 4U, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
