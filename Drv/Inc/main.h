/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define TIM_CHAN_ALI TIM_CHANNEL_1
#define TIM_CHAN_BLI TIM_CHANNEL_4
#define ADC_CHAN_MCUR ADC_CHANNEL_0
#define ADC_CHAN_VREF ADC_CHANNEL_1
#define ADC_CHAN_VIN ADC_CHANNEL_2
#define ADC_CHAN_ACC ADC_CHANNEL_3
#define ADC_CHAN_TMP ADC_CHANNEL_TEMPSENSOR
#define ADC_IN0_I_M_Pin GPIO_PIN_0
#define ADC_IN0_I_M_GPIO_Port GPIOA
#define ADC_IN1_VIN_P_Pin GPIO_PIN_1
#define ADC_IN1_VIN_P_GPIO_Port GPIOA
#define ADC_IN2_VIN_N_Pin GPIO_PIN_2
#define ADC_IN2_VIN_N_GPIO_Port GPIOA
#define ADC_IN3_VACC_N_Pin GPIO_PIN_3
#define ADC_IN3_VACC_N_GPIO_Port GPIOA
#define OVER_C_Pin GPIO_PIN_5
#define OVER_C_GPIO_Port GPIOA
#define OVER_C_EXTI_IRQn EXTI4_15_IRQn
#define AHI_Pin GPIO_PIN_1
#define AHI_GPIO_Port GPIOB
#define BHI_Pin GPIO_PIN_2
#define BHI_GPIO_Port GPIOB
#define LED_Pin GPIO_PIN_12
#define LED_GPIO_Port GPIOB
#define DIS_Pin GPIO_PIN_15
#define DIS_GPIO_Port GPIOB
#define TIM1_CH1_ALI_Pin GPIO_PIN_8
#define TIM1_CH1_ALI_GPIO_Port GPIOA
#define TIM1_CH4_BLI_Pin GPIO_PIN_11
#define TIM1_CH4_BLI_GPIO_Port GPIOA
#define IN_ON_Pin GPIO_PIN_3
#define IN_ON_GPIO_Port GPIOB
#define TIM3_CH1_ENC_A_Pin GPIO_PIN_4
#define TIM3_CH1_ENC_A_GPIO_Port GPIOB
#define TIM3_CH2_ENC_B_Pin GPIO_PIN_5
#define TIM3_CH2_ENC_B_GPIO_Port GPIOB
#define ACC_ON_Pin GPIO_PIN_6
#define ACC_ON_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
